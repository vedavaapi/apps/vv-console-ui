import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { EndpointsService } from 'src/app/endpoints.service';
import { OauthHelperService } from 'src/app/services/oauth-helper.service';
import * as $ from 'jquery';
import { DOCUMENT } from '@angular/common';
import { ChangeDetectorRef } from '@angular/core';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { BooksService } from 'src/app/services/books-service'
import { ScannedBook, Library, joFromJson } from 'src/app/services/models';
import { ObjstoreApiService } from 'src/app/services/objstore-api.service';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { JsonUtilsService } from 'src/app/services/json-utils.service';


@Component({
    selector: 'app-book-list',
    templateUrl: './book-list.component.html',
    styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit {

    private vvSiteUrl: string;
    
    private currentLibraryId: string = '0';

    private currentLibraryName: string = '';
    
    private books: Array<ScannedBook>;
    private startIndex: number = 0;
    private static BOOKS_COUNT: number = 15;

    private isSearchByTitleEnabled: boolean = false;
    private isFetchBooksFirstTime: boolean = false;
    private authorized: boolean = false;
    private clickFirstTimeSearch: boolean = false;
    //stores book images, title, author name.
    private bookStoreData = [];

    private matchedApps: {
        [jsonClass: string]: any[];
    } = {};
    private companionUrl: string;


    constructor(@Inject(DOCUMENT) private document: Document, private endpointService: EndpointsService, private http: HttpClient,
        private oauthHelperService: OauthHelperService, private cdRef: ChangeDetectorRef,
        private booksService: BooksService, 
        private objstore: ObjstoreApiService,
        private localStorage: LocalStorageService,
        private _route: ActivatedRoute,
        private _spinner: NgxSpinnerService,
        private _jsonUtilsService: JsonUtilsService) {
            
        //let queryParams = new URLSearchParams(document.location.search.substring(1));
        //this.currentLibraryId = queryParams.get('library_id') || '0';
        this.vvSiteUrl = this.localStorage.getValue('server_url');
        this.companionUrl = this.oauthHelperService.companionUrl;
        this.subscribeAuthorizationChangeBroadcast();
    }

    // get the updated status of login or logout.
    private subscribeAuthorizationChangeBroadcast() {
        let bc = new BroadcastChannel(this.oauthHelperService.authorizationChangeBroadcastChannelName);
        bc.onmessage = ({ data }) => {
            if (data) {
                this.updateAuthorizedStatus();
            }
        }
    }


    /**
     * Append book details to the book list displayed.
     * @param coverPageThumbnailUrl 
     * @param bookName 
     * @param authorName
     */

    private addBook(book: ScannedBook) {
        let title = book.getTitle();
        let authors = book.getAuthors() || [];
        let authorName = authors.join(', ');
        let coverImageUrl = book.cover ? book.cover.getIIIFOrDefaultUrl(this.vvSiteUrl, 'full', ',200', '0', 'default', 'png') : ((book.jsonClass == 'ScannedBook') ? "assets/default-book-īmg.png" : "assets/textdoc.svg");
        this.bookStoreData.push({ book_id: book._id, Name: title, img: coverImageUrl, authorName: authorName, jsonClass: book.jsonClass });
    }


    /**
     * Fetches the Book List
     * @param searchString 
     */
    fetchBooksList(searchString: string) {
        $("#span-no-books-present-text").css('display', 'none');
        $("#span-no-books-present-text-search").css('display', 'none');
        $('#home-books-load-more-btn-wrapper').css('display', 'none');
        $('#loadding-gif-icon').show();

        this.isFetchBooksFirstTime = false;
        let successCallback = (response: any) => {
            $('#loadding-gif-icon').hide();
            this.books = response.items;

            if(!this.books || !this.books.length) {
                if (this.isSearchByTitleEnabled && this.clickFirstTimeSearch) {
                    $("#span-no-books-present-text-search").css('display', 'block');
                } else {
                    $("#span-no-books-present-text-search").css('display', 'none');
                }

                if (this.isFetchBooksFirstTime) {
                    $("#span-no-books-present-text").css('display', 'block');
                } else {
                    $("#span-no-books-present-text").css('display', 'none');
                }
            } else {
                for(let book of this.books) {
                    this.addBook(book);
                }
                this.updateAuthorizedStatus();  // TODO
            }

            this.startIndex += this.books.length;

            if(this.books.length >= BookListComponent.BOOKS_COUNT) {
                $('#home-books-load-more-btn-wrapper').show();
            } else {
                $('#home-books-load-more-btn-wrapper').css('display', 'none');
            }
        }

        let errorCallback = (error: any) => {
            $('#loadding-gif-icon').hide();
            if (!error) {
                return;
            }
            $("#span-no-books-present-text").text('Unable to fetch books.');  // TODO: Specific Error Message needs to be shown
            if (this.isFetchBooksFirstTime) {
                $("#span-no-books-present-text").css('display', 'block');
            } else {
                $("#span-no-books-present-text").css('display', 'none');
            }
        }

        let selectorDoc = {"source": this.currentLibraryId};
        if(searchString) {
            selectorDoc['title'] = {
                "$regex": `^${searchString}.*$`
            };
        }
        this.booksService.getBooks(this.vvSiteUrl, selectorDoc, undefined, [["created", -1]], this.startIndex, BookListComponent.BOOKS_COUNT, false, successCallback, errorCallback);
    }

    ngOnInit() {
        this._route.queryParamMap.subscribe(queryParams => {
            var libraryName = queryParams.get("name");
            if (libraryName != null && libraryName.trim().length > 0) {
                this.currentLibraryName = libraryName;
                $('#span_library_name').text(libraryName.trim());
            }//if
        });

        this._route.params.subscribe(params => {
            if (params['_id']) {
                this.currentLibraryId = params['_id']
            }
            this.fetchBooksList(null);
            this.isFetchBooksFirstTime = true;
            this.updateAuthorizedStatus();
        });
    }

    /**
     * display edit,delete icon by authentication. 
    */
    updateAuthorizedStatus() {
        let authorizedCreds = this.oauthHelperService.getAuthorizedCreds();
        this.cdRef.detectChanges();
        this.authorized = Boolean(authorizedCreds);
        this.cdRef.detectChanges();

        if (this.authorized) {
            $('.add-book-icon-holder').css('display','block');
            $('.book-list-edit-icon').css('display', 'inline-block');
            $('.book-list-about-us-icon').css('display', 'none');
            $('.book-list-delete-icon').css('display', 'inline-block');
        } else {
            $('.add-book-icon-holder').css('display','none');
            $('.book-list-edit-icon').css('display', 'none');
            $('.book-list-about-us-icon').css('display', 'none');
            $('.book-list-delete-icon').css('display', 'none');
        }

    }

    /**
     * Load more books
    */
    getLoadMoreBooks() {
        this.clickFirstTimeSearch = false;
        this.fetchBooksList($('#input-search-book-by-title').val());
    }

    /**
     * Shows the clear input book search filter .
     */
    filterClear() {
        $('#clear-icon').css('display', 'inline-block');
    }

    /**
    * Hides the close button.
    */
    hideCloseButton() {
        $('#clear-icon').css('display', 'none');
        $('#input-search-book-by-title').val('');

        if (!this.isSearchByTitleEnabled) {
            return;
        }
        this.isSearchByTitleEnabled = false;
        this.bookStoreData = [];
        this.startIndex = 0;
        this.fetchBooksList(null);
    }


    /**
     * Searches Book By title.
     */
    searchBooksByTitle() {
        this.updateAuthorizedStatus();
        let searchString = $('#input-search-book-by-title').val();
        if (searchString == null || searchString.trim().length == 0) {
            console.log("No Search String given.");
            return;
        }
        this.filterClear();
        this.isSearchByTitleEnabled = true;
        this.isFetchBooksFirstTime = false;
        this.clickFirstTimeSearch = true;
        //Remove All existing books.
        this.bookStoreData = [];
        this.startIndex = 0;
        this.fetchBooksList(searchString);
    }


    private onDeleteButtonClicked(bookObject: any){
        if(!bookObject){
            return;
        }
       let bookId = bookObject.book_id;
       if(!bookId){
           return;
       }

       let resourceIds = new Array();
       resourceIds.push(bookId);

       this._spinner.show();
       this.objstore.deleteResources(this.vvSiteUrl, resourceIds,
        successResponse => {
            this._spinner.hide();
            $('#books-list-item-' + bookId).css('display', 'none');
            this.insertAlertMessage('success-alert-parent', "Success! Deleted the book from this library");
        },
        error => {
            this._spinner.hide();
            this.insertAlertMessage('failure-alert-parent', this._jsonUtilsService.getErrorMessage(error, 'delete_book',
            'Failure! Unable to delete the book'));
     
        });
    }

    private insertAlertMessage(spanClass: string, message: string){
        if(!message || message.trim().length == 0 || !spanClass){
            return;
        }
        $('.' + spanClass + " span").text(message);
        $('.' + spanClass).css('display', 'block');
    }

    private getMatchedApps(object: any) {
        if (Object.prototype.hasOwnProperty.call(this.matchedApps, object.jsonClass)) {
            return this.matchedApps[object.jsonClass]
        }
        let registeredApplications = this.oauthHelperService.getApplications(this.vvSiteUrl)
        if (!registeredApplications) {
            return;
        }
        this.matchedApps[object.jsonClass] = [];

        for (let app of registeredApplications) {
            if (app.handledTypes && Array.isArray(app.handledTypes) && app.handledTypes.includes(object.jsonClass) && !app.apis
            ) {
                this.matchedApps[object.jsonClass].push(app)
            }
        }
        return this.matchedApps[object.jsonClass];
    }

    private appUrl(object: any) {
        const matchedApps = this.getMatchedApps(object);
        if (!matchedApps || !matchedApps.length) {
            return `object/${object.book_id}`;
        }
        const app = matchedApps[0];
        let url = new URL(app.url);
        url.searchParams.set('_id', object.book_id);
        url.searchParams.set('companion', this.companionUrl)
        url.searchParams.set('jsonClass', object.jsonClass);
        return url.href;
    }
    
}
