import { Component, OnInit } from '@angular/core';
import { OauthHelperService } from 'src/app/services/oauth-helper.service';
import { ChangeDetectorRef } from '@angular/core';


@Component({
    selector: 'app-unified-header',
    templateUrl: './unified-header.component.html',
})

export class UnifiedHeaderComponent implements OnInit {
    public authorized = false;

    constructor(private oauthHelperService: OauthHelperService, private cdRef: ChangeDetectorRef) {
        this.subscribeAuthorizationChangeBroadcast();
    }

    private subscribeAuthorizationChangeBroadcast() {
        let bc = new BroadcastChannel(this.oauthHelperService.authorizationChangeBroadcastChannelName);
        bc.onmessage = ({ data }) => {
            if (data) {
                this.updateAuthorizedStatus();
            }
        }
    }

    updateAuthorizedStatus() {
        let authorizedCreds = this.oauthHelperService.getAuthorizedCreds();
        this.cdRef.detectChanges();
        this.authorized = Boolean(authorizedCreds);
        console.log('unbm', {'authorized': this.authorized});
        this.cdRef.detectChanges();
    }

    ngOnInit() {
        this.updateAuthorizedStatus();
    }
}
