import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'src/app/services/local-storage.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

    private vvSiteUrl: string;

    constructor(private localStorage: LocalStorageService) {
        this.vvSiteUrl = this.localStorage.getValue('server_url');
    }

    ngOnInit() {
    }

}
