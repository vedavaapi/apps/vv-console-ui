import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EndpointsService } from '../../endpoints.service';

const LOGIN_TOKEN = 'login_token';

@Injectable({
  providedIn: 'root'
})


export class LoginService {


  constructor(private http: HttpClient, private endpointService: EndpointsService) {

  }

  /**
   *
   * Stores the login token to local storage.
   * @param {string} token
   * @memberof LoginService
   */
  storeLoginToken(token: string): void {
    localStorage.setItem(LOGIN_TOKEN, token);
  }

  /**
   * Removes the Login Token from Local Storage.
   */
  removeLoginToken(): void {
    localStorage.removeItem(LOGIN_TOKEN);
  }

  /**
   *
   * Returns true if user is logged in.
   * @returns {boolean}
   * @memberof LoginService
   */
  isLoggedIn(): boolean {
    return localStorage.getItem(LOGIN_TOKEN) != null;
  }


  /**
   * Logout from the client.
   * @param successCB 
   * @param errorCB 
   */
  logout(callback: (responseFlag: boolean) => void) {

    this.http.get(this.endpointService.getBaseUrl() + '/accounts/v1/oauth/signout')
      .subscribe(
        response => {
          console.log('Logged out successfully');
          if(callback != null) {
            callback(true);
          }
        },
        error => {
          console.log('Could not logout from the vedavaapi service');
          if(callback != null) {
            callback(false);
          }
        }
      )
  }

  /**
   *
   * Login to server.
   * @param {*} $email
   * @param {*} $password
   * @memberof LoginService
   */
  login($email, $password, errorCB: (message: string) => void, successCB: (token: string) => void) {

    const httpUploadOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
    };
    this.http.post(this.endpointService.getBaseUrl() + '/accounts/v1/oauth/signin',
      'email=' + $email + '&password=' + $password,
      httpUploadOptions).subscribe(
        response => {
          console.log('Login API call success!! ');
          if (successCB != null) {
            successCB($email);
          }
        },
        error => {
          console.log('Login API call failed', error);

          const errorStr = JSON.stringify(error);
          let errorCode;
          JSON.parse(errorStr, (key, value) => {
            if (key === 'code') {
              errorCode = value;
            }
            return value;
          });

          if (errorCB != null) {
            if (errorCode === 401) {
              errorCB('Email address or password incorrect. Please retry with the right credentials.');
            } else {
              errorCB('Unknown error, unable to login to Vedavaapi. Please retry after sometime.');
            }
          }
        }
      );

  }
}
