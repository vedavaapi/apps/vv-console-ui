import { Component, OnInit, Inject, ChangeDetectorRef, ElementRef, ViewChild } from '@angular/core';
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { DOCUMENT } from '@angular/common';
import { EndpointsService } from 'src/app/endpoints.service';
import { OauthHelperService } from 'src/app/services/oauth-helper.service';
import { GroupsApiService } from 'src/app/services/groups-api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PagedDataComponent } from '../paged-data/paged-data.component';
import { UserApiService } from 'src/app/services/user-api.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { VedavaapiApiService } from 'src/app/services/vedavaapi-api.service';
import { PermissionsDropdownComponent } from '../widgets/permissions-dropdown/permissions-dropdown.component';

@Component({
    selector: 'app-user-list',
    templateUrl: './user-list.component.html',
    styleUrls: ['./user-list.component.scss']
})

/**
 * Component Displays Users(of Teams/Groups)
 */
export class UserListComponent extends PagedDataComponent {

    private groupIdentifier: string;

    private groupMemberIds: any;

    private static DISPLAY_TYPE_TEAM_MEMBERS: number = 0;

    private static DISPLAY_TYPE_ADD_REMOVE_MEMBERS: number = 1;

    private static DISPLAY_TYPE_FOR_PERMISSIONS: number = 2;


    private displayType: number = UserListComponent.DISPLAY_TYPE_TEAM_MEMBERS;

    private usersAttributes: any = [];

    private selectedUsers: string[] = [];


    private teamDisplayMode: number = PagedDataComponent.DISPLAY_MODE_DEFAULT;

    private targetObjectId: string = null;

    private isObjectRoot: string = null;

    private isPageClicked: boolean = false;

    @ViewChild(PermissionsDropdownComponent) permissionsComponent: PermissionsDropdownComponent;

    /**
     * Constructor of the Component.
     * @param document 
     * @param endpointService 
     * @param oauthHelperService 
     * @param cdRef 
     * @param _route 
     * @param _router 
     * @param _groupsApiService 
     * @param _userApiService 
     */
    constructor(@Inject(DOCUMENT) protected document: Document, protected endpointService: EndpointsService,
        protected oauthHelperService: OauthHelperService, protected cdRef: ChangeDetectorRef,
        private spinner: NgxSpinnerService,
        private _route: ActivatedRoute,
        private _router: Router,
        private _groupsApiService: GroupsApiService,
        private _userApiService: UserApiService,
        private _vedavaapiApiService: VedavaapiApiService) {
        super(document, endpointService, oauthHelperService, cdRef, _router);

    }

    ngOnInit() {
        super.ngOnInit();

        $('#btn-permissions-submit').click(callback => {
            this.permissionSubmitButtonClicked();
        });

        this._route.queryParams.subscribe(params => {
            this.isObjectRoot = params['object-root'];
        });

    }

    /**
     * Callback when an item is selected from list.
     * @param itemId 
     */
    protected onItemSelected(itemId: string) {
        if (!itemId) {
            return;
        }//if
        if (!this.selectedUsers) {
            this.selectedUsers = [];
        }
        var index = this.selectedUsers.indexOf(itemId);
        if (index > -1) {
            this.selectedUsers.splice(index, 1);
            return;
        }//if
        this.selectedUsers.push(itemId);
        console.log("Selected Users: " + this.selectedUsers.toString());

    }

    /**
     * Permissions Submit Button has been clicked.
     */
    private permissionSubmitButtonClicked() {

        $('#permissions-wizard-error').css('display', 'none');
        if (!this.groupIdentifier) {
            return;
        }
        var formOptions = this.permissionsComponent.getPermissionsFormJson();
   
        var isIncludeTeam = false; //check_include_team
        var teamIds = [];
        if(this.selectedUsers == null){
            this.selectedUsers = [];
        }
        if (formOptions.hasOwnProperty('check_include_team') && formOptions["check_include_team"]) {
            isIncludeTeam = true;
            teamIds = [this.groupIdentifier];
        }//if
        if (formOptions.hasOwnProperty('check_nologin') && formOptions["check_nologin"]) {
            this.selectedUsers.push("*");
        }//if

        if(!isIncludeTeam){
            if (!this.selectedUsers || this.selectedUsers.length == 0) {
                $('#permissions-wizard-error').text('Please select users to whom permissions are to be granted/withdrawn. Alternatively, you can grant or revoke permissions on the team itself by checking the corresponding action below.');
                $('#permissions-wizard-error').css('display', 'block');
                return;
            }
        }
        
        if (!formOptions) {
            $('#permissions-wizard-error').text('Please select permission options.');
            $('#permissions-wizard-error').css('display', 'block');
            return;
        }

        console.log(JSON.stringify(formOptions));

        var isGrantPermissions = false;
        var isWithdrawPermissions = false;

        if (formOptions.hasOwnProperty('check_grant') && formOptions["check_grant"]) {
            isGrantPermissions = true;
        }//if

        if (formOptions.hasOwnProperty('check_withdraw') && formOptions["check_withdraw"]) {
            isWithdrawPermissions = true;
        }//if

        if (!isGrantPermissions && !isWithdrawPermissions) {
            $('#permissions-wizard-error').text('You can either grant or withdraw permissions. You have chosen none.');
            $('#permissions-wizard-error').css('display', 'block');
            return;
        }

        var permissions_to_grant = null;
        if (formOptions.hasOwnProperty('grant_permissions_dropdown')) {
            permissions_to_grant = formOptions['grant_permissions_dropdown'];
        }

        var permissions_to_withdraw = null;
        if (formOptions.hasOwnProperty('withdraw_permissions_dropdown')) {
            permissions_to_withdraw = formOptions['withdraw_permissions_dropdown'];
        }

        if (isGrantPermissions && (!permissions_to_grant || permissions_to_grant.length == 0)) {
            $('#permissions-wizard-error').text('Please select some permissions to grant.');
            $('#permissions-wizard-error').css('display', 'block');
            return;
        }

        if (isWithdrawPermissions && (!permissions_to_withdraw || permissions_to_withdraw.length == 0)) {
            $('#permissions-wizard-error').text('Please select some permissions to withdraw.');
            $('#permissions-wizard-error').css('display', 'block');
            return;
        }

        var resourceIdentifier = this.groupIdentifier;
        if (this.teamDisplayMode == PagedDataComponent.DISPLAY_MODE_SINGLE_CHOICE &&
            this.targetObjectId && this.targetObjectId.trim().length > 0) {
            resourceIdentifier = this.targetObjectId;
        }

        if (isGrantPermissions) {
            this.spinner.show();

            this._vedavaapiApiService.grantPermissionsOnResource(this.getAccessToken(), resourceIdentifier,
                permissions_to_grant, this.selectedUsers, teamIds,
                successResponse => {
                    this.selectedUsers = [];
                    this.spinner.hide();
                    if (this.permissionsComponent) {
                        this.permissionsComponent.clearSelections();
                    }
                    this.insertAlertMessage('success-alert-parent', 'Success! Permissions granted.');
                    this.populateData(this.getSearchString());
                },
                errorMessage => {
                    this.spinner.hide();
                    if (!errorMessage)
                        return;
                    this.insertAlertMessage('failure-alert-parent', errorMessage);

                });
        } else if (isWithdrawPermissions) {
            this.spinner.show();
            this._vedavaapiApiService.withdrawPermissionsOnResource(this.getAccessToken(), resourceIdentifier,
                permissions_to_withdraw, this.selectedUsers, teamIds,
                successResponse => {
                    this.selectedUsers = [];
                    this.spinner.hide();
                    if (this.permissionsComponent) {
                        this.permissionsComponent.clearSelections();
                    }
                    this.insertAlertMessage('success-alert-parent', 'Success! Permissions Withdrawn.');
                    this.populateData(this.getSearchString());
                },
                errorMessage => {
                    this.spinner.hide();
                    if (!errorMessage)
                        return;
                    this.insertAlertMessage('failure-alert-parent', errorMessage);

                });
        }



    }


    /**
     * Method is called when an action Button is clicked
     * @param elementId Id of the Button
     * @param action Action to be performed.
     */
    private onActionButtonClicked(elementId: string) {
        if (elementId == null) {
            return;
        }

        var action = "add";
        if (this.isMemberPartOfGroup(elementId)) {
            action = "remove";
        }
        var access_token = this.getAccessToken();
        if (!access_token || access_token.trim().length == 0) {
            this.showErrorMessage('Please login to see users in this team.');
            return;
        }

        var memberIds = [];
        memberIds.push(elementId);

        var successCallback = successResponse => {
            this.spinner.hide();
            this.groupMemberIds = successResponse;
            if (action === 'add') {
                $('#btn_' + elementId).text('Remove');
            } else if (action === 'remove') {
                $('#btn_' + elementId).text('Add');
            }
        };

        var errorCallback = errorMessage => {
            this.spinner.hide();
            this.showErrorMessage(errorMessage);
        };

        switch (action) {
            case 'add': {
                this.spinner.show();
                this._groupsApiService.addMembersToGroup(access_token, this.groupIdentifier,
                    memberIds,
                    successCallback,
                    errorCallback);
                break;
            }

            case 'remove': {
                this.spinner.show();
                this._groupsApiService.removeMembersFromGroup(access_token, this.groupIdentifier,
                    memberIds,
                    successCallback,
                    errorCallback);
                break;
            }

            default:
                break;
        }
    }

    /**
     * Fetches and displays the data in a paginated manner.
     * @param search_string 
     */
    protected populateData(search_string: string) {
        var that = this;
        $('.new-button').click(callback => {
            if (this.groupIdentifier)
                that._router.navigate(['create', 'User'], { queryParams: { team_id: this.groupIdentifier } });
            else
                that._router.navigate(['create', 'User'])
        });



        this._route.queryParamMap.subscribe(queryParams => {
            var teamName = queryParams.get("name");
            if (teamName != null && teamName.trim().length > 0) {
                $('#span_team_name').text(teamName.trim());
            }//if
        });

        this._route.params.subscribe(params => {
            if (params.hasOwnProperty('team_id') && params['team_id']) {

                if (params.hasOwnProperty('object_id')) {
                    this.targetObjectId = params['object_id'] || null;
                }

                if (params.hasOwnProperty('team_display_mode')) {
                    this.teamDisplayMode = params['team_display_mode'];
                }//if


                if (that._router.url) {
                    if (that._router.url.indexOf('/addremove') != -1) {
                        that.displayType = UserListComponent.DISPLAY_TYPE_ADD_REMOVE_MEMBERS;
                    } else if (that._router.url.indexOf('/permissions') != -1) {
                        that.displayType = UserListComponent.DISPLAY_TYPE_FOR_PERMISSIONS;
                    }
                }
                if (search_string && search_string.trim().length > 0 && !this.isPageClicked){
                    this.startIndex = 0;
                    this.isPageClicked = true;
                }else if(search_string == null){
                    this.startIndex = 0;
                    this.isPageClicked = false;
                }
                that.fetchUsers(search_string, params['team_id']);
            }
        });
    }//end of method

    /**
     * Fetches the users from the Server.
     * @param search_string
     * @param teamId 
     */
    private fetchUsers(search_string: string, teamId: string) {

        this.groupIdentifier = teamId;
        this.hideAllUIElements();
        $('.pagination-row').css('display', 'none');
        $('.new-button').css('display', 'none');

        //Setting the Default Table Properties
        this.authorized = this.isAuthorized();
        if (!this.authorized) {
            this.showErrorMessage('Please login to see users in this team.');
            return;
        }
        var access_token = this.getAccessToken();
        if (!access_token || access_token.trim().length == 0) {
            this.showErrorMessage('Please login to see users in this team.');
            return;
        }

        $('#permissions-wizard').css('display', 'block');
        $('#loading-gif-icon').css('display', 'block');
        $('#loading-gif-text').text('Fetching users ...');

        var successCallback = successResponse => {
            $('#loading-gif-icon').css('display', 'none');
            this.extractUsersDetails(successResponse);
            $('.new-button').css('display', 'block');
            $('.pagination-row').css('display', 'flex');
        };

        var errorCallback = errorMessage => {
            this.hideAllUIElements();
            this.showErrorMessage(errorMessage);

        };

        switch (this.displayType) {

            case UserListComponent.DISPLAY_TYPE_ADD_REMOVE_MEMBERS: {
                //fetch all the users so as to add or remove them from the team/group.
                if (this.startIndex == 0) {
                    this._groupsApiService.getGroupMembers(access_token, this.groupIdentifier,
                        successResponse => {
                            this.groupMemberIds = successResponse;
                            this._userApiService.getUserList(access_token, search_string, this.startIndex, PagedDataComponent.PAGE_COUNT,
                                successCallback, errorCallback);
                        },
                        errorMessage => {
                            errorCallback(errorMessage);
                        });
                } else {
                    this._userApiService.getUserList(access_token, search_string, this.startIndex * PagedDataComponent.PAGE_COUNT, PagedDataComponent.PAGE_COUNT,
                        successCallback, errorCallback);
                }
                break;
            }

            default: {
                //Fetch team members.
                this._groupsApiService.getUsersOfGroup(access_token, this.groupIdentifier, search_string,
                    this.startIndex * PagedDataComponent.PAGE_COUNT, PagedDataComponent.PAGE_COUNT,
                    successCallback,
                    errorCallback);
                break;
            }
        }
    }

    /**
     * Returns true if the Member is already part of the group.
     * @param memberId 
     */
    private isMemberPartOfGroup(memberId: string) {
        if (memberId == null || this.groupMemberIds == null) {
            return false;
        }
        return this.groupMemberIds.includes(memberId);
    }


    /**
     * Extracts the Users attributes from the response.
     * @param successResponse 
     */
    private extractUsersDetails(successResponse: any) {
        if (!successResponse || successResponse.length == 0
            || !successResponse.hasOwnProperty('items')) {
            this.hideAllUIElements();
            if (this.displayType == UserListComponent.DISPLAY_TYPE_ADD_REMOVE_MEMBERS) {
                this.showErrorMessage('No users to add. Please click on the new button on top to create a new User.');
            } else {
                this.showErrorMessage('No users present in this team. Please click on the new button on top to create a new User.');
            }
            return;
        }

        if (successResponse.hasOwnProperty("total_count")) {
            this.totalNumber = successResponse['total_count'];
        }

        this.usersAttributes = [];
        let userListResponse = successResponse['items'];
        var userObject: any;
        for (let index = 0; index < userListResponse.length; index++) {
            userObject = userListResponse[index];
            if (userObject == null) {
                continue;
            }
            var name: string = null, _id: string = null, email: string = null;
            if (userObject.hasOwnProperty('_id')) {
                _id = userObject['_id'];
            }
            if (_id == null) {
                continue;
            }

            if (userObject.hasOwnProperty('name')) {
                name = userObject['name'];
            }

            if (name == null) {
                name = '';
            }
            if (userObject.hasOwnProperty('email')) {
                email = userObject['email'];
            }
            if (email == null) {
                email = '';
            }

            var isPartOfGroup = this.isMemberPartOfGroup(_id);
            let resolvedPermissions = userObject.resolvedPermissions || {};
            this.usersAttributes.push({
                "_id": _id, "name": name, "email": email,
                "isPartOfGroup": isPartOfGroup,
                "is_me": this.getCurrentUserId() === _id,
                "is_read": resolvedPermissions.read,
                "is_update_links": resolvedPermissions.updateLinks,
                "is_delete": resolvedPermissions.delete,
                "is_update_content": resolvedPermissions.updateContent
            });
        }
        $('.search-container').css('display', 'block');
        $('#data-table').css('display', 'inline-table');
        // const table: any = $('#data-table');
        // this.dataTable = table.DataTable({ searching: false, paging: false, info: false, ordering: false });
    }

    /**
     * Callback when users select an action.
     */
    private onActionSelected(user) {

        if (!user.hasOwnProperty('_id') || user['_id'].trim().length == 0) {
            return;
        }
        var userId = user['_id'];
        var name = null;
        if (user.hasOwnProperty('name')) {
            name = user['name'];
        }
        if (!name) {
            name = '';
        }
        var selectedAction = $('#select_' + userId + ' option:selected').val();
        if (selectedAction == null) {
            return;
        }

        switch (selectedAction) {
            case 'delete': {
                alert("Delete feature implemented. But not calling the method due to it's risky usage. The feature will exposed shortly to the users.");
            }
                break;
            case 'edit':{
                this._router.navigate(['edit','User',userId], { queryParams: { "edit-object": "true" } });
                break;
            }
            default:
                break;

        }//if

    }


    /**
    * Deletes the User Account from the Team.
    * @param user_identifier 
    */
    protected delete(user_identifier: string) {
        if (!user_identifier || user_identifier.trim().length == 0) {
            this.insertAlertMessage('failure-alert-parent', "User credentials missing.");
            return;
        }//if
        this.spinner.show();
        this._userApiService.deleteUser(this.getAccessToken(), user_identifier,
            successResponse => {
                this.spinner.hide();
                this.insertAlertMessage('success-alert-parent', 'Success! The User account has been deleted.');
                this.populateData(this.getSearchString());
            },
            errorMessage => {
                this.spinner.hide();
                this.insertAlertMessage('failure-alert-parent', errorMessage);
            });
    }


    /**
     * Page Changed callback
     * @param pageClicked 
     */
    private pageChanged(pageClicked) {
        this.startIndex = pageClicked - 1;
        this.usersAttributes = [];
        this.fetchUsers(this.getSearchString(), this.groupIdentifier);
    }


}//end of class
