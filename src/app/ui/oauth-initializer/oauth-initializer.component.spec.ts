import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OauthInitializerComponent } from './oauth-initializer.component';

describe('OauthInitializerComponent', () => {
    let component: OauthInitializerComponent;
    let fixture: ComponentFixture<OauthInitializerComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [OauthInitializerComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(OauthInitializerComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
