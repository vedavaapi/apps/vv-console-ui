import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import * as $ from 'jquery';
import { EndpointsService } from 'src/app/endpoints.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { OauthHelperService } from 'src/app/services/oauth-helper.service';
import { Location } from '@angular/common'


@Component({
    selector: 'app-oauth-initializer',
    templateUrl: './oauth-initializer.component.html',
    styleUrls: ['./oauth-initializer.component.scss']
})


export class OauthInitializerComponent implements OnInit {

    searchParams: URLSearchParams;
    explicitlyConversate = 'false';
    openedInNewWindow = 'false'
    redirectUri = '';
    vvSiteUrl = '';
    additionalState = '';

    constructor(private oauthHelperService: OauthHelperService, private endpointService: EndpointsService, private localStorageService: LocalStorageService, private _loc: Location) {
        this.searchParams = new URLSearchParams(document.location.search.substring(1));
        this.explicitlyConversate = this.searchParams.get('explicitly_conversate') || this.explicitlyConversate;
        this.openedInNewWindow = this.searchParams.get('opened_in_new_window') || this.openedInNewWindow;
        this.redirectUri = this.searchParams.get('redirect_uri') || this.redirectUri;
        this.vvSiteUrl = this.searchParams.get('vv_site_url') || null;
        this.additionalState = this.searchParams.get('additional_state') || this.additionalState;
        this.computeVvSiteUrl();
    }

    computeVvSiteUrl() {
        this.vvSiteUrl = this.vvSiteUrl ? this.vvSiteUrl : this.localStorageService.getValue('server_url');
        if (this.vvSiteUrl.endsWith('/')) {
            this.vvSiteUrl = this.vvSiteUrl.substring(0, this.vvSiteUrl.length - 1);
        }
    }

    getUrlParts(url: string) {
    let a = document.createElement('a');
    a.href = url;

    let urlDetails = {
        href: a.href,
        host: a.host,
        hostname: a.hostname,
        port: a.port,
        pathname: a.pathname,
        protocol: a.protocol,
        hash: a.hash,
        search: a.search
    };
    a.remove();
    return urlDetails;
}

    initializeAuthorizationFlow() {

        let clientCredsForPlatform = this.oauthHelperService.getPersistedOauthClientCreds(this.vvSiteUrl);

        let callbackState = JSON.stringify({
            "redirect_uri": this.redirectUri,
            "opened_in_new_window": this.openedInNewWindow,
            "vv_site_url": this.vvSiteUrl,
            "additional_state": this.additionalState
        });

        let redirectQueryParams = {
            "client_id": clientCredsForPlatform['client_id'],
            "response_type": "token",
            "scope": "vedavaapi.root",
            "redirect_uri": encodeURIComponent(this.getUrlParts('oauth_callback').href),
            "state": encodeURIComponent(callbackState)
        };

        let qstr = Object.keys(redirectQueryParams).reduce((ps, cs) => `${ps}&${cs}=${redirectQueryParams[cs]}`, '').substring(1);
        let authorizationRedirectUri = `${this.vvSiteUrl}/accounts/v1/oauth/authorize?${qstr}`;
        window.location.replace(authorizationRedirectUri);
    }

    onExplicitClick() {
        document.getElementById('explicit-auth-block').style.display = 'none';
        this.initializeAuthorizationFlow();
    }

    ngOnInit() {
        if(this.explicitlyConversate == 'false') {
            this.initializeAuthorizationFlow();
        }
        else {
            $("#explicit-auth-block").css({"display": "block"});
        }
    }
}
