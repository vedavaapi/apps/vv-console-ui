import { Component, OnInit, ViewChild, ElementRef, Renderer2, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as $ from 'jquery';
import { NgxSpinnerService } from 'ngx-spinner';
import { EndpointsService } from 'src/app/endpoints.service';
import { OauthHelperService } from 'src/app/services/oauth-helper.service';
import { from } from 'rxjs';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { VedavaapiShellHelperService } from 'src/app/services/vedavaapi-shell-helper.service';
import { ScannedBook } from 'src/app/services/models';
import { BooksService } from 'src/app/services/books-service';

@Component({
  selector: 'app-book-information-page',
  templateUrl: './book-information-page.component.html',
  styleUrls: ['./book-information-page.component.scss']
})
export class BookInformationPageComponent implements OnInit, AfterViewInit {

  private bookId: string = null;

  private iframeHtml: string = null;

  constructor(private _Activatedroute:ActivatedRoute, 
    private booksService: BooksService, 
    private _endpointsService: EndpointsService, 
    private _spinner: NgxSpinnerService,
    private oauthHelperService: OauthHelperService,
    private localStorageService: LocalStorageService,
    private vedavaapiShellHelperService: VedavaapiShellHelperService) {
    this.bookId = this._Activatedroute.snapshot.params['id'];
      let vvSiteUrl = this.localStorageService.getValue('server_url');
  }

  ngOnInit() {
    if(this.bookId == null || this.bookId.trim().length == 0){
     this.displayErrorMessage("Book Id is missing from the page URL.");
     return;
    }
    this.initializeMiradorViewer(this.bookId);
    this.getBookDetails(this.bookId);
    
  }

    ngAfterViewInit() {
        let miradorFrameWindow = document.getElementById('mirador-iframe')['contentWindow'];
        this.vedavaapiShellHelperService.setupCommunication(miradorFrameWindow);
    }
  

    private getIIIFViewerUrl(bookId: string) {
        let vvSiteUrl = this.localStorageService.getValue('server_url');
        let registeredApplications = this.oauthHelperService.getApplications(vvSiteUrl)
        if (!registeredApplications) {
            return null;
        }
        let application = null;
        for (let item of registeredApplications) {
            if (item['handledTypes'] && Array.isArray(item['handledTypes']) && item['handledTypes'].indexOf('ScannedBook') >= 0
            ) {
                application = item;
                break;
            }
        }
        if (!application) {
            return null;
        }
        let applicationUrl = application['url'];
        return applicationUrl + "?book_id=" + bookId;
    }


  /**
   * Initializes the Mirador Viewer.
   * @param bookId string
   */
  private initializeMiradorViewer(bookId: string) {
    let vvSiteUrl = this.localStorageService.getValue("server_url");
    let iiifViewerUrl = this.getIIIFViewerUrl(this.bookId);
    if(iiifViewerUrl) {
        this.iframeHtml = '<iframe title="Mirador" id="mirador-iframe" src="'
            + this.getIIIFViewerUrl(this.bookId) + `&vv_site_url=${vvSiteUrl}`
            + '" allowfullscreen="true"'
            + ' width="100%" style="height:100vh;border: 0px;"'
            + ' webkitallowfullscreen="true" mozallowfullscreen="true"></iframe>';
    }
    else {
        this.iframeHtml = 'No IIIF compatible viewer registered.';
    }
  }

  /**
   * Slides to the Mirador Viewer
   */
  private slideToViewer(){
    $('html,body').animate({
      scrollTop: $("#mirador-viewer").offset().top},
      'slow');
    return false;
  }

  /**
   * Displays the Error message
   * @param message string
   */
  private displayErrorMessage(message: string){
    $("#heading-no-content").css('display', 'block');
    if(message == null){
      return;
    }
    $("#heading-no-content").text(message);
  }
  
  
  /**
   * Fetches Book Details from the Server.
   * @param book_id string
   */
  private getBookDetails(bookId: string){

    this._spinner.show();
   
    $("#heading-no-content").show();
    $("#heading-no-content").text("Fetching book details...");

    var that = this;
    let successCallback = (response: ScannedBook) => {
        that._spinner.hide();
        let book = response;
        $(".div-book-content").hide();
        /* 
        $('#text-book-title').text(book.getTitle() || '');
        
        let metadataMap = book.getMetadataMap() || {};
        if(metadataMap['description']) {
            $('#text-book-description').text(metadataMap['description']);
        }

        let coverImageUrl = book.cover ? book.cover.getIIIFOrDefaultUrl(this._endpointsService.getBaseUrl(), 'full', ',200', '0', 'default', 'png') : "assets/default-book-īmg.png";

        $('#image-book-thumbnail').attr('src', coverImageUrl);
        $('#text-book-authors').text("by " + (book.getAuthors() || []).join(', '));
        */
    };

    let errorCallback = (error: any) => {
        console.log('errior occured', { error });
        that._spinner.hide();
        that.displayErrorMessage(error);
        return;
    };

    this.booksService.getBook(this._endpointsService.getBaseUrl(), bookId, undefined, successCallback, errorCallback);
  }

}//end of class
