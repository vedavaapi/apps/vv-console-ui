import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout/layout.component';
import { FooterComponent } from './footer/footer.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from './home/home.component';
import { SignedInHeaderComponent } from './signed-in-header/signed-in-header.component';
import { SafePipe } from './safe.pipe';
import { AnnotatorLoginComponent } from './annotator-login/annotator-login.component';
import { AppsListComponent, GetApplicationsPipe } from './apps-list/apps-list.component';
import { LibrarySelectorComponent } from './library-selector/library-selector.component';
import { HeaderComponent } from './header/header.component';
import { UnifiedHeaderComponent } from './unified-header/unified-header.component';
import { AddBookComponent } from './add-book/add-book.component';
import { AddTextDocComponent } from './add-textdoc/add-textdoc.component';
import { IIIFImporterComponent } from './iiif-importer/iiif-importer.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { ApplicationInfoPageComponent } from './application-info-page/application-info-page.component';
import { LearnerLoginPageComponent } from './learner-login-page/learner-login-page.component';
import { ViewAllBooksComponent } from './view-all-books/view-all-books.component';
import { EditBookComponent } from './edit-book/edit-book.component';
import { ModalDialogModule } from 'ngx-modal-dialog';
import { BookInformationPageComponent } from './book-information-page/book-information-page.component';
import { ObjectViewerComponent } from './view-object/view-object.component';
import { ModalModule, BsModalRef } from 'ngx-bootstrap/modal';
import { OauthInitializerComponent } from './oauth-initializer/oauth-initializer.component';
import { OauthCallbackComponent } from './oauth-callback/oauth-callback.component';
import { ApplicationPageComponent } from './application-page/application-page.component';
import { NgxMultiLineEllipsisModule } from "ngx-multi-line-ellipsis";
import { EllipsisModule } from 'ngx-ellipsis';
import { BookListComponent } from './book-list/book-list.component';
import {Bootstrap4FrameworkModule} from 'angular6-json-schema-form';
import { UserListComponent } from './user-list/user-list.component';
import { LoadingGifTextComponent } from './widgets/loading-gif-text/loading-gif-text.component';
import { SearchBarComponent } from './widgets/search-bar/search-bar.component';
import { GroupListComponent } from './group-list/group-list.component';
import { PagedDataComponent } from './paged-data/paged-data.component';
import { NewButtonComponent } from './widgets/new-button/new-button.component';
import { CreateObjectComponent } from './create-object/create-object.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { InPageAlertsComponent } from './widgets/in-page-alerts/in-page-alerts.component'; 
import {TypeaheadModule} from "ngx-type-ahead";
import { PermissionsDropdownComponent } from './widgets/permissions-dropdown/permissions-dropdown.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LibraryListComponent } from './library-list/library-list.component';
import { ObjectsListComponent } from './objects-list/objects-list.component';
import { SiteUrlComponent } from './site-url/site-url.component';
import { OpenWithComponent } from './open-with/open-with.copmponent';
// import { JSONViewer } from './json-view/json-view.component';
import { from } from 'rxjs';

@NgModule({
  imports: [
    CommonModule,
    NgxSpinnerModule,
    SlickCarouselModule,
    ModalDialogModule.forRoot(),
    ModalModule.forRoot(),
    NgxMultiLineEllipsisModule,
    EllipsisModule,
    Bootstrap4FrameworkModule,
    NgxPaginationModule,
    TypeaheadModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [LayoutComponent, 
    AppsListComponent,
    LibrarySelectorComponent,
    GetApplicationsPipe,
    HeaderComponent,
    SignedInHeaderComponent,
    UnifiedHeaderComponent,
    FooterComponent, 
    DashboardComponent,
    ApplicationPageComponent,
    HomeComponent, 
    SafePipe,
    AnnotatorLoginComponent,
    AddBookComponent,
    AddTextDocComponent,
    IIIFImporterComponent,
    ApplicationInfoPageComponent,
    LearnerLoginPageComponent,
    ViewAllBooksComponent,
    EditBookComponent,
    BookInformationPageComponent,
    ObjectViewerComponent,
    OauthInitializerComponent,
    OauthCallbackComponent,
    BookListComponent,
    UserListComponent,
    LoadingGifTextComponent,
    SearchBarComponent,
    GroupListComponent,
    PagedDataComponent,
    NewButtonComponent,
    CreateObjectComponent,
    InPageAlertsComponent,
    PermissionsDropdownComponent,
    LibraryListComponent,
    ObjectsListComponent,
    SiteUrlComponent,
    OpenWithComponent,
    // JSONViewer
    ],
  exports: [LayoutComponent],
  providers: [
    BsModalRef,
],
})
export class UiModule { }
