import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import * as $ from 'jquery';
import { EndpointsService } from 'src/app/endpoints.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';
// import { OauthHelperService } from 'src/app/services/oauth-helper.service';


@Component({
    selector: 'app-oauth-callback',
    templateUrl: './oauth-callback.component.html',
    styleUrls: ['./oauth-callback.component.scss']
})

export class OauthCallbackComponent implements OnInit {
    callbackParams: URLSearchParams;
    openedInNewWindow = 'false'
    uiCallbackState = null;
    redirectUrl = '';
    fullRedirectUrl: string;
    vvSiteUrl: string;
    additionalState ='';

    constructor(private endpointService: EndpointsService, private localStorageService: LocalStorageService) {
        this.callbackParams = new URLSearchParams(document.location.search.substring(1));
        this.uiCallbackState = JSON.parse(this.callbackParams.get('state'));
        this.redirectUrl = this.uiCallbackState['redirect_uri'] || null;
        if(this.redirectUrl) {
            this.fullRedirectUrl = `${this.redirectUrl}?${document.location.hash.substring(1)}`;
        }
        this.openedInNewWindow = this.uiCallbackState['opened_in_new_window'] || 'false';
        this.vvSiteUrl = this.callbackParams.get('site_url');
        this.additionalState = this.uiCallbackState['additional_state'];
    }

    getMarshaledAuthorizedCreds() {
        let authorizedCreds = {
            "access_token": this.callbackParams.get('access_token'),
            "token_type": this.callbackParams.get('token_type'),
            "expires_in": this.callbackParams.get('expires_in'),
            "scope": this.callbackParams.get('scope'),
            "granted_time": Date.now()
        }
        return authorizedCreds;
        // this.oauthHelperService.persistAuthorizedCreds(authorizedCreds);
    }

    retrieveUserDetails(creds: object, successCallback: (userDetails: object) => void) {
        console.log({"vvSiteUrl": this.vvSiteUrl});
        let meUrl = `${this.vvSiteUrl}/accounts/v1/me`;
        let projection = {"permissions": 0};
        let fullMeUrl = `${meUrl}?projection=${encodeURIComponent(JSON.stringify(projection))}`;
        console.log({fullMeUrl});
        let accessToken = creds['access_token'];
        let authorizedHeaders = {
            "Authorization": `Bearer ${accessToken}`
        }
        fetch(fullMeUrl, {
            headers: new Headers(authorizedHeaders)
        })
            .then((response) => {
                response.json().then(successCallback)
            })
    }

    performRedirection() {
        if(this.fullRedirectUrl) {
            window.location.replace(this.fullRedirectUrl);
        }
    }

    postMessage(userDetails: any) {
        let oauthBroadcastChannel = new BroadcastChannel('oauth_callback_channel');
        let messageStatus = {
            "success": true,
            "creds": this.getMarshaledAuthorizedCreds(),
            "vv_site_url": this.vvSiteUrl,
            "user_details": userDetails,
            "additional_state": this.additionalState
        }
        oauthBroadcastChannel.postMessage(messageStatus);
    }

    ngOnInit() {
        // this.persistAuthorizedCreds();
        
        if(this.openedInNewWindow) {
            let creds = this.getMarshaledAuthorizedCreds();
            this.retrieveUserDetails(creds, (userDetails) => {this.postMessage(userDetails)});
        }
        else if (this.redirectUrl) {
            this.performRedirection();
        }
    }
}
