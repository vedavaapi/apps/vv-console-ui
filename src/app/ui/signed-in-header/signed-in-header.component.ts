import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { EndpointsService } from 'src/app/endpoints.service';
import { Router } from '@angular/router';
import { OauthHelperService } from 'src/app/services/oauth-helper.service'
import { from } from 'rxjs';
import { ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-signed-in-header',
  templateUrl: './signed-in-header.component.html',
  styleUrls: ['./signed-in-header.component.scss']
})
export class SignedInHeaderComponent implements OnInit {

  private authorized = false;
  private accessToken: string;
  private user = null;

  constructor(private endpointService: EndpointsService, private http: HttpClient
    , private router: Router, private oauthHelperService: OauthHelperService, private cdRef: ChangeDetectorRef) {
        this.subscribeAuthorizationChangeBroadcast();
    }
    
    private subscribeAuthorizationChangeBroadcast() {
        let bc = new BroadcastChannel(this.oauthHelperService.authorizationChangeBroadcastChannelName);
        bc.onmessage = ({ data }) => {
            if (data) {
                this.updateAuthorizedStatus();
            }
        }
    }

    updateAuthorizedStatus() {
        let authorizedCreds = this.oauthHelperService.getAuthorizedCreds();
        this.cdRef.detectChanges();
        this.authorized = Boolean(authorizedCreds);
        this.user = this.authorized ? this.oauthHelperService.getAuthorizedUserDetails() : null;
        this.accessToken = authorizedCreds['access_token'];
        this.cdRef.detectChanges();
    }

    ngOnInit() {
        this.updateAuthorizedStatus();
    }

  /**
   * Simple Method to logout from Service. Easy to make call from html file
   */
  logoutFromService(){
    console.log('logout called');
    this.oauthHelperService.unAuthorizeUser();
  }


}
