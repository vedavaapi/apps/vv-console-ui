import { Component, OnInit, ViewChild, ElementRef, Renderer2, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as $ from 'jquery';
import { NgxSpinnerService } from 'ngx-spinner';
import { OauthHelperService } from 'src/app/services/oauth-helper.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { ObjstoreApiService } from 'src/app/services/objstore-api.service'
import { VedavaapiShellHelperService } from 'src/app/services/vedavaapi-shell-helper.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'app-view-object-page',
    templateUrl: './view-object.component.html',
    styleUrls: ['./view-object.component.scss']
})
export class ObjectViewerComponent implements OnInit, AfterViewInit {

    private objectId: string = null;
    private object: any;
    private objectGlance: any;

    private vvSiteUrl: string;
    private companionUrl: string;
    private referrers = {};

    constructor(private _Activatedroute: ActivatedRoute,
        private _spinner: NgxSpinnerService,
        private sanitizer: DomSanitizer,
        private objstore: ObjstoreApiService,
        private oauthHelperService: OauthHelperService,
        private localStorageService: LocalStorageService,
        private vedavaapiShellHelperService: VedavaapiShellHelperService) {
        this.objectId = this._Activatedroute.snapshot.params['id'];
        this.vvSiteUrl = this.localStorageService.getValue('server_url');
    }

    ngOnInit() {
        if (this.objectId == null || this.objectId.trim().length == 0) {
            this.displayErrorMessage("ObjectId Id is missing from the page URL.");
            return;
        }
        let a = document.createElement('a');
        a.href = this.oauthHelperService.companionUrl;
        this.companionUrl = a.href;
        a.remove();

        this.getObject();
    }

    ngAfterViewInit() {
    }


    /**
     * Displays the Error message
     * @param message string
     */
    private displayErrorMessage(message: string) {
        $("#heading-no-content").css('display', 'block');
        if (message == null) {
            return;
        }
        $("#heading-no-content").html(message);
    }

    private htmlify(json: any) {
        if(typeof json == 'string') {
            return /^[0-9A-F]{24}$/i.test(json) ? `<a href='object/${json}'>${json}</a>` : json;
        }
        if(Array.isArray(json)) {
            return json.map(value => this.htmlify(value));
        }
        if(Object.prototype.isPrototypeOf(json)) {
            let obj = {};
            Object.keys(json).forEach((key) => {
                let value =json[key];
                obj[key] = this.htmlify(value); 
            });
            return obj;
        }
        return json;
    }

    private showObject() {
        document.getElementById('obj-navigator').style.display = 'flex';
        let codeElem = document.getElementById('view-object-json');
        codeElem.innerHTML = JSON.stringify(this.htmlify(this.object), null, 2);
    }

    private computeObjectGlance() {
        let name = this.object.name || this.object.title || this.object._id;
        if (Object.prototype.isPrototypeOf.call(Object.prototype, name) && name.chars) {
            name = name.chars;
        }
        let parent = this.object.source || this.object.target;
        if (parent) {
            parent = Array.isArray(parent) ? parent : [parent];
            parent =  '[' + parent.map(id => `<a href="object/${id}">${id}</a>`).join(', ') + ']';
        }
        this.objectGlance = {
            _id: this.object._id,
            'class': this.object.jsonClass,
            name: name,
            classLabel: this.object.jsonClassLabel,
            parent: parent
        };
        Object.keys(this.objectGlance).forEach((k) => {
            if (!this.objectGlance[k]) {
                delete this.objectGlance[k];
            }
        });
    }


    /**
     * Fetches Object Details from the Server.
     * @param objectId string
     */
    private getObject() {

        this._spinner.show();

        $("#heading-no-content").show();
        $("#heading-no-content").text("Fetching object details...");

        var that = this;
        let successCallback = (response: any) => {
            this.object = response
            that._spinner.hide();
            this.computeObjectGlance();
            $(".div-object-content").hide();
            this.showObject();
            // this.initializeViewerApp();
        };

        let errorCallback = (error: any) => {
            console.log('error occurred', { error });
            that._spinner.hide();
            document.getElementById('obj-navigator').style.display = 'none';
            that.displayErrorMessage(`Error in retrieving object:<br><pre>${JSON.stringify(error.error, null, 2)}</pre>`);
            return;
        };

        this.objstore.getResource(this.vvSiteUrl, this.objectId, null, successCallback, errorCallback, false);
    }

    private getReferrers(referKey: string, statusElem: Element) {
        if (Object.prototype.hasOwnProperty.call(this.referrers, referKey)) {
            statusElem.innerHTML = 'retrieved successfully';
            return this.referrers[referKey];
        }
        const successCallback = (data: any) => {
            statusElem.innerHTML = 'retrieved successfully';
            this.referrers[referKey] = data.items;
        }

        const errorCallback = (error: any) => {
            statusElem.innerHTML = 'error in retrieving';
        }

        statusElem.innerHTML = 'retrieving.........';
        this.objstore.getResources(this.vvSiteUrl, {[referKey]: this.object._id}, {_id: 1, jsonClass: 1, name: 1, title: 1}, undefined, undefined, undefined, undefined, successCallback, errorCallback);
    }

    private onReferrersToggle(referKey: string, toggleElem) {
        const iconElem = toggleElem.firstChild;
        let isExpanded = iconElem.innerHTML == '▼';
        let wrapper = toggleElem.nextSibling;
        let statusElem = wrapper.firstChild;
        if (isExpanded) {
            wrapper.style.display = 'none';
            iconElem.innerText = '►';
        } else {
            wrapper.style.display = 'block';
            iconElem.innerHTML = '▼';
            this.getReferrers(referKey, statusElem);
        }
    }

}//end of class
