import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { EndpointsService } from 'src/app/endpoints.service';
import { ScannedBook, Library, joFromJson } from 'src/app/services/models';
import { Router } from '@angular/router'

@Component({
  selector: 'app-iiif-importer',
  templateUrl: './iiif-importer.component.html',
  styleUrls: ['./iiif-importer.component.scss']
})
export class IIIFImporterComponent implements OnInit {

    private currentLibraryId: string = '0';
    private currentLibrary: Library;
    private librarySelectorConfig: any;
    private importedBookId: string;

    constructor(private endpointService: EndpointsService, private http: HttpClient, private router: Router) {

      let queryParams = new URLSearchParams(document.location.search.substring(1));
      this.currentLibraryId = queryParams.get('library_id') || '0';

      this.librarySelectorConfig = {
          shouldUpdateQueryParams: true,
          selectionChangeCallback: (library) => { this.onLibraryChange(library); },
          listenAuthChanges: true,
          selected: this.currentLibraryId
      };
    }

    private onLibraryChange(library: Library) {
        this.currentLibraryId = library._id;
        this.currentLibrary = library;
    }

    private navigateToImportedBookPage() {
        if(!this.importedBookId) {
            return;
        }

        this.router.navigate(['object', this.importedBookId]);
    }

    private displayStatusBlock(statusBlockId: string) {
        let allStatusBlockIds = ["iiif-importer-import-success-status", "iiif-importer-import-error-status", "iiif-importer-import-progress-status", 'iiif-importer-import-unauthorized-status'];
        for (let blockId of allStatusBlockIds) {
            let displayStyle = (blockId == statusBlockId) ? "block" : "none";
            document.getElementById(blockId).style.display = displayStyle;
        }
    }

    private importBook() {
        let importForm: any = document.getElementById('iiif-importer-import-form');
        let importFormData = new FormData(importForm);
        importFormData.append('library', this.currentLibraryId);
        this.displayStatusBlock("iiif-importer-import-progress-status");
        let iiifImporterUrl = this.endpointService.getBaseUrl() + '/importer/v1/iiif_importer/import_task';
        this.http.post(iiifImporterUrl, importFormData)
            .subscribe(
                (response) => {
                    console.log({response});
                    window['Iresp'] = response;
                    this.displayStatusBlock("iiif-importer-import-progress-status");
                    let statusUrl = response['status_url'];
                    this.updateProgress(statusUrl);
                    // document.getElementById("iiif-importer-imported-book_id-cell").innerHTML = `${bookId}`;
                },
                (error) => {
                    console.log({ error });
                    this.importedBookId = null;
                    this.displayStatusBlock("iiif-importer-import-error-status");
                    document.getElementById("iiif-importer-import-error-response-cell").innerText = JSON.stringify(error['error'], null, 2);
                }
            );
    }

    private updateProgress(statusUrl: string) {
        let progressCell = document.getElementById('iiif-importer-progress-bar');
        let infoCell = document.getElementById('iiif-importer-progress-info');
        this.displayStatusBlock('iiif-importer-import-progress-status');

        this.http.get(statusUrl).subscribe(
            (resp) => {
                if(resp['state'] == 'FAILURE' || resp['state'] == 'Failure') {
                    this.displayStatusBlock('iiif-importer-import-error-status');
                }
                else if(resp['state'] == 'SUCCESS') {
                    this.displayStatusBlock('iiif-importer-import-success-status');
                    this.importedBookId = resp['book_id'];
                    document.getElementById("iiif-importer-imported-book_id-cell").innerHTML = `${this.importedBookId}`;
                }
                else if(resp['state'] == 'PROGRESS' || resp['state'] == 'PENDING') {
                    this.displayStatusBlock('iiif-importer-import-progress-status');
                    progressCell.innerText = '';
                    infoCell.innerText = '';
                    if (resp['status']) {
                        infoCell.innerText = resp['status'];
                    }
                    let progress = resp['current'] != undefined && resp['total'] ? resp['current'] * 100 / resp['total'] : null;
                    if (progress !== null) {
                        progressCell.innerText = `Progress: ${resp['current']}/${resp['total']}`;
                    }
                    setTimeout(() => {
                        this.updateProgress(statusUrl);
                    }, 2000);
                }
                
            },
            (error) => {
                this.displayStatusBlock('iiif-importer-import-error-status');
            }
        );
    }


    ngOnInit() {
    }


}
