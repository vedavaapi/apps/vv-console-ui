import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { PagedDataComponent } from '../paged-data/paged-data.component';

import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { DOCUMENT } from '@angular/common';
import { EndpointsService } from 'src/app/endpoints.service';
import { OauthHelperService } from 'src/app/services/oauth-helper.service';
import { GroupsApiService } from 'src/app/services/groups-api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-group-list',
    templateUrl: './group-list.component.html',
    styleUrls: ['./group-list.component.scss']
})
/**
 * The Component Displays list of groups in a paginated manner
 */
export class GroupListComponent extends PagedDataComponent {


    private teamsList: any = [];

    private selectControl: FormControl = new FormControl();

    constructor(@Inject(DOCUMENT) protected document: Document, protected endpointService: EndpointsService,
        protected oauthHelperService: OauthHelperService, protected cdRef: ChangeDetectorRef,
        private _router: Router,
        private _spinner: NgxSpinnerService,
        private _groupsApiService: GroupsApiService,
        private _route: ActivatedRoute) {
        super(document, endpointService, oauthHelperService, cdRef, _router);

    }



    ngOnInit() {
        super.ngOnInit();

        this._route.params.subscribe(params => {
            if (params.hasOwnProperty("display_mode") && params['display_mode']) {
                this.displayMode = params['display_mode'];
            }
            if (params.hasOwnProperty("object_id") && params['object_id']) {
                this.objectId = params['object_id'];
            }

        });
    }

    /**
     * Fetches and displays the data in a paginated manner.
     * @param search_string 
     */
    protected populateData(search_string: string) {
        this.userId = this.getCurrentUserId();

        this.hideAllUIElements();

        var that = this;
        $('.new-button').click(callback => {
            that._router.navigate(['create', 'Team']);
        });

        $('.pagination-row').css('display', 'none');
        $('.new-button').css('display', 'none');

        $('#loading-gif-icon').css('display', 'block');
        $('#loading-gif-text').text('Fetching teams ...');

        //Setting the Default Table Properties
        this.authorized = this.isAuthorized();
        if (!this.authorized) {
            $('#loading-gif-icon').css('display', 'none');
            this.showErrorMessage('Please login to see the teams.');
            return;
        }
        var access_token = this.getAccessToken();
        if (!access_token || access_token.trim().length == 0) {
            $('#loading-gif-icon').css('display', 'none');
            this.showErrorMessage('Please login to see the teams.');
            return;
        }
        if (search_string && search_string.trim().length > 0){
            this.startIndex = 0;
        } 
        this._groupsApiService.getMyTeams(this.getCurrentUserId(), access_token, search_string,
            this.startIndex * PagedDataComponent.PAGE_COUNT, PagedDataComponent.PAGE_COUNT,
            successResponse => {
                $('#loading-gif-icon').css('display', 'none');
                this.extractTeamsData(successResponse);
            },
            errorMessage => {
                this.hideAllUIElements();
                this.showErrorMessage(errorMessage);
            });
    }//end of method

    /**
     * Extracts Teams Data from the Response.
     * @param teams_response 
     */
    private extractTeamsData(teams_response: any) {
        if (teams_response == null || !teams_response.hasOwnProperty('items')) {
            this.hideAllUIElements();
            this.showErrorMessage('No teams present found. Please click on the new button on top to create a new team.');
            $('.pagination-row').css('display', 'none');
            $('.new-button').css('display', 'block');
            return;
        }

        if (teams_response.hasOwnProperty("total_count")) {
            this.totalNumber = teams_response['total_count'];
        }

        this.teamsList = [];
        let teamsResponse = teams_response['items'];
        var teamsObj: any;

        for (let index = 0; index < teamsResponse.length; index++) {
            teamsObj = teamsResponse[index];
            if (teamsObj == null) {
                continue;
            }
            var name: string = null, _id: string = null, description: string = null;;

            if (teamsObj.hasOwnProperty('_id')) {
                _id = teamsObj['_id'];
            }
            if (_id == null) {
                continue;
            }

            if (teamsObj.hasOwnProperty('name')) {
                name = teamsObj['name'];
            }

            if (name == null) {
                name = '';
            }

            if (teamsObj.hasOwnProperty('description')) {
                description = teamsObj['description'];
            }
            if (description == null) {
                description = '';
            }

            var membersCount = 0;
            if (teamsObj.hasOwnProperty('members')) {
                membersCount = teamsObj['members'].length;
            }

            let resolvedPermissions = teamsObj.resolvedPermissions || {};

            this.teamsList.push({
                "_id": _id, "name": name,
                "description": description, "member_count": membersCount,
                "is_member": this.isMemberOfTeam(this.userId, teamsObj),
                "is_read": resolvedPermissions.read,
                "is_update_links": resolvedPermissions.updateLinks,
                "is_update_content": resolvedPermissions.updateContent,
                "is_delete": resolvedPermissions.delete,
                "is_update_permissions": resolvedPermissions.updatePermissions
            });
        }

        $('.search-container').css('display', 'block');
        $('#data-table').css('display', 'inline-table');
        $('.pagination-row').css('display', 'flex');
        $('.new-button').css('display', 'block');
        console.log("Extract teams complete");
    }


    /**
     * User has selected an action on the group. This callback is called.
     * @param team 
     */
    private onActionSelected(team: any) {

        if (!team.hasOwnProperty('_id') || team['_id'].trim().length == 0) {
            return;
        }

        var teamId = team['_id'];
        var name = null;
        if (team.hasOwnProperty('name')) {
            name = team['name'];
        }
        if (!name) {
            name = '';
        }
        var selectedAction = $('#select_' + teamId + ' option:selected').val();

        if (selectedAction == null) {
            return;
        }

        switch (selectedAction) {

            case 'edit':{
                this._router.navigate(['edit', 'Team',teamId],{ queryParams: { "edit-object": "true" } });
                break;
            }
            case 'view': {
                this._router.navigate(['users', teamId], { queryParams: { name: name } });
            }
                break;

            case 'add_remove': {
                this._router.navigate(['users', teamId, 'addremove'], { queryParams: { name: name } });
            }
                break;

            case 'delete': {
                this.delete(teamId);
            }
                break;

            case 'update_permissions': {
                this._router.navigate(['users', teamId, 'permissions'], { queryParams: { name: name } });
            }
                break;

            default:
                break;

        }
    }

    /**
     * Deletes the Team.
     * @param team_identifier 
     */
    protected delete(team_identifier: string) {
        if (!team_identifier || team_identifier.trim().length == 0) {
            return;
        }//if
        this._spinner.show();
        this._groupsApiService.deleteTeam(this.getAccessToken(), team_identifier,
            successResponse => {
                this._spinner.hide();
                this.insertAlertMessage('success-alert-parent', 'Success! The team was deleted.');
                this.populateData(this.getSearchString());
            },
            errorMessage => {
                this._spinner.hide();
                this.insertAlertMessage('failure-alert-parent', errorMessage);
            });
    }


    /**
  * Page Changed callback
  * @param pageClicked 
  */
    private pageChanged(pageClicked) {
        this.startIndex = pageClicked - 1;
        this.populateData(this.getSearchString());
    }

    /**
     * onTeamSelected is called when a Team is Selected.
     * @param teamId 
     */
    private onTeamSelected(teamObject: any) {

        if (!teamObject || !teamObject.hasOwnProperty('_id') || teamObject['_id'].trim().length == 0
            || !this.objectId || this.displayMode == PagedDataComponent.DISPLAY_MODE_NORMAL) {
            return;
        }//if


        var name = null;
        if (teamObject.hasOwnProperty('name')) {
            name = teamObject['name'];
        }
        if (!name) {
            name = '';
        }


        this._router.navigate(['users', teamObject['_id'], 'permissions', this.displayMode, this.objectId, 'select'], { queryParams: { name: name } });

    }

}//end of class declaration
