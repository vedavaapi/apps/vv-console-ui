import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { EndpointsService } from 'src/app/endpoints.service';
import { OauthHelperService } from 'src/app/services/oauth-helper.service';
import * as $ from 'jquery';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-view-all-books',
  templateUrl: './view-all-books.component.html',
  styleUrls: ['./view-all-books.component.scss']
})
export class ViewAllBooksComponent implements OnInit {

  private bookDetails : any;
  private showCount : number = 0;

  private isSearchByTitleEnabled: boolean = false;

  //stores book images, title.
  private bookStoreData = [];

  private isEditBooks: boolean = false;

  private isBrowseBooks: boolean = false;
 
  
  constructor(@Inject(DOCUMENT) private document: Document, private endpointService: EndpointsService, private http: HttpClient, private oauthHelperService: OauthHelperService) {
    console.log("View All Books URL:" + this.document.location.href);
    var currentUrl =  this.document.location.href;
    if(currentUrl != null){
      if(currentUrl.endsWith("/edit-books")){
        this.isEditBooks = true;
      }else if (currentUrl.endsWith("dashboard/browse-books")){
        this.isBrowseBooks = true;
      }
    }
  }
   
  /**
   * Append book to the book list displayed.
   * @param imgFileId 
   * @param bookName 
   */
  private addBookDetails(bookId, imgFileId, bookName, isIIIFImage=true){
    var cover_image_url = null;

    if(imgFileId !=null){
      if(isIIIFImage) {
        cover_image_url = this.endpointService.getBaseUrl() + "/iiif_image/v1" + "/objstore/" + imgFileId + "/full/,150/0/default.png";
      }
      else {
        cover_image_url = this.endpointService.getBaseUrl() + "/objstore/v1/files/" + imgFileId;
      }
    }else{
      cover_image_url ="assets/default-book-īmg.png";   
    }
    this.bookStoreData.push({book_id: bookId, Name:bookName, img:cover_image_url});
  }

  /**
   * Fetches Books from Server.
   */
  fetchBooks(){
      $("#span-no-books-present-text").css('display', 'none');
      $('#load-more-btn-wrapper').css('display','none');
      $('#loadding-gif-icon').show();

      let searchString = $('#input-search-book-by-title').val();
      let selectorDoc = '{"jsonClass": "ScannedBook"}';
      if(this.isSearchByTitleEnabled &&  searchString != null && searchString.trim().length > 0){
        selectorDoc = '{"jsonClass": "ScannedBook", "title":  {"$regex": "^' + searchString + '.*$" }}';
      }

      let httpImageParams = new HttpParams()
        .append('selector_doc', selectorDoc)
        .append('projection', '{"permissions": 0}')
        .append('sort_doc', '[["created", -1]]')
        .append('start', this.showCount.toString())
        .append('count', '24');
      this.showCount = this.showCount + 24;

      let httpImageHeaders: HttpHeaders = new HttpHeaders({
        'Content-Type': 'application/json',
      });

      console.log('querying: ', this.endpointService.getBaseUrl() + '/objstore/v1/resources');

      this.http.get(
        this.endpointService.getBaseUrl() + '/objstore/v1/resources',
        { headers: httpImageHeaders, params: httpImageParams, })
        .subscribe(
          response => {
            $('#loadding-gif-icon').css('display', 'none');
            this.bookDetails = response;
            if (this.bookDetails != null && this.bookDetails['items'].length != 0) {
                this.bookDetails = this.bookDetails['items'];
              for (let i = 0; i < this.bookDetails.length; i++) {
                if (this.bookDetails[i].hasOwnProperty("_id") 
                  // && this.bookDetails[i].hasOwnProperty("cover") 
                  && this.bookDetails[i].hasOwnProperty("title")
                  // && this.bookDetails[i]["cover"].hasOwnProperty("data")
                  ) {
                  let bookTitle = this.bookDetails[i]["title"];
                  let hasCoverImage = Boolean(this.bookDetails[i]['cover']);
                  let isIIIFImage = Boolean(hasCoverImage && this.bookDetails[i]['cover']['implements'] && this.bookDetails[i]['cover']['implements'].indexOf('iiif_image') >= 0);
                  console.log({isIIIFImage, "d": this.bookDetails[i]['cover']});
                  this.addBookDetails(this.bookDetails[i]["_id"], hasCoverImage ? this.bookDetails[i]["cover"]["data"].slice(6): null, bookTitle, isIIIFImage);
                }//if
              }//for 
            }else{
              if(this.isSearchByTitleEnabled){
                $("#span-no-books-present-text").text("There are no books matching the search string provided.")
              }else{
                $("#span-no-books-present-text").text("No books found.")
              }
              $("#span-no-books-present-text").css('display', 'block');
            }//if-else

            if (this.bookDetails.length >= 24) {
              console.log(this.bookDetails.length);
              $('#load-more-btn-wrapper').show();
            } else {
              $('#load-more-btn-wrapper').css('display', 'none');
            }
          }
        );
  }//end of method.


  ngOnInit() {
    this.fetchBooks();
  }

  /**
   * Shows the Search bar.
   */
  showSearchBar(){
    $('#span-all-books').css('display', 'none');
    $('#div-search-bar').css('display', 'block');
  }

  /**
   * Hides the Search Bar
   */
  hideSearchBar(){
    $('#span-all-books').css('display', 'block');
    $('#div-search-bar').css('display', 'none');
    $('#input-search-book-by-title').val('');

    if(!this.isSearchByTitleEnabled){
      return;
    }
    
    this.isSearchByTitleEnabled = false;
    this.bookStoreData = [];
    $( "#div-book-list" ).load( "#div-book-list" );
    this.showCount = 0;
    this.fetchBooks();
  }

  /**
   * Searches Book By title.
   */
  searchBooksByTitle(){
    let searchString = $('#input-search-book-by-title').val();
    if(searchString == null && searchString.trim().length == 0){
      console.log("No Search String given.");
      return;
    }
    this.isSearchByTitleEnabled = true;
   
    //Remove alll existing books.
    this.bookStoreData = [];
    $( "#div-book-list" ).load( "#div-book-list" );
    this.showCount = 0;
    this.fetchBooks();
  }
}//end of class declaration

