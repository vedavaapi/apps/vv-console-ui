import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Component, OnInit, ViewChild, ViewContainerRef, Inject } from '@angular/core';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import { DOCUMENT } from '@angular/common';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { EndpointsService } from 'src/app/endpoints.service';
import { BooksService } from 'src/app/services/books-service';
import { ObjstoreApiService } from 'src/app/services/objstore-api.service'
import { ScannedBook, Person, MetadataItem, TextObj, Library } from 'src/app/services/models';


@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styleUrls: ['./edit-book.component.scss']
})

export class EditBookComponent implements OnInit {

private currentLibraryId: string = '0';
private currentLibraryName: string = '';

private currentLibrary: Library;
private librarySelectorConfig: any;

  private bookId: string;

  private  pageFilesArraySelected: Array<File> = [];

  private uploadedBookId: string = null;

  private bookTitle: string = null;

  private book: ScannedBook;

  private bookPageResponse: any;

  private selectedThumbnailFile: File;

  private isThumbnailAsPerAspectRatio: boolean = false;

  private imagePreview: any;

  private coverImageUrl: string;

  private isIIIFThumbnail = true;

  private currentPageIndex: number;

  private bookPageCount: number;

  private resourceIds:Array<any> = [];

  private slides = [];

  @ViewChild('slickModal') private slickModal;

  private selectedPageFile: File;


  private slideConfig = { "slidesToShow": 8, "slidesToScroll": 1, "loop": true,"infinite": false,"arrows": true,
  prevArrow:"<img class='a-left control-c prev slick-prev' src='assets/carousel-left-chev.png' style='top:0%;height: 39px; width: 34px;display:block;transform:none; margin-top:42px;'>",
  nextArrow:"<img class='a-right control-c next slick-next' src='assets/carousel-right-chev.png' style='top:0%;height: 39px; width: 34px;display:block; margin-top: 42px;transform:none'>",
  
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 6
      }
    },
    {
      breakpoint: 990,
      settings: {
        slidesToShow: 5
      }
    },
    {
      breakpoint: 764,
      settings: {
        slidesToShow: 3
      }
    },
    {
      breakpoint: 460,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 350,
      settings: {
        slidesToShow: 1
      }
    },
  ]
};

/**
 * Adds a Slide to the carousel
 * @param fileId 
 */
addSlide(fileId: string) {
  if(fileId != null){
    var pageUrl =  this.endpointService.getBaseUrl() + "/iiif_image/v1/"  + "objstore/" + fileId +"/full/100,/0/default.png";
    console.log("Added Page URL: " + pageUrl);
    this.slides.push({img: pageUrl});
    this.currentPageIndex++;
  }

  this.slideConfig.slidesToScroll = this.bookPageCount;
}

removeSlide() {
  this.slides.length = this.slides.length - 1;
}

slickInit(e) {
  console.log('slick initialized');
}

breakpoint(e) {
  console.log('breakpoint');
}

afterChange(e) {
  console.log('afterChange');
}

beforeChange(e) {
  console.log('beforeChange');
}

slideLeft(){
  this.currentPageIndex--;
  if(this.currentPageIndex < 0){
    this.currentPageIndex = 0;
    return;
  }//if
  this.slickModal.slickGoTo(this.currentPageIndex);
}

slideRight(){
  this.currentPageIndex++;
  if(this.currentPageIndex >=  this.slides.length){
    this.currentPageIndex = this.slides.length - 1 ;
    return;
  }//if
  this.slickModal.slickGoTo(this.currentPageIndex);
}
  
  
  /**
   * Constructor
   * @param endpointService 
   * @param http 
   * @param spinner 
   */
    constructor(@Inject(DOCUMENT) private document: Document, private modalDialogService: ModalDialogService, 
    private viewRef: ViewContainerRef,
    private endpointService: EndpointsService, 
    private booksService: BooksService,
    private objstore: ObjstoreApiService,
    private http: HttpClient, 
    private spinner: NgxSpinnerService,
    private router: Router) {
    let queryParams = new URLSearchParams(document.location.search.substring(1));
    this.currentLibraryId = queryParams.get('library_id') || '0';
    this.currentLibraryName = queryParams.get('name') || '';

    
      this.librarySelectorConfig = {
          shouldUpdateQueryParams: true,
          selectionChangeCallback: (library) => { this.onLibraryChange(library); },
          listenAuthChanges: true,
          selected: this.currentLibraryId
      };

    this.bookId = this.getUrlParameter('book_id');
    }

    private onLibraryChange(library: Library) {
        this.currentLibraryId = library._id;
        this.currentLibrary = library;
        this.currentLibraryName = library.name;
    }

  ngOnInit() {
    //First let's get the Book details.
    $("#add-book-step-1").text("Update book metadata(Step 1 of 3)");
    $("#btn-create-book-entry").css("display", "none");
    $("#btn-update-book-entry").css("display", "inline");
    this.fetchBookDetails(this.bookId);
  }


  /**
   * Returns the error code.
   * @param error 
   */
  private getResponseErrorCode(error) {
    const errorStr = JSON.stringify(error);
    let errorCode = -1;
    JSON.parse(errorStr, (key, value) => {
      if (key === 'code') {
        errorCode = value;
      }
      return value;
    });
    return errorCode;
  }



  /**
   * Method to update basic details of book
   */
  private updateBookBasicDetails() { 
    this.hideErrorDivs();

    var $book_title = $('#book_title').val();
    var $book_description = $('#book_description').val();
    var $book_author_information = $('#book_author').val();
    var $book_type = $('#book_type').val();

    if(!this.validateBasicBookDataFields($book_title, $book_author_information,
      $book_description)){
        return;
    }
    this.spinner.show();
    let authorNames: Array<string> = $book_author_information.split(',');
    let jsonClassLabel = "book";
    if ($book_type != null && $book_type.trim().length > 0) {
      jsonClassLabel = $book_type;
    }

    let book = new ScannedBook(this.currentLibraryId, {"description": $book_description}, $book_title, authorNames, undefined, jsonClassLabel);
    book['_id'] = this.bookId;

    let successCallback = (response: Array<ScannedBook>) => {
        this.spinner.hide();

        $("#upload_book_container").css("display", "none");
        $("#book_title").attr("readonly", "true");
        $("#book_type").attr("readonly", "true");
        $("#book_author").attr("readonly", "true");
        $("#book_description").attr("readonly", "true");
        $("#add_book_thumbnail_container").css("display", "block");

        this.uploadedBookId = response[0]._id;
        if (this.uploadedBookId == null) {
            this.showErrorModalDialog("Server response was incomplete. Cannot continue with the upload process.");
            return;
        }

        $("#info-book-created").text("Book details have been updated. The updated title is '" + $book_title + "'. Click on the Image to update the thumbnail.");
        this.setBookThumbnailImage(this.coverImageUrl);
        this.bookTitle = $book_title;
    };

    let errorCallback = (error: any) => {
        this.spinner.hide();
        this.displayServerErrorMessage("#error-add-book", error);
    }

    this.objstore.postResources(this.endpointService.getBaseUrl(), [book], undefined, false, successCallback, errorCallback);
  };



  /**
   * Displays the Appropriate Error message based on the Server error code.
   * @param messageHolderElId
   * @param error 
   */
  private displayServerErrorMessage(messageHolderElId: string, error: any) {

    if(messageHolderElId == null){
      return;
    }

    if(error == null){
      $(messageHolderElId).text("Unknown erorr, please contact the platform administrator or retry after some time.");
    }else{
      let errorCode = this.getResponseErrorCode(error);
      $(messageHolderElId).text(this.getErrorMessage(errorCode));
    }
    $(messageHolderElId).css("display", "block");
  }

  /**
   * Returns the Error message.
   */
  private getErrorMessage(errorCode: number): string {
    if (errorCode == 403) {
      return "Login did not occur properly. Please re-login before next attempt.";
    }
    else if (errorCode == 401) {
      return "Unauthorized call, session may have expired. Please re-login before next attempt.";
    }
    else if (errorCode == 400) {
      return "Bad Request. Please contact the platform administartor.";
    }
    else {
      return "Unknown error, please contact the platform administrator or retry after some time.";
    }
  }

  /**
   * Hides the error related messages.
   */
  private hideErrorDivs() {
    $('#book_title_minlength_error').css('display', 'none');
    $('#book_author_minlength_error').css('display', 'none');
    $('#book_description_minlength_error').css('display', 'none');
  }


    /**
   * On Thumbnail File changed callback.
   * @param event 
   */
  private onThumbnailFileChanged(event) {

    let fileSelected = event.target.files[0];
    if(fileSelected == null){
      return;
    }

    if (!fileSelected.name.endsWith('.png') && !fileSelected.name.endsWith('.jpg')
      && !fileSelected.name.endsWith('.jpeg')) {
      $('#error-upload-thumbnail').text("Please choose a proper image file. Only PNG and JPEG files are supported.");
      $('#error-upload-thumbnail').css("display", "block");
      return;
    }

    let fileSize = fileSelected.size;
    if(fileSize > (1024*1024*5)){
      $('#error-upload-thumbnail').text("The Size of the image file is greater than 5 MB. Please retry with an image of lesser size.");
      $('#error-upload-thumbnail').css("display", "block");
      return;
    }

    this.selectedThumbnailFile = fileSelected;
    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = reader.result;
      $("#img_book_thumbnail").attr('src', this.imagePreview);

      var img = new Image();
      img.onload = () => {
          if((img.width/img.height) == (2/3)){
            this.isThumbnailAsPerAspectRatio = true;
          }
      };

      img.src = reader.result as string;
    };
    reader.readAsDataURL(fileSelected);
  }
  

  /**
   * Method updates the thumbnail of the Book.
   */
  private onUpdateThumbnail() {
    if (this.bookId == null || this.uploadedBookId.trim().length == 0) {
      this.showErrorModalDialog("Book Id is missing. The Request URL is malformed.");
      return;
    }

    if (this.selectedThumbnailFile == null) {
      $('#error-upload-thumbnail').text("Please select a PNG or JPG file to upload.");
      $('#error-upload-thumbnail').css("display", "block");
      return;
    }

    if (this.selectedThumbnailFile.name == null) {
      $('#error-upload-thumbnail').text("The file seems to be corrupt. Please select a different file.");
      $('#error-upload-thumbnail').css("display", "block");
      return;
    }

    if (!this.selectedThumbnailFile.name.endsWith('.png') && !this.selectedThumbnailFile.name.endsWith('.jpg')
      && !this.selectedThumbnailFile.name.endsWith('.jpeg')) {
      $('#error-upload-thumbnail').text("Please choose a proper image file. Only PNG and JPEG files are supported.");
      $('#error-upload-thumbnail').css("display", "block");
      return;
    }

    if(this.selectedThumbnailFile.size > (1024*1024*5)){
      $('#error-upload-thumbnail').text("The Size of the image file is greater than 5 MB. Please retry with an image of lesser size.");
      $('#error-upload-thumbnail').css("display", "block");
      return;
    }
    

    /*if(this.bookThumbnailId == null){
      $('#error-upload-thumbnail').text("Unable to process requests as thumbnail information is missing. Please retry or contact the platform administrator.");
      $('#error-upload-thumbnail').css("display", "block");
      return;
    }//if*/

    if(!this.isThumbnailAsPerAspectRatio){
      $('#error-upload-thumbnail').text("The thumbnail image should be in the aspect ratio 2:3(width:height). Please retry with an image that matches the aspect ratio.");
      $('#error-upload-thumbnail').css("display", "block");
      return;
    }
    //Content-Type not required;
    // https://stackoverflow.com/questions/46787612/file-upload-angular2-via-multipart-form-data-400-error

    this.spinner.show();
    const httpUploadOptions = {
      headers: new HttpHeaders(
        { 'Accept': 'application/json' })
      };

      let coverPageOOLDBlankId = `_:cover_page_oold`;
      let coverPageOOLD = {
          jsonClass: "StillImage",
          source: this.uploadedBookId,
          namespace: "_vedavaapi",
          identifier: this.selectedThumbnailFile.name
      };

      let bookJson = {
          _id: this.uploadedBookId,
          jsonClass: 'ScannedBook',
          cover: {
              jsonClass: 'StillImageRepresentation',
              data: `_OOLD:${coverPageOOLDBlankId}`
          }
      };

      let graph = {
          [this.uploadedBookId]: bookJson
      };
      let ooldGraph = {
          [coverPageOOLDBlankId]: coverPageOOLD
      };

      const uploadData = new FormData();
      uploadData.append('files', this.selectedThumbnailFile);
      uploadData.append("graph", JSON.stringify(graph));
      uploadData.append('ool_data_graph', JSON.stringify(ooldGraph));
      uploadData.append("projection", JSON.stringify({ "_id": 1, "cover": 1 }));
      uploadData.append("should_return_resource", 'true');
    
      this.http.post(this.endpointService.getBaseUrl() + "/objstore/v1/graph", uploadData, httpUploadOptions)
      .subscribe(
        response => {
          this.spinner.hide();
          this.showUpdatePagesComponent();
        },
        error => {
          this.spinner.hide();
          this.displayServerErrorMessage('#error-upload-thumbnail', error);
        }
      )
  }


  /**
   * Validates the Basic Fields of Book details
   * @param $book_title 
   * @param $book_author_information 
   * @param $book_description 
   */
  private validateBasicBookDataFields($book_title: string, $book_author_information: string, 
    $book_description: string){
    
    
    if ($book_title == null || $book_title.trim().length < 3) {
      $('#book_title_minlength_error').css('display', 'block');
      return false;
    }

    if ($book_author_information == null || $book_author_information.trim().length < 3) {
      $('#book_author_minlength_error').css('display', 'block');
      return false;
    }

    if ($book_description == null || $book_description.trim().length < 10) {
      $('#book_description_minlength_error').css('display', 'block');
      return false;
    }

    return true;
  }


  /**
   * Returns URL parameters
   * @param sParam 
   */
  public getUrlParameter(sParam) { 
    return decodeURIComponent(window.location.search.substring(1)).split('&') .map((v)=>
    { return v.split("=") }) .filter((v) => { return (v[0] === sParam) ? true : false })
    .reduce((acc:any,curr:any) => { return curr[1]; },undefined); 
  };


  /**
   * Fetches the details of the book including all the page details.
   * @param bookId 
   */
  private fetchBookDetails(bookId: string) {

    if(bookId == null || bookId.trim().length == 0){
      this.showErrorModalDialog("Book Id is missing. The Request URL is malformed.");
      return;
    }
    this.spinner.show();

    let successCallback = (response: any) => {
        this.spinner.hide();
        this.book = response;
        this.currentLibraryId = this.book.source;
        this.librarySelectorConfig.selected = this.currentLibraryId;
        this.setBookMetadataFields();
    }

    let errorCallback = (error: any) => {
        this.spinner.hide();
        this.showErrorModalDialog("Unable to fetch book details. Please retry or contact the platform administrator.");
    };

    this.booksService.getBook(this.endpointService.getBaseUrl(), bookId, undefined, successCallback, errorCallback);
  }

  /**
   * Sets the book Metatadata fields
   */
  private setBookMetadataFields() {
    $("#book_title").attr("value", this.book.getTitle());
    if(this.book.hasOwnProperty('jsonClassLabel')) {
        $("#book_type").attr("value", this.book['jsonClassLabel']);
    }
    if(this.book.metadata) {
        let metadataMap = this.book.getMetadataMap();
        if (metadataMap['description']) {
            $('#book_description').text(metadataMap['description']);
        }
    }
    if(this.book.cover) {
        this.coverImageUrl = this.book.cover.getIIIFOrDefaultUrl(this.endpointService.getBaseUrl(), 'full', ',150', '0', 'default', 'png');
    }
    if(this.book.author) {
        let authorNames = this.book.getAuthors() || [];
        $('#book_author').attr('value', authorNames.join(','));
    }
  }//end of method

  /**
   * Displays an Error modal dialog
   * @param errorMessage 
   */
  private showErrorModalDialog(errorMessage: string){
    
    if(errorMessage == null){
      return;
    }//if

	  this.modalDialogService.openDialog(this.viewRef, {
      title: 'Error',
      childComponent: SimpleModalComponent,
      data: {
        text: errorMessage
      },
      actionButtons: [
        {
          text: 'Close',
          buttonClass: 'btn btn-info',
          onAction: () => new Promise((resolve: any, reject: any) => {
            this.router.navigate(['annotator/edit-books']);
            resolve();
          })
        },
      ]
	  });
  }
  
  /**
   * Shows the Update Thumbnail Component.
   */
  private showUpdateThumbnailComponent(){
    $("#upload_book_container").css("display", "none");
    $("#book_title").attr("readonly", "true");
    $("#book_type").attr("readonly", "true");
    $("#book_author").attr("readonly", "true");
    $("#book_description").attr("readonly", "true");
    $("#add_book_thumbnail_container").css("display", "block");
    $('#info-book-created').text("Click on the Image to replace the thumbnail image.");

    this.setBookThumbnailImage(this.coverImageUrl);
    this.uploadedBookId = this.bookId;
    this.bookTitle = $("#book_title").val(); 
  }

  /**
   * Frames a IIIF URL and sets to the thumbnail icon.
   * @param thumbnailFileId 
   */
  private setBookThumbnailImage(url?: string){
      $('#img_book_thumbnail').attr('src', url);
  }

  /**
   * Shows the Update Pages Component
   */
  private showUpdatePagesComponent(){
    this.spinner.show();
    let httpThumbnailIdparams = new HttpParams()
    .append('projection','{"permissions": 0}')
    .append('filter_doc','{}');

    let httpThumbnailHeaders : HttpHeaders = new HttpHeaders({
      'Content-Type':'application/json',
    });                            

    this.http.get(this.endpointService.getBaseUrl()
      + '/objstore/v1/resources/' + this.bookId + "/specific_resources",  
      {headers:httpThumbnailHeaders, params:httpThumbnailIdparams,})
    .subscribe(          
      response =>{
        this.spinner.hide();
        this.bookPageResponse = response;

        if(this.bookPageResponse == null){
          this.showErrorModalDialog("Unable to fetch page details. Please retry or contact the platform administrator.");
          return;
        }//if
        this.bookPageResponse = this.bookPageResponse.items;
        for(let i=0;i<this.bookPageResponse.length;i++){
          if(this.bookPageResponse[i].hasOwnProperty("jsonClass") && (this.bookPageResponse[i]["jsonClass"] === "ScannedPage"
            || this.bookPageResponse[i]["jsonClass"] === "ScannedPage")
            && this.bookPageResponse[i].hasOwnProperty("_id")
            && this.bookPageResponse[i].hasOwnProperty("representations")
            && this.bookPageResponse[i]["representations"].hasOwnProperty("stillImage")) {

              
              let pageImageOOLDs = this.bookPageResponse[i]["representations"]["stillImage"];
              if(pageImageOOLDs != null && pageImageOOLDs.length > 0){
                this.resourceIds.push(this.bookPageResponse[i]["_id"]);
                this.addSlide(pageImageOOLDs[0].data.slice(6));
              }
          }
        }

        $("#add_book_thumbnail_container").css("display", "none");
        $("#add_page_container").css("display", "block");
    
        if(this.bookTitle != null) {
          $("#book-title-carousel").text("Books > " + this.bookTitle);
        }

      },
      error => {
        this.spinner.hide();
        this.showErrorModalDialog("Unable to fetch page details. Please retry or contact the platform administrator.");
      }
    );
  }

  
 /**
  * Displays the container for uploading books in bulk.
  */
 private showBulkUploadBooks() {
  $('#add_page_container').css('display','none');
  $('#upload-bulk-image').css('display','block');
}


  /**
   * Multiple files selected callback.
   * @param event 
   */
  private onMultiPageImageSelected(fileInput: any){
    $("#error-upload-multi-pages").css("display", "none");
    let filesSelected = fileInput.target.files;
    if(this.haveNonImageFilesBeenSelected(filesSelected)){
      $('#error-upload-multi-pages').text("It seems like you have selected a file which is not an image. Please select only PNG or JPG files to upload.");
      $("#error-upload-multi-pages").css("display", "block");
      this.pageFilesArraySelected = null;
      $('#multiple-pages-images-files').val('');
      return;
    }
    this.pageFilesArraySelected = <Array<File>>fileInput.target.files;
  }
  
  /**
   * Returns true if non image files have been selected.
   * @param selectedFiles 
   */
  private haveNonImageFilesBeenSelected(selectedFiles: Array<File>): boolean{
    if(selectedFiles != null && selectedFiles.length > 0){
      for(let i=0;i<selectedFiles.length;i++){
          if(selectedFiles[i] == null || selectedFiles[i].name == null){
            continue;
          }
          if (!selectedFiles[i].name.endsWith('.png') && !selectedFiles[i].name.endsWith('.jpg')
            && !selectedFiles[i].name.endsWith('.jpeg')) {
            return true;
          }
      }//for
    }
    return false;
  }
  
  /**
   *  Method uploads multiple pages at once.
   */
  private onMultiPageUpload(){
    $("#error-upload-multi-pages").css("display", "none");
    if(this.uploadedBookId == null){
      $("#error-upload-multi-pages").text("Book details missing, the session may have been lost. Please login afresh and try again.");
      $("#error-upload-multi-pages").css("display", "block");
      return;
    }//if

    if (this.pageFilesArraySelected == null || this.pageFilesArraySelected.length == 0) {
      $('#error-upload-multi-pages').text("Please select at least one PNG or JPG file to upload.");
      $("#error-upload-multi-pages").css("display", "block");
      return;
    }

    if(this.haveNonImageFilesBeenSelected(this.pageFilesArraySelected)){
      $('#error-upload-multi-pages').text("It seems like you have selected a file which is not an image. Please select only PNG or JPG files to upload.");
      $("#error-upload-multi-pages").css("display", "block");
      return;
    }

      let pagesGraph = {};
      let ooldGraph = {};
      let pageNo = 1;

      for (let pageFile of this.pageFilesArraySelected) {
          let pageBlankId = `_:page_${pageNo}`;
          let pageImageOOLDBlankId = `_:page_image_oold_${pageNo}`;
          let pageImageOOLD = {
              _id: pageImageOOLDBlankId,
              jsonClass: "StillImage",
              source: `_VV:${pageBlankId}`,
              namespace: "_vedavaapi",
              identifier: pageFile.name
          };

          let pageJson = {
              _id: pageBlankId,
              jsonClass: "ScannedPage",
              purpose: "page",
              selector: {
                  jsonClass: "QualitativeSelector",
              },
              source: this.uploadedBookId,
              representations: {
                  jsonClass: 'DataRepresentations',
                  default: 'stillImage',
                  stillImage: [{
                      jsonClass: 'StillImageRepresentation',
                      data: `_OOLD:${pageImageOOLDBlankId}`
                  }]
              }
          };

          pagesGraph[pageBlankId] = pageJson;
          ooldGraph[pageImageOOLDBlankId] = pageImageOOLD;
          pageNo = pageNo + 1;
      }
   
    const httpUploadOptions = {
      headers: new HttpHeaders({ 'Accept': 'application/json' })
    };
  
    
    $("#error-upload-multi-pages").css("display", "none");
    $('#multi-pages-upload-info').text("Uploading pages...");
    //show the spinner
    this.spinner.show();

      const formData: any = new FormData();
      formData.append("graph", JSON.stringify(pagesGraph));
      formData.append('ool_data_graph', JSON.stringify(ooldGraph));
      for (let file of this.pageFilesArraySelected) {
          formData.append("files", file);
      }

      formData.append("response_projection_map", JSON.stringify({ "*": { "permissions": 0 } }));
      formData.append('should_return_resources', 'true');

      this.http.post(this.endpointService.getBaseUrl() + '/objstore/v1/graph', formData,
          httpUploadOptions).subscribe(
        response => {
          this.spinner.hide();
          $('#multiple-pages-images-files').val('');
          $('#multi-pages-upload-info').text("All Pages uploaded successfully. Continue to add more pages or click on Finish to go to the dashboard.");
          this.pageFilesArraySelected = null;
        },
        error => {
          this.spinner.hide();
          $('#multiple-pages-images-files').val('');
          $('#multi-pages-upload-info').css("display", "none");
          this.displayServerErrorMessage("#error-upload-multi-pages", error);
          this.pageFilesArraySelected = null;
        } //error
      );
  }//end of method

  /**
   * Image file for Page selected callback.
   * @param event
   */
  onPageImageSelected(event){
    $("#error-upload-page").css("display", "none");
    let fileSelected = event.target.files[0];
    if(fileSelected == null){
      return;
    }
    if (!fileSelected.name.endsWith('.png') && !fileSelected.name.endsWith('.jpg')
      && !fileSelected.name.endsWith('.jpeg')) {
      $('#error-upload-page').text("Only image files are allowed. Please select a PNG or JPG file to upload.");
      $("#error-upload-page").css("display", "block");
      return;
    }

    this.selectedPageFile = event.target.files[0];
  }


  /**
   * Uploads a Single Page of a Book.
   */
   private onPageUpload(){
    $("#error-upload-page").css("display", "none");
    if(this.uploadedBookId == null){
      $("#error-upload-page").text("Book details missing, the session may have been lost. Please login afresh and try again.");
      $("#error-upload-page").css("display", "block");
      return;
    }//if

    if (this.selectedPageFile == null) {
      $('#error-upload-page').text("Please select a PNG or JPG file to upload.");
      $("#error-upload-page").css("display", "block");
      return;
    }

    if (!this.selectedPageFile.name.endsWith('.png') && !this.selectedPageFile.name.endsWith('.jpg')
      && !this.selectedPageFile.name.endsWith('.jpeg')) {
      $('#error-upload-page').text("Only image files are allowed. Please select a PNG or JPG file to upload.");
      $("#error-upload-page").css("display", "block");
      return;
    }

    const httpUploadOptions = {
      headers: new HttpHeaders({ 'Accept': 'application/json' })
    };

    
       let pageGraph = {};
       let pageOOLDGraph = {};

       let pageBlankId = `_:page_for_${this.uploadedBookId}`
       let pageImageOOLDBlankId = `_:page_image_oold`;
       let pageImageOOLD = {
           _id: pageImageOOLDBlankId,
           jsonClass: 'StillImage',
           source: `_VV:${pageBlankId}`,
           namespace: '_vedavaapi',
           identifier: this.selectedPageFile.name
       };

       let pageJson = {
           _id: pageBlankId,
           jsonClass: "ScannedPage",
           purpose: "page",
           selector: {
               jsonClass: "QualitativeSelector",
           },
           source: this.uploadedBookId,
           representations: {
               jsonClass: 'DataRepresentations',
               default: 'stillImage',
               stillImage: [{
                   jsonClass: 'StillImageRepresentation',
                   data: `_OOLD:${pageImageOOLDBlankId}`
               }]
           }
       };
       pageGraph[pageBlankId] = pageJson;
       pageOOLDGraph[pageImageOOLDBlankId] = pageImageOOLD;

       const formData: any = new FormData();
       formData.append("graph", JSON.stringify(pageGraph));
       formData.append("files", this.selectedPageFile);
       formData.append("ool_data_graph", JSON.stringify(pageOOLDGraph));
       formData.append("response_projection_map", JSON.stringify({ '*': { "permissions": 0 } }));
       formData.append("should_return_resources", 'true');
       formData.append("should_return_oold_resources", 'true');


   
    this.spinner.show();
       return this.http.post(this.endpointService.getBaseUrl() + '/objstore/v1/graph', formData,
           httpUploadOptions).subscribe(
        response => {
          $('#page-image-file').val('');
          $('#info-upload-page').text("Page uploaded successfully. Continue to add more pages or click on Finish to go to the dashboard.");          
                   var pageUploadResponse: any = response;
                   // get page id
                   let page = pageUploadResponse['graph'][pageBlankId];
                   let page_id = page["_id"];
                   let imageOOLDataId = page.representations.stillImage[0].data.slice(6);
                   this.resourceIds.push(page_id);
                   this.addSlide(imageOOLDataId);

                   this.spinner.hide();
                   //Uploads Page thumbnail
                   this.selectedPageFile = null;
        },
        error => {
          this.spinner.hide();
          this.displayServerErrorMessage("#error-upload-page", error);
        }
      );
  }//end of method


  /**
   * Shows modal dialog.
   * @param idx 
   */
  showModalDialog(index){
    $("#modal_delete_button").attr('value',index);
  }
  /**
   * Delete the uploded book page from server.
   * @param index 
   */
  onDeletePage() {

    $("#modal_cancel_btn").click();
    let index = $("#modal_delete_button").val();
    if(this.resourceIds.length <= index || index < 0){
      return;
    }
    const deletePageFormData = new FormData();
    deletePageFormData.append('resource_ids','["'+this.resourceIds[index]+'"]');
    const httpOptions = {
      headers: new HttpHeaders({'Accept': 'application/json'}),body:deletePageFormData,
    };
    this.http.delete(this.endpointService.getBaseUrl() + '/objstore/v1/resources' , httpOptions)
    .subscribe(          
      response =>{
        $("#delete_page_error").hide();
        $("#" + index).remove();
      },
      error => {
        $("#error_message_title").show();
        let errorCode = this.getResponseErrorCode(error);
        let error_message = this.getErrorMessage(errorCode);
        $("#delete_page_error").show();
        $("#delete_page_error").text(error_message);
      }
    );
  }
  

}//end of class


