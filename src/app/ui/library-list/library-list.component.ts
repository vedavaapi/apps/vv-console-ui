import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { PagedDataComponent } from '../paged-data/paged-data.component';
import { EndpointsService } from 'src/app/endpoints.service';
import { OauthHelperService } from 'src/app/services/oauth-helper.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import * as $ from 'jquery';
import { VedavaapiApiService } from 'src/app/services/vedavaapi-api.service';
import { JsonUtilsService } from 'src/app/services/json-utils.service';
import { ObjstoreApiService } from 'src/app/services/objstore-api.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';

@Component({
    selector: 'app-library-list',
    templateUrl: './library-list.component.html',
    styleUrls: ['./library-list.component.scss']
})

/**
 * Displays list of libraries.
 */
export class LibraryListComponent extends PagedDataComponent {

    private libraryList: any = [];

    private vvSiteUrl: string;

    private isPageClicked : boolean = false;

    constructor(@Inject(DOCUMENT) protected document: Document, protected endpointService: EndpointsService,
        protected oauthHelperService: OauthHelperService, protected cdRef: ChangeDetectorRef,
        private _router: Router,
        private _vedavaapiApiService: VedavaapiApiService,
        private _objStore: ObjstoreApiService,
        private _jsonUtilsService: JsonUtilsService,
        private _spinner: NgxSpinnerService,
        private _localStorageService: LocalStorageService) {
        super(document, endpointService, oauthHelperService, cdRef, _router);
        this.vvSiteUrl = this._localStorageService.getValue("server_url");

    }

    ngOnInit() {
        super.ngOnInit();
    }

    /**
     * Populates the Data in this Component/Page. 
     * This method 
     * 
     * @param search_string 
     */
     protected populateData(search_string: string,) {
        this.userId = this.getCurrentUserId();
        this.hideAllUIElements();

        var that = this;
        $('.new-button').click(callback => {
            that._router.navigate(['create', 'Library']);
        });

        $('.pagination-row').css('display', 'none');
        $('.new-button').css('display', 'none');

        $('#loading-gif-icon').css('display', 'block');
        $('#loading-gif-text').text('Fetching libraries ...');

        if (search_string && search_string.trim().length > 0 && !this.isPageClicked){
            this.startIndex = 0;
            this.isPageClicked = true;
        }else if(search_string == null){
            this.startIndex = 0;
            this.isPageClicked = false;
        }

        var access_token = this.getAccessToken();
        this._vedavaapiApiService.getLibraryList(access_token, this.getSearchString(),
            this.startIndex * PagedDataComponent.PAGE_COUNT,
            PagedDataComponent.PAGE_COUNT,
            successResponse => {
                $('#loading-gif-icon').css('display', 'none');
                this.extractLibraryList(successResponse);
            },
            errorMessage => {
                this.hideAllUIElements();
                this.showErrorMessage(errorMessage);
            }
        );


        /*
        this._groupsApiService.getMyTeams(this.getCurrentUserId(), access_token, search_string,
            this.startIndex * PagedDataComponent.PAGE_COUNT, PagedDataComponent.PAGE_COUNT,
            successResponse => {
                $('#loading-gif-icon').css('display', 'none');
                this.extractTeamsData(successResponse);
            },
            errorMessage => {
                this.hideAllUIElements();
                this.showErrorMessage(errorMessage);
            });
        */

    }//end of meethod

    /**
     * Extracts Library List Data from the Response.
     * @param libraries_response 
     */
    private extractLibraryList(libraries_response: any) {
        if (libraries_response == null || libraries_response.total_count == 0
            || !libraries_response.hasOwnProperty('items')) {
            this.hideAllUIElements();
            this.showErrorMessage('No Libraries present found. Please click on the new button on top to create a new library.');
            $('.pagination-row').css('display', 'none');
            $('.new-button').css('display', 'block');
            return;
        }

        if (libraries_response.hasOwnProperty("total_count")) {
            this.totalNumber = libraries_response['total_count'];
        }

        this.libraryList = [];
        let librariesResponse = libraries_response['items'];
        var libraryObject: any;

        for (let index = 0; index < librariesResponse.length; index++) {
            libraryObject = librariesResponse[index];
            if (libraryObject == null) {
                continue;
            }
            var name: string = null, _id: string = null, description: string = null;;

            if (libraryObject.hasOwnProperty('_id')) {
                _id = libraryObject['_id'];
            }
            if (_id == null) {
                continue;
            }

            if (libraryObject.hasOwnProperty('name')) {
                name = libraryObject['name'];
            }

            if (name == null) {
                name = '';
            }

            if (libraryObject.hasOwnProperty('description')) {
                description = libraryObject['description'];
            }
            if (description == null) {
                description = '';
            }



            let resolvedPermissions = libraryObject.resolvedPermissions || {};

            this.libraryList.push({
                "_id": _id, "name": name,
                "description": description,
                "is_member": this.isMemberOfTeam(this.userId, libraryObject),
                "is_read": resolvedPermissions.read,
                "is_update_links": resolvedPermissions.updateLinks,
                "is_update_content": resolvedPermissions.updateContent,
                "is_delete": resolvedPermissions.delete,
                "is_edit": resolvedPermissions.edit,
                "is_update_permissions": resolvedPermissions.updatePermissions
            });
        }

        $('.search-container').css('display', 'block');
        $('#data-table').css('display', 'inline-table');
        $('.pagination-row').css('display', 'flex');
        $('.new-button').css('display', 'block');
        console.log("Extract Library Response complete");
    }

    /**
     * Page Changed callback
     * @param pageClicked 
     */
    private pageChanged(pageClicked) {
        this.startIndex = pageClicked - 1;
        this.populateData(this.getSearchString());
    }

    /**
     * User has selected an action on the Library. This callback is called from the Html Page.
     * 
     * @param libraryObj 
     */
    private onActionSelected(libraryObj: any) {

        if (!libraryObj.hasOwnProperty('_id') || libraryObj['_id'].trim().length == 0) {
            return;
        }

        var identifier = libraryObj['_id'];
        var name = null;
        if (libraryObj.hasOwnProperty('name')) {
            name = libraryObj['name'];
        }
        if (!name) {
            name = '';
        }

        var selectedAction = $('#select_' + identifier + ' option:selected').val();
        if (selectedAction == null) {
            return;
        }

        switch (selectedAction) {
            case 'add_remove':
            case 'view': {
                this._router.navigate(['library', identifier, 'books'], { queryParams: { name: name } });
                break;
            }

            case 'delete': {
                this.deleteLibrary(identifier);
                break;
            }

            case 'update_permissions': {
                this._router.navigate(['teams', 1, identifier, 'select']);
                break;
            }
            case 'edit': {
                this._router.navigate(['edit','Library',identifier], { queryParams: { "edit-object": "true" } });
                break;
            }

            default:
                break;

        }
    }

    private deleteLibrary(identifier: any) {
        if (!identifier) {
            return;
        }

        let resourceIds = new Array();
        resourceIds.push(identifier);

        this._spinner.show();
        this._objStore.deleteResources(this.vvSiteUrl, resourceIds,
            successResponse => {
                this._spinner.hide();
                this.insertAlertMessage('success-alert-parent', "Success! Deleted the book from this library");
                this.populateData(this.getSearchString());
            },
            error => {
                this._spinner.hide();
                this.insertAlertMessage('failure-alert-parent', this._jsonUtilsService.getErrorMessage(error, 'delete_book',
                    'Failure! Unable to delete the book'));

            });
    }
}//end of class declaration
