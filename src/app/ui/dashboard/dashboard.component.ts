import { Component, OnInit, ViewChild, ElementRef, Renderer2, AfterViewInit } from '@angular/core';
import { EndpointsService } from 'src/app/endpoints.service';
import { OauthHelperService } from 'src/app/services/oauth-helper.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { VedavaapiShellHelperService } from 'src/app/services/vedavaapi-shell-helper.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, AfterViewInit {
    //   private iframeHtml: string;

    @ViewChild('mirador_iframe') miradorDiv: ElementRef;

    content = '<button (click)="onclick">This is a Clickable Span</button>';

    constructor(private renderer: Renderer2, private endpointsService: EndpointsService, private oauthHelperService: OauthHelperService, private localStorageService: LocalStorageService, private vedavaapiShellHelperService: VedavaapiShellHelperService) {
        /*   let vvSiteUrl = this.localStorageService.getValue('server_url');
          let authorizedCreds = this.oauthHelperService.getAuthorizedCreds(vvSiteUrl);
          let accessToken = authorizedCreds ? authorizedCreds['access_token'] : '';
          
          this.iframeHtml = '<iframe title="Mirador" id="mirador-iframe" src="'
            + this.endpointsService.getMiradorInstanceUrl() + `?vv_site_url=${vvSiteUrl}&access_token=${accessToken}`
            + '" allowfullscreen="true"'
            + ' width="100%" height="100%" style="height:1400px;border: 0px;"'
            + ' webkitallowfullscreen="true" mozallowfullscreen="true"></iframe>'; */
    }

    ngOnInit() {
        // this.renderer.setAttribute(this.miradorDiv.nativeElement, 'innerHTML', this.content);
    }

    ngAfterViewInit() {
        /*  let miradorFrameWindow = document.getElementById('mirador-iframe')['contentWindow'];
         this.vedavaapiShellHelperService.listenAuthorizationRequestsFromIframe(miradorFrameWindow);
         this.vedavaapiShellHelperService.channelAuthorizationChangesToIframe(miradorFrameWindow); */
    }

    /*  onclick() {
       console.log('On Click!!!');
     } */
}
