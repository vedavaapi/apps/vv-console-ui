import { Component, OnInit, Input } from '@angular/core';
import { ScannedBook, Library, joFromJson } from 'src/app/services/models';
import { ObjstoreApiService } from 'src/app/services/objstore-api.service';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { EndpointsService } from 'src/app/endpoints.service';
import { OauthHelperService } from 'src/app/services/oauth-helper.service';
import * as $ from 'jquery';
import { stringify } from '@angular/core/src/util';


@Component({
    selector: 'app-library-selector',
    templateUrl: './library-selector.component.html',
    styleUrls: ['./library-selector.component.scss']
})

export class LibrarySelectorComponent implements OnInit {

    // TODO; presently a simple one. have to be generified.

    private start: number = 0;

    private totalCount: number = -1;

    private static ITEM_COUNT: number = 20;

    private fetchedItemsCount: number = 0;

    private isMoreLibrariesPresent: boolean = false;

    private isLoadMoreClicked: boolean = false;



    @Input() config: { shouldUpdateQueryParams: boolean, selectionChangeCallback: (library: Library) => void, listenAuthChanges: boolean, selected: string };

    private libraries: Array<Library>;
    private idLibraryMap: any;
    private selectedLibraryId: string;
    private selectedLibrary: Library;

    constructor(private endpointService: EndpointsService, private oauthHelperService: OauthHelperService,
        private objstore: ObjstoreApiService,
        private router: Router, private activatedRoute: ActivatedRoute) {

    }

    private subscribeAuthorizationChangeBroadcast() {
        let bc = new BroadcastChannel(this.oauthHelperService.authorizationChangeBroadcastChannelName);
        bc.onmessage = ({ data }) => {
            if (data) {
                this.fetchLibraries();
            }
        }
    }

    private fetchLibraries() {
        let selectorDoc = { "jsonClass": "Library" };
        let projection = { _id: 1, name: 1, description: 1, logo: 1 };
        let successCallback = (response: { total_count: number, items: Array<any> }) => {

            if(this.isLoadMoreClicked){
                this.isLoadMoreClicked = false;
                $('#span-library-fetch-progress-message').css('display', 'none');
                $('#span-library-fetch-status').css('display', 'block');
            }

            this.totalCount = response.total_count;
            if(response.items){
                this.fetchedItemsCount += response.items.length;
                if(this.fetchedItemsCount < this.totalCount){
                    this.isMoreLibrariesPresent = true;
                }else{
                    this.isMoreLibrariesPresent = false;
                }
            }//if
            if(this.libraries == null){
                this.libraries = joFromJson(response.items);
                this.idLibraryMap = {};
            }else{
                let tempJson = joFromJson(response.items);
                if(tempJson){
                    tempJson.forEach(item => {
                        this.libraries.push(item);
                    });
                }
                
            }
            if(this.libraries){
                this.libraries.forEach(l => { this.idLibraryMap[l._id] = l; });
            }

            
        };
        this.objstore.getResources(this.endpointService.getBaseUrl(), selectorDoc, projection, [['name', 1]], this.start + this.fetchedItemsCount, LibrarySelectorComponent.ITEM_COUNT, false, successCallback);
    }


    private onLibraryChange(library_id: string) {
 
        if(library_id === 'load-more'){
            console.log('Load More Button Clicked');
            $('#library-selector option').eq(0).prop('selected', true);
            $('#span-library-fetch-progress-message').css('display', 'block');
            $('#span-library-fetch-status').css('display', 'none');
            this.isLoadMoreClicked = true;
            this.fetchLibraries();
            return;
        }

        this.selectedLibraryId = library_id;
        if (this.config.shouldUpdateQueryParams) {
            const queryParams: Params = { library_id: library_id };
            this.router.navigate(
                [],
                {
                    relativeTo: this.activatedRoute,
                    queryParams: queryParams,
                    queryParamsHandling: "merge", // remove to replace all query params by provided
                }
            );
        }
        this.selectedLibrary = this.idLibraryMap[library_id];
        if (this.config.selectionChangeCallback) {
            this.config.selectionChangeCallback(this.selectedLibrary);
        }
    }

    ngOnInit() {
        console.log({ config: this.config });
        if (this.config) {
            if (this.config.listenAuthChanges) {
                this.subscribeAuthorizationChangeBroadcast();
            }
            this.selectedLibraryId = this.config.selected;
            this.fetchLibraries();
        }
    }
}
