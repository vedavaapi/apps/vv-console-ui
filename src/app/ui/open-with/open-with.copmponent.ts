import { Component, OnInit, AfterViewInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { OauthHelperService } from 'src/app/services/oauth-helper.service';
import { ObjstoreApiService } from 'src/app/services/objstore-api.service'

@Component({
    selector: 'app-open-with',
    templateUrl: './open-with.component.html',
    styleUrls: ['./open-with.component.scss']
})
export class OpenWithComponent implements OnInit, AfterViewInit {

    private vvSiteUrl: string;
    @Input() companionUrl: string;
    @Input() object: any;
    private matchedApps: Array<any>

    constructor(
        private oauthHelperService: OauthHelperService,
        private localStorageService: LocalStorageService) {
        this.vvSiteUrl = this.localStorageService.getValue('server_url');
        this.matchedApps = [];
    }

    ngOnInit() {
        this.getMatchedApps();
    }

    ngAfterViewInit() {
    }


    private getMatchedApps() {
        console.log({obj: this.object});
        let registeredApplications = this.oauthHelperService.getApplications(this.vvSiteUrl)
        if (!registeredApplications) {
            return;
        }
        this.matchedApps.length = 0;

        for (let app of registeredApplications) {
            if (app.handledTypes && Array.isArray(app.handledTypes) && app.handledTypes.includes(this.object.jsonClass)  && !app.apis
            ) {
                this.matchedApps.push(app)
            }
        }
    }

    private appUrl(app: any) {
        let url = new URL(app.url);
        url.searchParams.set('_id', this.object._id);
        url.searchParams.set('companion', this.companionUrl)
        url.searchParams.set('jsonClass', this.object.jsonClass);
        return url.href;
    }

}//end of class
