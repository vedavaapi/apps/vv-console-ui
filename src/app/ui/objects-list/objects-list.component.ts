import { Component, OnInit, Inject, ChangeDetectorRef, Injectable } from '@angular/core';
import { PagedDataComponent } from '../paged-data/paged-data.component';
import { EndpointsService } from 'src/app/endpoints.service';
import { OauthHelperService } from 'src/app/services/oauth-helper.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import * as $ from 'jquery';
import { VedavaapiApiService } from 'src/app/services/vedavaapi-api.service';
import { JsonUtilsService } from 'src/app/services/json-utils.service';
import { ObjstoreApiService } from 'src/app/services/objstore-api.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';

@Injectable({
    providedIn: 'root'
})

@Component({
    selector: 'app-objects-list',
    templateUrl: './objects-list.component.html',
    styleUrls: ['./objects-list.component.scss']
})
/**
 * Displays list of objects.
 */
export class ObjectsListComponent extends PagedDataComponent {

    private objectsList: any = [];

    private vvSiteUrl: string;

    private isShowCreateObjectsModalDlg: boolean = false;

    private objectsErrorMsg: string = null;

    private isPageClicked: boolean = false;

    constructor(@Inject(DOCUMENT) protected document: Document, protected endpointService: EndpointsService,
        protected oauthHelperService: OauthHelperService, protected cdRef: ChangeDetectorRef,
        private _router: Router,
        private _vedavaapiApiService: VedavaapiApiService,
        private _objStore: ObjstoreApiService,
        private _jsonUtilsService: JsonUtilsService,
        private _spinner: NgxSpinnerService,
        private _localStorageService: LocalStorageService) {
        super(document, endpointService, oauthHelperService, cdRef, _router);
        this.vvSiteUrl = this._localStorageService.getValue("server_url");

    }

    ngOnInit() {
        super.ngOnInit();
    }

    /**
     * Populates the Data in this Component/Page. 
     * This method 
     * 
     * @param search_string 
     */
    protected populateData(search_string: string) {
        this.userId = this.getCurrentUserId();
        this.hideAllUIElements();

        $('.pagination-row').css('display', 'none');
        // $('.new-button').css('display', 'none');
        $('.new-button-objects').css('display', 'none');

        $('#loading-gif-icon').css('display', 'block');
        $('#loading-gif-text').text('Fetching objects...');
        if (search_string && search_string.trim().length > 0 && !this.isPageClicked){
            this.startIndex = 0;
            this.isPageClicked = true;
        }else if(search_string == null){
            this.startIndex = 0;
            this.isPageClicked = false;
        }
        var access_token = this.getAccessToken();
        this._vedavaapiApiService.getObjectsList(access_token, this.getSearchString(),
            this.startIndex * PagedDataComponent.PAGE_COUNT,
            PagedDataComponent.PAGE_COUNT,
            successResponse => {
                $('#loading-gif-icon').css('display', 'none');
                this.extractObjectsList(successResponse);
            },
            errorMessage => {
                this.hideAllUIElements();
                this.showErrorMessage(errorMessage);
            }
        );
    }//end of meethod

    /**
     * create objects of Library.
     */
    private createLibrary() {
        var that = this;
        let navigationObjects = {
            queryParams: {
                "object-root": "true"
            }
        };

        that._router.navigate(['create', 'Library'], navigationObjects);
    }
    /**
     *  This method callbacks the create object view.where user entered object name create object modal dialog.
     */
    private submitObjectType() {
        var that = this;
        var schemaName = $('#input-schema-name').val();
        if (schemaName == null || schemaName.trim().length == 0) {
            this.objectsErrorMsg = "Objects Schema should not be empty."
            return;
        }
        let navigationObjects = {
            queryParams: {
                "object-root": "true"
            }
        };
        schemaName = schemaName.trim();
        that._router.navigate(['create', schemaName], navigationObjects);

    }
    /**
     *  cancel the object type value in input form.
     */
    private cancelObjectType() {
        $("#input-schema-name").val('');
        this.objectsErrorMsg = null;

    }
    /**
     * Extracts Objects List Data from the Response.
     * @param objects_response 
     */
    private extractObjectsList(objects_response: any) {
        if (objects_response == null || objects_response.total_count == 0
            || !objects_response.hasOwnProperty('items')) {
            this.hideAllUIElements();
            this.showErrorMessage('No Objects present found. Please click on the new button on top to create a new Objects list.');
            $('.pagination-row').css('display', 'none');
            $('.new-button').css('display', 'block');
            $('.new-button-objects').css('display', 'block');
            return;
        }

        if (objects_response.hasOwnProperty("total_count")) {
            this.totalNumber = objects_response['total_count'];
        }

        this.objectsList = [];
        let objectsResponse = objects_response['items'];
        var object: any;

        for (let index = 0; index < objectsResponse.length; index++) {
            object = objectsResponse[index];
            if (object == null) {
                continue;
            }
            var name: string = null, _id: string = null, email: string = null, jsonClass: string = null, isTeam: string = null, isLibrary: string = null, description: string = null;;

            if (object.hasOwnProperty('_id')) {
                _id = object['_id'];
            }
            if (_id == null) {
                continue;
            }

            if (object.hasOwnProperty('name')) {
                name = object['name'];
            }
            if (object.hasOwnProperty("title")) {
                name = object['title'];
            }
            if (name == null) {
                name = '';
            }
            if (object.hasOwnProperty('email')) {
                email = object['email'];
            }
            if (email == null) {
                email = '';
            }

            if (object.hasOwnProperty('description')) {
                description = object['description'];
            }
            if (object.hasOwnProperty("metadata")
                && object['metadata'].length > 0
                && object['metadata'][0].hasOwnProperty('label')
                && object['metadata'][0]['label'] === 'description'
                && object['metadata'][0].hasOwnProperty('value')) {
                description = object['metadata'][0]['value'];
            }//
            if (description == null) {
                description = '';
            }
            if (object.hasOwnProperty('jsonClass')) {
                jsonClass = object['jsonClass'];
            }
            if (jsonClass == null) {
                jsonClass = '';
            }
            if (jsonClass == "Library") {
                isLibrary = "true";
            }
            if (jsonClass == "Team") {
                isTeam = "true";
            }
            let resolvedPermissions = object.resolvedPermissions || {};

            this.objectsList.push({
                "_id": _id, "name": name,
                "email": email,
                "description": description,
                "isLibrary": isLibrary,
                "isTeam": isTeam,
                "jsonClass": jsonClass,
                "is_member": this.isMemberOfTeam(this.userId, object),
                "is_read": resolvedPermissions.read,
                "is_update_links": resolvedPermissions.updateLinks,
                "is_update_content": resolvedPermissions.updateContent,
                "is_delete": resolvedPermissions.delete,
                "is_update_permissions": resolvedPermissions.updatePermissions
            });
        }

        $('.search-container').css('display', 'block');
        $('#data-table').css('display', 'inline-table');
        $('.pagination-row').css('display', 'flex');
        $('.new-button').css('display', 'block');
        $('.new-button-objects').css('display', 'block');
    }

    /**
     * Page Changed callback
     * @param pageClicked 
     */
    private pageChanged(pageClicked) {
        this.startIndex = pageClicked - 1;
        this.populateData(this.getSearchString());
    }

    /**
     * User has selected an action on the objects. This callback is called from the Html Page.
     * 
     * @param objectHandle 
     */
    private onActionSelected(objectHandle: any) {
        if (!objectHandle.hasOwnProperty('_id') || objectHandle['_id'].trim().length == 0) {
            return;
        }
        if (!objectHandle.hasOwnProperty('jsonClass') || objectHandle['jsonClass'] == null){
            return;
        }
        var identifier = objectHandle['_id'];
        var jsonClass = objectHandle['jsonClass'];
        var name = null;
        if (objectHandle.hasOwnProperty('name')) {
            name = objectHandle['name'];
        }
        if (!name) {
            name = '';
        }

        var selectedAction = $('#select_' + identifier + ' option:selected').val();
        if (selectedAction == null) {
            return;
        }

        switch (selectedAction) {
            case 'add_remove':
            case 'view': {
                this._router.navigate(['object', identifier], { queryParams: { name: name } });
                break;
            }
            case 'delete': {
                this.deleteObject(identifier);
                break;
            }
            case 'add_remove_member': {
                this._router.navigate(['users', identifier, 'addremove'], { queryParams: { name: name, "object-root": "true" } });
                break;
            }
            case 'edit': {
                if (jsonClass == 'Library') {
                    this._router.navigate(['edit', 'Library', identifier], { queryParams: { "object-root": "true", "edit-object": "true" } });
                    break;
                }
                if (jsonClass == 'Team') {
                    this._router.navigate(['edit', 'Team', identifier], { queryParams: { "edit-object": "true", "object-root": "true" } });
                }
                if (jsonClass == 'User') {
                    this._router.navigate(['edit', 'User', identifier], { queryParams: { "edit-object": "true", "object-root": "true" } });
                }
            }

            default:
                break;

        }
    }
    /**
     * delete Object.
     * @param identifier 
     */
    private deleteObject(identifier: any) {
        if (!identifier) {
            return;
        }

        let resourceIds = new Array();
        resourceIds.push(identifier);

        this._spinner.show();
        this._objStore.deleteResources(this.vvSiteUrl, resourceIds,
            successResponse => {
                this._spinner.hide();
                this.insertAlertMessage('success-alert-parent', "Success! Deleted the object from this object list.");
                this.populateData(this.getSearchString());
            },
            error => {
                this._spinner.hide();
                this.insertAlertMessage('failure-alert-parent', this._jsonUtilsService.getErrorMessage(error, 'delete_book',
                    'Failure! Unable to delete the book'));

            });
    }

}
