import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { EndpointsService } from 'src/app/endpoints.service';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { OauthHelperService } from 'src/app/services/oauth-helper.service';
import { ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-site-url',
    templateUrl: './site-url.component.html',
    styleUrls: ['./site-url.component.scss']
})
/**
 *  this class returns the server url.
 */
export class SiteUrlComponent implements OnInit {

    private vvSiteUrl: string;
    private authorized: boolean = false;
    private siteUrlChangeStatusMessages: string = null;

    /**
     * constructor
     * @param endpointService 
     * @param localStorageService 
     * @param oauthHelperService 
     * @param http 
     * @param cdRef 
     * @param router 
     */
    constructor(private endpointService: EndpointsService, private localStorageService: LocalStorageService, private oauthHelperService: OauthHelperService,
        private http: HttpClient, private cdRef: ChangeDetectorRef, private router: Router) {
        this.vvSiteUrl = this.localStorageService.getValue('server_url');
        this.subscribeAuthorizationChangeBroadcast();
    }


    /**
     *  get the updated status of login or logout.
     */
    private subscribeAuthorizationChangeBroadcast() {
        let bc = new BroadcastChannel(this.oauthHelperService.authorizationChangeBroadcastChannelName);
        bc.onmessage = ({ data }) => {
            if (data) {
                this.updateAuthorizedStatus();
            }
        }
    }
    /**
     *  update login/logout status
     */
    updateAuthorizedStatus() {
        let authorizedCreds = this.oauthHelperService.getAuthorizedCreds();
        this.cdRef.detectChanges();
        this.authorized = Boolean(authorizedCreds);
        this.cdRef.detectChanges();
    }

    ngOnInit() {
        this.updateAuthorizedStatus();
        let serverUrl = this.localStorageService.getValue("server_url");
        if (serverUrl != null) {
            $("#default-input-site-url").attr("readonly", true)
        }

    }


    /**
     * on click Change button.
     */
    private changeSiteUrl() {
        $('#site-url-page-default').css('display', 'none');
        $('#site-url-page-change').css('display', 'inline-block');
        $('#input-change-server-url').val('');
    }
    /**
     * on click back button.
     */
    private backSiteUrl() {
        this.siteUrlChangeStatusMessages = null;
        $('#site-url-page-change').css('display', 'none');
        $('#site-url-page-default').css('display', 'inline-block');
    }

    /**
     * Saves the new Endpoint URLs.
     */
    private saveChangedEndpointUrls() {
        var serverUrl = null;
        if (this.vvSiteUrl == null) {
            serverUrl = $('#default-input-site-url').val();
        } else {
            serverUrl = $('#input-change-server-url').val();
        }

        if (serverUrl == null || serverUrl.trim().length == 0) {
            this.siteUrlChangeStatusMessages = "Server URL should not be empty";
            return;
        }
        if (this.vvSiteUrl == serverUrl) {
            this.siteUrlChangeStatusMessages = "The server url is the same as current one, please give a different url.";
            return;
        }

        if (this.authorized == true) {
            this.oauthHelperService.unAuthorizeUser();
        }

        serverUrl = serverUrl.trim();
        let clientCreds = this.oauthHelperService.getStorageView(this.oauthHelperService.clientCredsKey);
        if (!(serverUrl in clientCreds || '*' in clientCreds)) {
            this.siteUrlChangeStatusMessages = "This client is not registered to the platform";
            return;
        }
        this.siteUrlChangeStatusMessages = "Retrieving registered applications.....";
        this.oauthHelperService.getRegisteredApplications(
            serverUrl,
            () => {
                this.siteUrlChangeStatusMessages = null;
                this.vvSiteUrl = serverUrl;
                this.localStorageService.storeValue("server_url", serverUrl);
                this.endpointService.setBaseServerUrl(serverUrl);
                $('#site-url-page-change').css('display', 'none');
                $('#site-url-page-default').css('display', 'inline-block');
            },
            () => {
                this.siteUrlChangeStatusMessages = "error in retrieving registered applications";
            }
        )

    }
    /**
     * Clears the Local Storage i.e local storage goes to default state when home page is loaded.
     */
    private reInitializeLocalStorage() {
        localStorage.clear();
    }
    /**
     * Remove the Endpoint URLs.
     */
    private removeSiteUrl() {
        if (this.authorized == true) {
            this.oauthHelperService.unAuthorizeUser();
            this.reInitializeLocalStorage();
            this.router.navigateByUrl('/');
        } else {
            this.reInitializeLocalStorage();
            this.router.navigateByUrl('/');
        }
    }
}
