import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InPageAlertsComponent } from './in-page-alerts.component';

describe('InPageAlertsComponent', () => {
  let component: InPageAlertsComponent;
  let fixture: ComponentFixture<InPageAlertsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InPageAlertsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InPageAlertsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
