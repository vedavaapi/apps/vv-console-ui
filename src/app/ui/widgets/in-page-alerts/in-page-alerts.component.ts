import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-in-page-alerts',
  templateUrl: './in-page-alerts.component.html',
  styleUrls: ['./in-page-alerts.component.scss']
})
export class InPageAlertsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('#failure-alert-close-btn').click(callback => {
        $('.failure-alert-parent').css('display', 'none');
    });

    $('#success-alert-close-btn').click(callback => {
        $('.success-alert-parent').css('display', 'none');
    });
  }

}
