import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingGifTextComponent } from './loading-gif-text.component';

describe('LoadingGifTextComponent', () => {
  let component: LoadingGifTextComponent;
  let fixture: ComponentFixture<LoadingGifTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadingGifTextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadingGifTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
