import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as $ from 'jquery';

@Component({
    selector: 'app-permissions-dropdown',
    templateUrl: './permissions-dropdown.component.html',
    styleUrls: ['./permissions-dropdown.component.scss']
})
export class PermissionsDropdownComponent implements OnInit {

    private permissions: string[] = ["delete", "read", "createChildren", "createAnnos", "updateContent", "updateLinks", "updatePermissions"];

    private myForm: FormGroup;

    private isGranted: boolean = false;
    private isWithdrawn: boolean = false;
    private isIncludeTeam: boolean = false;
    private isNologin: boolean = false;


    constructor(private formBuilder: FormBuilder) { }

    ngOnInit() {
        this.initializeForm();
    }

    /**
     * Initializes the form.
     */
    private initializeForm() {
        this.myForm = this.formBuilder.group({
            check_grant: false,
            check_withdraw: false,
            check_include_team: false,
            check_nologin: false,
            grant_permissions_dropdown: [],
            withdraw_permissions_dropdown: [],
        });
    }

    /**
     * Clears the Selections.
     */
    public clearSelections() {
        this.isGranted = false;
        this.isWithdrawn = false;
        this.isIncludeTeam = false;
        this.isNologin = false;

        if (this.myForm) {
            this.myForm.setValue({
                check_grant: false,
                check_withdraw: false,
                check_include_team: false,
                check_nologin: false,
                grant_permissions_dropdown: [],
                withdraw_permissions_dropdown: []
            });
        }
    }

    /**
     * Returns Selected options along with their values.
     */
    public getPermissionsFormJson() {
        return this.myForm.value;
    }

    /**
     * Callback when Grant Checkbox is clicked.
     */
    private onClickGrantCheckbox() {

        this.isGranted = !this.isGranted;
        if (this.isGranted && this.isWithdrawn) {
            this.isWithdrawn = false;
        }

        this.myForm.setValue({
            check_grant: this.isGranted,
            check_withdraw: this.isWithdrawn,
            check_include_team: this.isIncludeTeam,
            check_nologin: this.isNologin,
            grant_permissions_dropdown: this.myForm.controls['grant_permissions_dropdown'].value,
            withdraw_permissions_dropdown: this.myForm.controls['withdraw_permissions_dropdown'].value
        });

    }

    /**
     * Callback when Include Team check box is clicked.
     */
    private onClickIncludeTeamCheckBox(){
        this.isIncludeTeam = !this.isIncludeTeam;
        this.myForm.setValue({
            check_grant: this.isGranted,
            check_withdraw: this.isWithdrawn,
            check_include_team: this.isIncludeTeam,
            check_nologin: this.isNologin,
            grant_permissions_dropdown: this.myForm.controls['grant_permissions_dropdown'].value,
            withdraw_permissions_dropdown: this.myForm.controls['withdraw_permissions_dropdown'].value
        });
    }

    /**
     * Callback when Nologin check box is clicked.
     */
    private onClickIncludeNoLoginCheckBox(){
        this.isNologin = !this.isNologin;
        this.myForm.setValue({
            check_grant: this.isGranted,
            check_withdraw: this.isWithdrawn,
            check_nologin: this.isNologin,
            check_include_team: this.isIncludeTeam,
            grant_permissions_dropdown: this.myForm.controls['grant_permissions_dropdown'].value,
            withdraw_permissions_dropdown: this.myForm.controls['withdraw_permissions_dropdown'].value
        });
    }

    /**
     * Callback when withdraw checkbox is clicked.
     */
    private onClickWithdrawCheckbox() {
        this.isWithdrawn = !this.isWithdrawn;
        if (this.isGranted && this.isWithdrawn) {
            this.isGranted = false;
        }

        this.myForm.setValue({
            check_grant: this.isGranted,
            check_withdraw: this.isWithdrawn,
            check_include_team: this.isIncludeTeam,
            check_nologin: this.isNologin,
            grant_permissions_dropdown: this.myForm.controls['grant_permissions_dropdown'].value,
            withdraw_permissions_dropdown: this.myForm.controls['withdraw_permissions_dropdown'].value
        });
    }
}
