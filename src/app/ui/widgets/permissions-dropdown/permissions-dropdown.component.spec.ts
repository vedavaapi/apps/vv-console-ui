import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PermissionsDropdownComponent } from './permissions-dropdown.component';

describe('PermissionsDropdownComponent', () => {
  let component: PermissionsDropdownComponent;
  let fixture: ComponentFixture<PermissionsDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PermissionsDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermissionsDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
