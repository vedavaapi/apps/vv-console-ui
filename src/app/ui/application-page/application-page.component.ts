import { Component, OnInit, ViewChild, ElementRef, Renderer2, AfterViewInit } from '@angular/core';
import { EndpointsService } from 'src/app/endpoints.service';
import { OauthHelperService } from 'src/app/services/oauth-helper.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { VedavaapiShellHelperService } from 'src/app/services/vedavaapi-shell-helper.service';
import { ChangeDetectorRef } from '@angular/core';

@Component({
    selector: 'app-application-page',
    templateUrl: './application-page.component.html',
    styleUrls: ['./application-page.component.scss']
})
export class ApplicationPageComponent implements OnInit, AfterViewInit {
    private iframeHtml= '';

    @ViewChild('application_iframe') applicationDiv: ElementRef;

    content = '<button (click)="onclick">This is a Clickable Span</button>';

    applicationId: string;
    applicationUrl: string;
    launchCustomApplication: string;

    constructor(private renderer: Renderer2, private endpointsService: EndpointsService, private oauthHelperService: OauthHelperService, private localStorageService: LocalStorageService, private vedavaapiShellHelperService: VedavaapiShellHelperService, private cdRef: ChangeDetectorRef) {

        let vvSiteUrl = this.localStorageService.getValue('server_url');

        let queryParams = new URLSearchParams(document.location.search.substring(1));
        this.applicationId = queryParams.get('application_id');
        this.launchCustomApplication = queryParams.get('launch_custom_app') || 'false';

        if(!this.applicationId) {
            return;
        }
        this.applicationUrl = this.getApplicationUrl(this.applicationId);
        this.setupIframe()
    }

    getApplicationUrl(applicationId: string) {
        let vvSiteUrl = this.localStorageService.getValue('server_url');

        let registeredApplications = this.oauthHelperService.getApplications(vvSiteUrl)
        if (!registeredApplications) {
            return null;
        }
        let application = null;
        for(let item of registeredApplications) {
            if(item['_id'] == applicationId) {
                application = item;
                break;
            }
        }
        if(!application) {
            return null;
        }
        let applicationUrl = application['url'];
        return applicationUrl;
    }

    setupIframe() {
        let vvSiteUrl = this.localStorageService.getValue('server_url');
        this.iframeHtml = `<iframe title="${this.applicationId}" id="application-iframe" src="`
            + this.applicationUrl + `?vv_site_url=${vvSiteUrl}`
            + '" allowfullscreen="true"'
            + ' width="100%" height="100%" style="height:100vh;border: 0px;"'
            + ' webkitallowfullscreen="true" mozallowfullscreen="true"></iframe>';
    }

    ngOnInit() {
        // this.renderer.setAttribute(this.applicationDiv.nativeElement, 'innerHTML', this.content);
    }

    ngAfterViewInit() {
        if(this.applicationId) {
            let applicationIFrameWindow = document.getElementById('application-iframe')['contentWindow'];
            this.vedavaapiShellHelperService.setupCommunication(applicationIFrameWindow);
        }
        else if(this.launchCustomApplication == 'true'){
            this.setupCustomAppSpecificUi();
        }
    }

    setupCustomAppSpecificUi() {
        let customAppFormContainer = document.getElementById('custom-app-form-container');
        let customAppStatus = document.getElementById('custom-app-status');
        customAppFormContainer.style.display = 'block';
        document.getElementById('custom-app-load-button').onclick = (e) => {
            let customAppName = document.getElementById('custom-app-name')['value'];
            let customAppUrl = document.getElementById('custom-app-url')['value'];
            if(!customAppName || !customAppUrl) {
                customAppStatus.innerText = 'invalid input';
                return;
            }
            customAppFormContainer.style.display = 'none';
            this.applicationId = customAppName;
            this.applicationUrl = customAppUrl;
            this.setupIframe();
            this.cdRef.detectChanges();
            let applicationIFrameWindow = document.getElementById('application-iframe')['contentWindow'];
            this.vedavaapiShellHelperService.setupCommunication(applicationIFrameWindow);
        }
    }

    onclick() {
        console.log('On Click!!!');
    }
}
