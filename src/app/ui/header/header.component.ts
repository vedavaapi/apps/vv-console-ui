import { Component, OnInit } from '@angular/core';
import { OauthHelperService } from 'src/app/services/oauth-helper.service';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/app/services/local-storage.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private oauthHelperService: OauthHelperService, private _router: Router, private localstorageService: LocalStorageService) {}

  ngOnInit() {
  }

  /**
   * Login Action needs to be taken here.
   */
  private loginAction(){
    // alert('Login button Clicked');
    let vvSiteUrl = this.localstorageService.getValue("server_url");
    if(!vvSiteUrl){
        this._router.navigate(['/']);
        return false;
    }
    this.oauthHelperService.initializeAuthorizationFlowInNewWindow('oauth_initializer', false);
    return false;
  }
}
