import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { DOCUMENT } from '@angular/common';
import { EndpointsService } from 'src/app/endpoints.service';
import { OauthHelperService } from 'src/app/services/oauth-helper.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-paged-data',
    templateUrl: './paged-data.component.html',
    styleUrls: ['./paged-data.component.scss']
})

/**
 * Parent class to display Data in Responsive Data Tables
 */
export class PagedDataComponent implements OnInit {


    protected dataTable: any;

    protected authorized: any;

    protected static PAGE_COUNT: number = 15;

    protected startIndex: number = 0;

    protected totalNumber: number = 0; //Temporary

    protected itemsPerPage: number = PagedDataComponent.PAGE_COUNT;

    protected userId: string = null;

    protected displayMode: number = PagedDataComponent.DISPLAY_MODE_NORMAL;

    protected objectId: string = null;


    protected static DISPLAY_MODE_DEFAULT: number = -1;

    protected static DISPLAY_MODE_NORMAL: number = 0;

    protected static DISPLAY_MODE_SINGLE_CHOICE: number = 1;


    constructor(@Inject(DOCUMENT) protected document: Document, protected endpointService: EndpointsService,
        protected oauthHelperService: OauthHelperService, protected cdRef: ChangeDetectorRef, private router: Router,
    ) {
        this.subscribeAuthorizationChangeBroadcast();
    }

    ngOnInit() {
        var that = this;
        $('#search-bar-submit-button').click(function () {
            var searchString = $('#search-bar-input').val();
            if (searchString == null || searchString.trim().length == 0) {
                return;
            }
            $('#search-bar-clear-button').css('display', 'inline-block');
            that.populateData(searchString);
        });

        $('#search-bar-clear-button').click(function () {
            $('#search-bar-input').val('');
            $('#search-bar-clear-button').css('display', 'none');
            that.populateData(null);
        });
        that.populateData(null);
    }

    /**
     * returns the Search String.
     */
    protected getSearchString() {
        return $('#search-bar-input').val();
    }

    // get the updated status of login or logout.
    protected subscribeAuthorizationChangeBroadcast() {
        let bc = new BroadcastChannel(this.oauthHelperService.authorizationChangeBroadcastChannelName);
        bc.onmessage = ({ data }) => {
            console.log('Authorization broadcast', { data });
            if (data) {
                this.updateAuthorizedStatus();
            }
        }
    }

    protected updateAuthorizedStatus() {
        console.log("Paged Component: updateAuthorizedStatus");
        $('#data-table').css('display', 'none');
        $('.search-container').css('display', 'none');
        $('.new-button').css('display', 'none');
        $('.new-button-objects').css('display', 'none');
        $('.pagination-row').css('display', 'none');
        $('.page-header').css("display", "none");
        $('#error-message-text').css('display', 'none');
        $('#permissions-wizard').css('display', 'none');

        this.authorized = this.isAuthorized();
        if (this.authorized) {
            console.log("Reloading paged component");
            window.location.reload();
        } else {
            $('#error-message-text').css('display', 'block');
            $('#error-message-text').text('Please login to see the data.');
        }
    }


    /**
     * Returns true if we are logged in.
     */
    protected isAuthorized() {
        let authorizedCreds = this.oauthHelperService.getAuthorizedCreds();
        this.cdRef.detectChanges();
        var authorized = Boolean(authorizedCreds);
        console.log('isAuthorized', { 'authorized': authorized });
        this.cdRef.detectChanges();
        return authorized;
    }

    /**
     * Returns the Access Token.
     */
    protected getAccessToken() {
        let authorizedCreds = this.oauthHelperService.getAuthorizedCreds();
        if (authorizedCreds && authorizedCreds.hasOwnProperty('access_token')) {
            return authorizedCreds['access_token'];
        }
        return null;
    }

    /**
     * Displays the Error Message
     * @param errorMessage 
     */
    protected showErrorMessage(errorMessage: string) {
        if (errorMessage == null || errorMessage.trim().length == 0) {
            return;
        }

        $('#error-message-text').text(errorMessage);
        $('#error-message-text').css('display', 'block');
    }


    /**
     * Hides all the UI elements.
     */
    protected hideAllUIElements() {
        $('#data-table').css('display', 'none');
        $('.search-container').css('display', 'none');
        $('.new-button').css('display', 'none');
        $('.new-button-objects').css('display', 'none');
        $('#error-message-text').css('display', 'none');
        $('#loading-gif-icon').css('display', 'none');
    }


    /**
     * Methods needs to be Overridden by Child Components. They can fill the datatable with their own data.
     * @param search_string 
     */
    protected populateData(search_string: string) {

    }


    /**
     * Returns the User Id of the currently logged in user.
     */
    protected getCurrentUserId() {
        if (!this.oauthHelperService) {
            return null;
        }

        var userDetails = this.oauthHelperService.getAuthorizedUserDetails(null);
        if (userDetails != null && userDetails.hasOwnProperty('_id')) {
            return userDetails['_id'];
        }
        return null;
    }

    /**
     * Return true if the Object has the specified permission for the given user.
     * @param singleObject 
     * @param permission 
     * @param userId
     */
    protected checkIfPermissionPresent(singleObject: any, permission: string, userId: string) {
        if (!singleObject || !userId)
            return false;
        if (singleObject.hasOwnProperty("resolvedPermissions")
            && singleObject["permissions"]
            && singleObject["permissions"].hasOwnProperty(permission)
            && singleObject["permissions"][permission]
            && singleObject["permissions"][permission].hasOwnProperty("grant")
            && singleObject["permissions"][permission]["grant"]
            && singleObject["permissions"][permission]["grant"].hasOwnProperty("users")
            && singleObject["permissions"][permission]["grant"]["users"]) {
            var userArray = singleObject["permissions"][permission]["grant"]["users"];
            return (userArray.indexOf(userId) > -1);
        }
        return false;
    }

    /**
     * Returns true if a User Id is a member of the team.
     * @param userId 
     * @param teamObj 
     */
    protected isMemberOfTeam(userId: string, teamObj: any) {
        if (!teamObj || !userId)
            return false;
        if (teamObj.hasOwnProperty("members")
            && teamObj["members"]) {
            var membersArray = teamObj["members"];
            return (membersArray.indexOf(userId) > -1);
        }
        return false;
    }


    /**
     * Inserts a Dismissable alert message in the Page.
     * @param parent_div_id 
     * @param errorMessage 
     */
    protected insertAlertMessage(span_id: string, message: string) {
        if (!message || message.trim().length == 0 || !span_id) {
            return;
        }
        $('.' + span_id + " span").text(message);
        $('.' + span_id).css('display', 'block');
    }


    /**
     * Callback when an Item is selcted.
     * @param itemId 
     */
    protected onItemSelected(itemId: string) {

    }
}//end of class declaration
