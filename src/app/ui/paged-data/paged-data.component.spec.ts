import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagedDataComponent } from './paged-data.component';

describe('PagedDataComponent', () => {
  let component: PagedDataComponent;
  let fixture: ComponentFixture<PagedDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagedDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagedDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
