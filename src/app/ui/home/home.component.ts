import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import * as $ from 'jquery';
import { EndpointsService } from 'src/app/endpoints.service';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { OauthHelperService } from 'src/app/services/oauth-helper.service';
import { ChangeDetectorRef } from '@angular/core';
import { identifierModuleUrl } from '@angular/compiler';
import { from } from 'rxjs';
import { BooksService } from 'src/app/services/books-service';
import { ScannedBook } from 'src/app/services/models';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})


export class HomeComponent implements OnInit {

  private currentPageIndex: number;

  private vvSiteUrl : string;

  private books: any;
  
  @ViewChild('slickModal') slickModal;
   
  // stores the images of books.
  slides = [];

  slideConfig = {"slidesToShow": 8, "slidesToScroll": 1,

    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 6
        }
      },
      {
        breakpoint: 990,
        settings: {
          slidesToShow: 5
        }
      },
      {
        breakpoint: 764,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 460,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 350,
        settings: {
          slidesToShow: 1
        }
      },
    ]

  };
  
  // adding book image to slides object.

  addSlide(book: ScannedBook) {

    if(!book) {
        return;
    }
      let coverImageUrl = book.cover ? book.cover.getIIIFOrDefaultUrl(this.endpointService.getBaseUrl(), 'full', ',150', '0', 'default', 'png') : ((book.jsonClass == 'ScannedBook') ? "assets/default-book-īmg.png" : "assets/textdoc.svg");
      this.slides.push({img: coverImageUrl, book_id: book._id, jsonClass: book.jsonClass});
  }
  
  removeSlide() {
    this.slides.length = this.slides.length - 1;
  }
  
  slickInit(e) {
    console.log('slick initialized');
  }
  
  breakpoint(e) {
    console.log('breakpoint');
  }
  
  afterChange(e) {
    console.log('afterChange');
  }
  
  beforeChange(e) {
    console.log('beforeChange');
  }
  slideLeft(){
    this.currentPageIndex--;
    if(this.currentPageIndex < 0){
      this.currentPageIndex = 0;
      return;
    }//if
    this.slickModal.slickGoTo(this.currentPageIndex);
  }

  slideRight(){
    this.currentPageIndex++;
    if(this.currentPageIndex >=  this.slides.length){
      this.currentPageIndex = this.slides.length - 1 ;
      return;
    }//if
    this.slickModal.slickGoTo(this.currentPageIndex);
  }


    /**
     * constructor
     * @param endpointService 
     * @param localStorageService 
     * @param oauthHelperService 
     * @param booksService 
     * @param http 
     * @param cdRef 
     * @param router 
     */
    constructor(private endpointService: EndpointsService, private localStorageService: LocalStorageService, private oauthHelperService: OauthHelperService,
        private booksService: BooksService, private http: HttpClient, private cdRef: ChangeDetectorRef, private router: Router) {
        this.vvSiteUrl = this.localStorageService.getValue('server_url');
    }
  


  /**
   * Saves the Endpoint URLs.
   */
  private retrieveApplications(){
    $('#modal-error-message-span').text("retrieving registered applications.....");
    $('#modal-error-message-span').css('display', 'inline-block');
    this.oauthHelperService.getRegisteredApplications(
      this.vvSiteUrl,
      () => {
        $("#modal_cancel_btn").click();
      },
      () => {
        $('#modal-error-message-span').text("error in retrieving registered applications");
      }
    )
    
  }

  
  /**
   * Init method
   */
  ngOnInit() {
    
    let _this = this;
    let prevServerUrl = this.localStorageService.getValue("server_url");

    function init() {
      console.log('inside init');
      let serverUrl = _this.localStorageService.getValue('server_url');
      if(prevServerUrl && serverUrl != prevServerUrl) {
          _this.oauthHelperService.unAuthorizeUser(prevServerUrl);
      }
      _this.vvSiteUrl = serverUrl;
      _this.endpointService.setBaseServerUrl(serverUrl);
      _this.retrieveApplications();
      _this.getLatestAddedBooks();
    }

    function siteInfoSuccessCallback() {
      $('#modal-error-message-span').text("retrieved site info successfully");
      init();
    }

    function siteInfoErrorCallback() {
      $('#modal-error-message-span').text("error in retrieving site info");
      $('#modal-error-message-span').css('display', 'inline-block');
    }

    $('#modal-error-message-span').text("retrieving site info.");
    $('#modal-error-message-span').css('display', 'inline-block');
    this.oauthHelperService.getVVSiteDetails(siteInfoSuccessCallback, siteInfoErrorCallback);
  }

  /**
   * Fetches the Latest Added Books from Server.
   */
  private getLatestAddedBooks() {
    this.currentPageIndex = 0;
    $('#home-books-img-dev').css('display', 'none');

    let successCallback = (response: any) => {
        this.books = response.items;
        if(!this.books.length) {
            return;
        }
        $('#home-books-img-dev').show();
        for(let book of this.books) {
            this.addSlide(book);
        }
    }

    this.booksService.getBooks(this.vvSiteUrl, {}, undefined, [["created", -1]], 0, 16, false, successCallback);
  }

}
