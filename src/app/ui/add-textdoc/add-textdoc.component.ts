import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import * as $ from 'jquery';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { EndpointsService } from 'src/app/endpoints.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Library } from 'src/app/services/models';

@Component({
    selector: 'app-add-textdoc',
    templateUrl: './add-textdoc.component.html',
    styleUrls: ['./add-textdoc.component.scss']
})

/**
 * Class for Uploading a Book, it's thumbnail and other pages.
 */
export class AddTextDocComponent implements OnInit {

    private currentLibraryId: string = '0';

    private currentLibraryName: string = '';

    private currentLibrary: Library;
    private librarySelectorConfig: any;

    private textDocTitle: string;

    private createdTextDocId: any;


    constructor(@Inject(DOCUMENT) private document: Document, private endpointService: EndpointsService, private http: HttpClient,
        private spinner: NgxSpinnerService) {

        let queryParams = new URLSearchParams(document.location.search.substring(1));
        this.currentLibraryId = queryParams.get('library_id') || '0';
        this.currentLibraryName = queryParams.get('name') || '';

        this.librarySelectorConfig = {
            shouldUpdateQueryParams: true,
            selectionChangeCallback: (library) => { this.onLibraryChange(library); },
            listenAuthChanges: true,
            selected: this.currentLibraryId
        };
    }

    private onLibraryChange(library: Library) {
        this.currentLibraryId = library._id;
        this.currentLibrary = library;
        this.currentLibraryName = library.name;
    }

    ngOnInit() {
    }


    /**
     * Performs validation of basic book information.
     * @param $textDocTitle 
     * @param $textDocAuthor 
     * @param $textDocDescription 
     */
    private validateFieldValues($textDocTitle, $textDocAuthor, $textDocDescription) {
        if ($textDocTitle == null || $textDocTitle.trim().length < 3) {
            $('#textdoc_title_minlength_error').css('display', 'block');
            return false;
        }

        if ($textDocAuthor == null || $textDocAuthor.trim().length < 3) {
            $('#textdoc_author_minlength_error').css('display', 'block');
            return false;
        }

        if ($textDocDescription == null || $textDocDescription.trim().length < 10) {
            $('#textdoc_description_minlength_error').css('display', 'block');
            return false;
        }
        return true;
    }

    
    /**
     * Creates a Book Entry on the Vedavaapi Platform.
     * @param $textDocTitle 
     * @param $textDocAuthor 
     * @param $textDocType 
     * @param $textDocDescription 
     */
    createTextDoc($textDocTitle, $textDocAuthor, $textDocDescription) {

        this.hideErrorDivs();

        if (!this.validateFieldValues($textDocTitle, $textDocAuthor, $textDocDescription)) {
            return;
        }

        this.textDocTitle = $textDocTitle;

        this.spinner.show();

        let authors = $textDocAuthor.split(',');

        let metadataJson = [
            {
                jsonClass: "MetadataItem",
                label: "description",
                value: $textDocDescription
            }];

        let requestJson = {
            author: authors, jsonClassLabel: 'document',
            jsonClass: "TextDocument", metadata: metadataJson,
            title: $textDocTitle,
            source: this.currentLibraryId
        };

        const httpUploadOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
        };

        let request_params = "resource_jsons=" + encodeURIComponent(JSON.stringify([requestJson]));
        return this.http.post(this.endpointService.getBaseUrl() + '/objstore/v1/resources', request_params,
            httpUploadOptions).subscribe(
                response => {
                    this.spinner.hide();

                    $("#upload_textdoc_container").css("display", "none");
                    $("#textdoc_title").attr("readonly", "true");
                    $("#textdoc_author").attr("readonly", "true");
                    $("#textdoc_description").attr("readonly", "true");

                    const responseStr = JSON.stringify(response);
                    JSON.parse(responseStr, (key, value) => {
                        if (key === '_id') {
                            this.createdTextDocId = value;
                        }
                        return value;
                    });

                    if (this.createdTextDocId == null) {
                        $("#error-add-textdoc").text("Server did not send the textdocument Id. Cannot continue with the upload process. Plese retry.");
                        $("#error-add-textdoc").css("display", "block");
                        return;
                    }

                    $("#info-textdoc-created").text("The Text Document '" + $textDocTitle + "' has been created");
                    $("#textdoc-add-success-status").css('display', 'block');
                    this.textDocTitle = $textDocTitle;
                },
                error => {
                    this.spinner.hide();
                    let errorCode = this.getResponseErrorCode(error);
                    if (errorCode == 403) {
                        $("#error-add-textdoc").text("You are forbidden to perform this operatiton. You may not have the necessary permissionss. Please contact the  platform administrator.");
                    } else if (errorCode == 401) {
                        $("#error-add-textdoc").text("Unauthorized call, session may have expired. Please login afresh and retry.");
                    } else if (errorCode == 400) {
                        $("#error-add-textdoc").text("Bad Request. Please contact the platform administartor.");
                    } else {
                        $("#error-add-textdoc").text("Unknown error, please contact the platform administrator or retry after some time.");
                    }

                    $("#error-add-textdoc").css("display", "block");

                }
            );
    }

    
    /**
     * Hides the Error Div elements
     */
    private hideErrorDivs() {
        $('#textdoc_title_minlength_error').css('display', 'none');
        $('#textdoc_author_minlength_error').css('display', 'none');
        $('#textdoc_description_minlength_error').css('display', 'none');
    }

    /**
     * 
     * @param error Returns the Error Code.
     */
    private getResponseErrorCode(error) {
        const errorStr = JSON.stringify(error);
        let errorCode = -1;
        JSON.parse(errorStr, (key, value) => {
            if (key === 'code') {
                errorCode = value;
            }
            return value;
        });
        return errorCode;
    }
}// end of class declaration
