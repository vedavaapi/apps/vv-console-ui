import { Component, OnInit, Inject, ChangeDetectorRef, Injectable } from '@angular/core';
import { JsonUtilsService } from 'src/app/services/json-utils.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { EndpointsService } from 'src/app/endpoints.service';
import { OauthHelperService } from 'src/app/services/oauth-helper.service';

import * as $ from 'jquery';
import { GroupsApiService } from 'src/app/services/groups-api.service';
import { UserApiService } from 'src/app/services/user-api.service';
import { VedavaapiApiService } from 'src/app/services/vedavaapi-api.service';

@Component({
    selector: 'app-create-object',
    templateUrl: './create-object.component.html',
    styleUrls: ['./create-object.component.scss']
})
/**
 *  Returns create/edit  options of objects. 
 */
export class CreateObjectComponent implements OnInit {

    private objectJsonSchema: any = { "properties": { "test": { "description": "Test field", "type": "string" } } };

    private schemaType: string = null;

    private teamIdentifier: string = null;

    private isObjectRoot: boolean = false;

    private isObjectId: string = null;

    private isEditObject: boolean = null;

    private objectDetails: any = [];

    constructor(
        @Inject(DOCUMENT) protected document: Document, protected endpointService: EndpointsService,
        protected oauthHelperService: OauthHelperService, protected cdRef: ChangeDetectorRef,
        private _jsonUtilsService: JsonUtilsService,
        private _groupsApiService: GroupsApiService,
        private _userApiService: UserApiService,
        private _vedavaapiApiService: VedavaapiApiService,
        private _route: ActivatedRoute,
        private http: HttpClient,
        private _router: Router) {
        this.subscribeAuthorizationChangeBroadcast();
    }

    ngOnInit() {

        $('#loading-gif-text').text('Initializing, please wait ...');
        $('#loading-gif-icon').css('display', 'block');


        this._route.queryParamMap.subscribe(queryParams => {
            this.teamIdentifier = queryParams.get("team_id");
        });
        this._route.queryParams.subscribe(params => {
            this.isObjectRoot = params['object-root'];
            this.isEditObject = params['edit-object'];

            this._route.params.subscribe(params => {
                this.schemaType = params['schema_type'];
                this.isObjectId = params['_id'];
                if (this.isObjectId !=null && this.isEditObject){
                    this.fetchObjectDetails(this.isObjectId);
                }else{
                    this.fetchAndRenderSchema(this.schemaType);
                }
               
            });
        });
    }


    // get the updated status of login or logout.
    private subscribeAuthorizationChangeBroadcast() {
        let bc = new BroadcastChannel(this.oauthHelperService.authorizationChangeBroadcastChannelName);
        bc.onmessage = ({ data }) => {
            console.log('Authorization broadcast', { data });
            if (data) {
                this.updateAuthorizedStatus();
            }
        }
    }

    private updateAuthorizedStatus() {
        if (this.isAuthorized()) {
            //May be have to laod data here again
            console.log('updateAuthorizedStatus');
            window.location.reload();
        }
    }

    /**
    * Returns true if we are logged in.
    */
    private isAuthorized() {
        let authorizedCreds = this.oauthHelperService.getAuthorizedCreds();
        this.cdRef.detectChanges();
        var authorized = Boolean(authorizedCreds);
        console.log('isAuthorized', { 'authorized': authorized });
        this.cdRef.detectChanges();
        return authorized;
    }

    /**
     * Returns the Access Token.
     */
    private getAccessToken() {
        let authorizedCreds = this.oauthHelperService.getAuthorizedCreds();
        if (authorizedCreds && authorizedCreds.hasOwnProperty('access_token')) {
            return authorizedCreds['access_token'];
        }
        return null;
    }
    
    /**
     * Fetches the object details from server.
     * @param objectId 
     */
    private fetchObjectDetails(objectId: string) {
        if (!this.isAuthorized()) {
            $('#loading-gif-icon').css('display', 'none');
            this.showErrorMessage('You are not logged in. Please login and try.');
            return;
        }
        if (objectId == null){
            this.showErrorMessage('Object resource Id is missing.');
            return;
        }
        $('#loading-gif-text').text('Fetching Object Details ...');
        $('#loading-gif-icon').css('display', 'block');

        let httpImageHeaders: HttpHeaders = new HttpHeaders({
            'Content-Type': 'application/json',
        });

        this.http.get(
            this.endpointService.getBaseUrl() + '/objstore/v1/resources/' + objectId,
            { headers: httpImageHeaders })
            .subscribe(
                response => {
                    $('#loadding-gif-icon').css('display', 'none');
                    this.objectDetails = response;
                    if (this.objectDetails != null) {
                        this.fetchAndRenderSchema(this.schemaType);
                    }
                },
                errorMessage => {
                    $('#loading-gif-icon').css('display', 'none');
                    this.showErrorMessage('Object resource does not exist on the server.');
                });

    }
    /**
     * Fetches the Schema from Server and renders the  form.
     * @param schema_type 
     */
    private fetchAndRenderSchema(schema_type: string) {

        if (!this.isAuthorized()) {
            $('#loading-gif-icon').css('display', 'none');
            this.showErrorMessage('You are not logged in. Please login and try.');
            return;
        }

        $('#loading-gif-text').text('Fetching Schema ...');
        $('#loading-gif-icon').css('display', 'block');
        if (schema_type) {

            if (schema_type == null) {
                $('#loading-gif-text').css('display', 'none');
                this.showErrorMessage('Schema Type should be part of URL.');
                return;
            }
            this.displayFormTitle(schema_type);
            this._jsonUtilsService.getSchemaJSON(schema_type, successResponse => {
                $('#loading-gif-icon').css('display', 'none');

                if (!successResponse) {
                    this.showErrorMessage('Unable to fetch the schema object.');
                    return;
                }
                console.log('Original Schema: ' + JSON.stringify(successResponse));

                var transformedSchema = this._jsonUtilsService.tranformJsonSchema(successResponse, true, true);

                if (schema_type == 'User') {
                    if(this.isEditObject){
                        if (transformedSchema.hasOwnProperty('properties') && transformedSchema['properties'].hasOwnProperty('name') && this.objectDetails.hasOwnProperty('name')) {
                            transformedSchema['properties']["name"] = { "description": "Name of the user", "default": this.objectDetails['name'], "type": "string" };
                        }
                        if (transformedSchema.hasOwnProperty('properties') && transformedSchema['properties'].hasOwnProperty('email') && this.objectDetails.hasOwnProperty('email')) {
                            transformedSchema['properties']["email"] = { "default": this.objectDetails['email'], "type": "string" };
                        }
                        if (transformedSchema.hasOwnProperty('properties') && transformedSchema['properties'].hasOwnProperty('password') && this.objectDetails.hasOwnProperty('password')) {
                            transformedSchema['properties']["password"] = { "default": this.objectDetails['password'], type: "hidden" };
                            delete transformedSchema['required'];
                        }
                    }else{
                        if (transformedSchema.hasOwnProperty('properties') && transformedSchema['properties'].hasOwnProperty('name')) {
                            transformedSchema['properties']["name"] = { "description": "Name of the user", "type": "string" };
                        }
    
                        if (transformedSchema.hasOwnProperty('properties') && transformedSchema['properties'].hasOwnProperty('password')
                            && transformedSchema['properties']["password"].hasOwnProperty('format')) {
                            delete transformedSchema['properties']["password"]["format"];
                        }
                    }
                   
                }else if (schema_type == 'Library' && this.isEditObject) {
                    if (transformedSchema.hasOwnProperty('properties') && transformedSchema['properties'].hasOwnProperty('name') && this.objectDetails.hasOwnProperty('name')) {
                        transformedSchema['properties']["name"] = { "default": this.objectDetails['name'], "type": "string" };
                    }
                    if (transformedSchema.hasOwnProperty('properties') && transformedSchema['properties'].hasOwnProperty('source') && this.objectDetails.hasOwnProperty('source')) {
                        transformedSchema['properties']["source"] = { "default": this.objectDetails['source'], "type": "string" };
                    }
                    if (transformedSchema.hasOwnProperty('properties') && transformedSchema['properties'].hasOwnProperty('description') && this.objectDetails.hasOwnProperty('description')) {
                        transformedSchema['properties']["description"] = { "default": this.objectDetails['description'], "type": "string" };
                    }
                } else if (schema_type == 'Team' && this.isEditObject) {
                    if (transformedSchema.hasOwnProperty('properties') && transformedSchema['properties'].hasOwnProperty('name') && this.objectDetails.hasOwnProperty('name')) {
                        transformedSchema['properties']["name"] = { "default": this.objectDetails['name'], "type": "string" };
                    }
                    if (transformedSchema.hasOwnProperty('properties') && transformedSchema['properties'].hasOwnProperty('source') && this.objectDetails.hasOwnProperty('source')) {
                        transformedSchema['properties']["source"] = { "default": this.objectDetails['source'], "type": "string" };
                    }
                }
                this.objectJsonSchema = transformedSchema;
                console.log('Transformed Schema: ' + JSON.stringify(this.objectJsonSchema));
                $('.json-schema-form-element').css('display', 'block');
                $('.form-control').css('border-radius', '0px');

            },
                errorMessage => {
                    $('#loading-gif-icon').css('display', 'none');
                    this.showErrorMessage(errorMessage);
                });
        }
    }

    /**
     * Shows Error Message
     * @param errorMessage 
     */
    protected showErrorMessage(errorMessage: string) {
        if (errorMessage == null || errorMessage.trim().length == 0) {
            return;
        }

        $('#error-message-text').text(errorMessage);
        $('#error-message-text').css('display', 'block');
    }
    /**
     * Displays create/update objects form titles.
     * @param schema_type 
     */
    private displayFormTitle(schema_type: string) {
        if (schema_type == null) {
            return;
        }
        switch (schema_type) {
            case 'Team':
                if(this.isEditObject)
                    $('#form-title').text('Update the team details.');
                else
                    $('#form-title').text('Please fill the form below to create a team.');
                break;

            case 'User': {
                if (this.teamIdentifier)
                    $('#form-title').text('Please fill the form below to create a User account and add it to the team.');
                else if (this.isEditObject)
                    $('#form-title').text('Update user details.');
                else 
                     $('#form-title').text('Please fill the form below to create a User account.');
                break;
            }

            case 'Library': {
                if(this.isEditObject)
                    $('#form-title').text('Update the library details.');
                else
                    $('#form-title').text('Please fill the form below to create a Library.');
                break;
            }
            default:
                break;
        }
    }

    /**
     * Callback from JSON form when submit button is clicked.
     * @param $event 
     */
    private submitButtonClicked($event) {

        if (!this.schemaType || !$event) {
            return;
        }

        console.log(JSON.stringify($event));
        var errorCallback = errorMessage => {
            $('#loading-gif-icon').css('display', 'none');
            $('.json-schema-form-element').css('display', 'block');
            this.showErrorMessage(errorMessage);
        };
        $('#loading-gif-icon').css('display', 'block');
        $('.json-schema-form-element').css('display', 'none');

        switch (this.schemaType) {

            case 'Team': {
                if (this.isEditObject) {
                    let successMessage = "Success! Updated the Team";
                    let errorMessage = "Error! Unable to updated the Team.Please try again.";
                    let updatingText = "Updating team, please wait ...";
                    var path = null;
                    if (this.isObjectRoot != null && this.isObjectRoot != false) {
                        path = "objects";
                    } else {
                        path = "teams";
                    }
                    this.updateObjectDetails($event, path, successMessage, errorMessage, updatingText);
                } else {
                    $('#loading-gif-text').text('Creating team, please wait ...');
                    var groups_json = $event;
                    groups_json['jsonClass'] = this.schemaType;
                    this._groupsApiService.createGroup(this.getAccessToken(), groups_json,
                        succesResponse => {
                            $('#loading-gif-icon').css('display', 'none');
                            $('.json-schema-form-element').css('display', 'none');
                            if (this.isObjectRoot != null && this.isObjectRoot != false) {
                                this._router.navigate(['objects']).then(nav => {
                                    $('#success-alert').css('display', 'block');
                                    $('#success-alert-message').text('Success! Created the Team.');
                                }, err => {
                                    console.log(err) // when there's an error
                                });
                            } else {
                                this._router.navigate(['teams']).then(nav => {
                                    $('#success-alert').css('display', 'block');
                                    $('#success-alert-message').text('Success! Created the Team.');
                                }, err => {
                                    console.log(err) // when there's an error
                                });
                            }
                        },
                        errorCallback => {
                            $('#loading-gif-icon').css('display', 'none');
                            $('.json-schema-form-element').css('display', 'block');
                            $('#error-alert').css('display', 'block');
                            $('#error-alert-message').text("Error! Unable to create the Team. Please try again.");
                        }
                    )
                }
                break;
            }

            case 'User': {
                if (this.isEditObject) {
                    let successMessage = "Success! Updated the User.";
                    let errorMessage = "Error! Unable to updated the User details. Please try again.";
                    let updatingText = "Updating user account, please wait ...";
                    var path = null;
                    if (this.isObjectRoot != null && this.isObjectRoot != false) {
                        path = "objects";
                    } else {
                        path = "teams";
                    }
                    this.updateObjectDetails($event, path, successMessage, errorMessage, updatingText);
                } else {
                    $('#loading-gif-text').text('Creating user account, please wait ...');
                    var user_json = $event;
                    user_json['jsonClass'] = this.schemaType;
                    this._userApiService.createUser(this.getAccessToken(), user_json, this.teamIdentifier,
                        successResponse => {
                            $('#loading-gif-icon').css('display', 'none');
                            $('.json-schema-form-element').css('display', 'none');
                            if (this.isObjectRoot != null && this.isObjectRoot != false) {
                                this._router.navigate(['objects']).then(nav => {
                                    $('#success-alert').css('display', 'block');
                                    var successMessage = 'Success! Created the User account.';
                                    $('#success-alert-message').text(successMessage);
                                }, err => {
                                    console.log(err) // when there's an error
                                });
                            } else {
                                this._router.navigate(['teams']).then(nav => {
                                    $('#success-alert').css('display', 'block');

                                    var successMessage = 'Success! Created the User account. You can add him to any of the teams listed below.';
                                    if (this.teamIdentifier) {
                                        successMessage = 'Success! Created the User account. The User has been added to the team you had selected.';
                                    }
                                    $('#success-alert-message').text(successMessage);
                                }, err => {
                                    console.log(err) // when there's an error
                                });
                            }
                        },
                        errorCallback => {
                            $('#loading-gif-icon').css('display', 'none');
                            $('.json-schema-form-element').css('display', 'block');
                            $('#error-alert').css('display', 'block');
                            $('#error-alert-message').text("Error! Unable to create the User account. Please try again.");
                        }
                    );
                }
                break;
            }

            case 'Library': {
                if (this.isEditObject) {
                    let successMessage = "Success! The Library object has been updated.";
                    let errorMessage = "Error! Unable to updated the Library details.Please try again.";
                    let updatingText = "Updating library, please wait ...";
                    var path = null;
                    if (this.isObjectRoot != null && this.isObjectRoot != false) {
                        path = "objects";
                    } else {
                        path = "libraries";
                    }
                    this.updateObjectDetails($event, path, successMessage, errorMessage, updatingText);
                } else {
                    $('#loading-gif-text').text('Creating library, please wait ...');
                    var resources_json = $event;
                    resources_json['jsonClass'] = this.schemaType;
                    if (this.isEditObject) {
                        resources_json['_id'] = this.isObjectId;
                    }
                    this._vedavaapiApiService.createResource(this.getAccessToken(), resources_json,
                        successResponse => {
                            $('#loading-gif-icon').css('display', 'none');
                            $('.json-schema-form-element').css('display', 'none');
                            if (this.isObjectRoot != null && this.isObjectRoot != false) {
                                this.navigateAndDisplaySuccessMessage('objects', 'Success! The Library object has been created. You can now add books to the library');
                            } else {
                                this.navigateAndDisplaySuccessMessage('libraries', 'Success! The Library object has been created. You can now add books to the library');
                            }
                        },
                        errorCallback => {
                            $('#loading-gif-icon').css('display', 'none');
                            $('.json-schema-form-element').css('display', 'block');
                            $('#error-alert').css('display', 'block');
                            $('#error-alert-message').text("Error! Unable to create the Library object. Please try again.");
                        }
                    );
                }
                break;
            }
            default:
                $('#loading-gif-icon').css('display', 'none');
                $('.json-schema-form-element').css('display', 'block');
                break;
        }
    }
    /**
     * This method is used to update the object details.
     * @param resources_json 
     * @param path 
     * @param successMessage 
     * @param errorMessage 
     * @param updatingText 
     */
    private updateObjectDetails(resources_json: any, path: string, successMessage: string, errorMessage : string, updatingText:string){
            
        if (resources_json == null || path == null) {
            return;
        }

        if(updatingText == null){
            updatingText = "Updating the object details...";
        }
        if(successMessage == null){
            successMessage = "Success! Updated the Object."
        }
        if(errorMessage == null){
            errorMessage = "Error! Unable to updated Object. Please try again."
        }
        $('#loading-gif-text').text(updatingText);
        resources_json['_id'] = this.isObjectId;
        resources_json['jsonClass'] = this.schemaType;
        this._vedavaapiApiService.createResource(this.getAccessToken(), resources_json,
            succesResponse => {
                $('#loading-gif-icon').css('display', 'none');
                $('.json-schema-form-element').css('display', 'none');
                this._router.navigate([path]).then(nav => {
                    $('#success-alert').css('display', 'block');
                    $('#success-alert-message').text(successMessage);
                }, err => {
                    console.log(err) // when there's an error
                });
              
            },
            errorCallback => {
                $('#loading-gif-icon').css('display', 'none');
                $('.json-schema-form-element').css('display', 'block');
                $('#error-alert').css('display', 'block');
                $('#error-alert-message').text(errorMessage);
            }
        )
    }

    /**
     * Navigates to page specified in path and displays the corresponding error message.
     * @param path 
     * @param message 
     */
    private navigateAndDisplaySuccessMessage(path: string, message: string) {
        if (path == null || message == null) {
            return;
        }
        this._router.navigate([path]).then(nav => {
            $('#success-alert').css('display', 'block');

            $('#success-alert-message').text(message);
        }, err => {
            console.log(err) // when there's an error
        });
    }
    /**
     *  Close the alert.
     */
    private onAlertClose(){
        $('#error-alert').css('display', 'none');
    }
}
