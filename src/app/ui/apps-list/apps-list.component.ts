import { Component, OnInit } from '@angular/core';
import { OauthHelperService } from 'src/app/services/oauth-helper.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { ChangeDetectorRef } from '@angular/core';
import { Pipe, PipeTransform } from '@angular/core';

@Component({
    selector: 'apps-list',
    templateUrl: './apps-list.component.html',
})
export class AppsListComponent implements OnInit {

    constructor(private oauthHelperService: OauthHelperService, private localStorageService: LocalStorageService, private cdRef: ChangeDetectorRef) {

    }

    ngOnInit() {
        this.subscribeToApplicationsUpdateBroadcast();
    }

    private subscribeToApplicationsUpdateBroadcast() {
        let bc = new BroadcastChannel(this.oauthHelperService.applicationsUpdateBroadcastChannelName);
        bc.onmessage = ({ data }) => {
            if (data) {
                this.cdRef.detectChanges();
            }
        }
    }

    applicationShellUrl(application: object) {
        let applicationId = application['_id'];
        return `applications?application_id=${encodeURIComponent(applicationId)}`;
    }

}

@Pipe({
    name: 'getApplications',
    pure: false
})
export class GetApplicationsPipe implements PipeTransform{

    constructor(private oauthHelperService: OauthHelperService, private localStorageService: LocalStorageService) {

    }

    public transform() {
        // console.log('inside getApplications');
        let vvSiteUrl = this.localStorageService.getValue('server_url');
        if (!vvSiteUrl) {
            return;
        }
        let applicationInstances = this.oauthHelperService.getApplications(vvSiteUrl) || [];
        // console.log({applicationInstances});
        return applicationInstances;
    }
}
