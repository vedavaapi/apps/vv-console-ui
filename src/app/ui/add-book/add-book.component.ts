import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import * as $ from 'jquery';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { EndpointsService } from 'src/app/endpoints.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Library } from 'src/app/services/models';

@Component({
    selector: 'app-add-book',
    templateUrl: './add-book.component.html',
    styleUrls: ['./add-book.component.scss']
})

/**
 * Class for Uploading a Book, it's thumbnail and other pages.
 */
export class AddBookComponent implements OnInit {

    private currentLibraryId: string = '0';

    private currentLibraryName: string = '';

    private currentLibrary: Library;
    private librarySelectorConfig: any;

    private bookTitle: string;

    private uploadedBookId: any;

    private isThumbnailAsPerAspectRatio: boolean = false;

    private selectedThumbnailFile: File;

    private imagePreview: any;

    private selectedPageFile: File;

    private bookPageCount: number;

    private currentPageIndex: number;

    private pageFilesArraySelected: Array<File> = [];

    private resourceIds: Array<any> = [];

    @ViewChild('slickModal') slickModal;

    private slides = [];

    private slideConfig = {
        "slidesToShow": 8, "slidesToScroll": 1, "loop": true, "infinite": false, "arrows": true,
        prevArrow: "<img class='a-left control-c prev slick-prev' src='assets/carousel-left-chev.png' style='top:0%;height: 39px; width: 34px;display:block;transform:none; margin-top:42px;'>",
        nextArrow: "<img class='a-right control-c next slick-next' src='assets/carousel-right-chev.png' style='top:0%;height: 39px; width: 34px;display:block; margin-top: 42px;transform:none'>",

        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 6
                }
            },
            {
                breakpoint: 990,
                settings: {
                    slidesToShow: 5
                }
            },
            {
                breakpoint: 764,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 460,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 350,
                settings: {
                    slidesToShow: 1
                }
            },
        ]
    };

    /**
     * Adds a Slide to the carousel
     * @param fileId 
     */
    addSlide(fileId: string) {
        if (fileId != null) {
            var pageUrl = this.endpointService.getBaseUrl() + "/iiif_image/v1/" + "objstore/" + fileId + "/full/100,/0/default.jpg";
            console.log("Added ScannedPage URL: " + pageUrl);
            this.slides.push({ img: pageUrl });
            this.currentPageIndex++;
        }

        this.slideConfig.slidesToScroll = this.bookPageCount;
    }

    removeSlide() {
        this.slides.length = this.slides.length - 1;
    }

    slickInit(e) {
        console.log('slick initialized');
    }

    breakpoint(e) {
        console.log('breakpoint');
    }

    afterChange(e) {
        console.log('afterChange');
    }

    beforeChange(e) {
        console.log('beforeChange');
    }

    slideLeft() {
        this.currentPageIndex--;
        if (this.currentPageIndex < 0) {
            this.currentPageIndex = 0;
            return;
        }//if
        this.slickModal.slickGoTo(this.currentPageIndex);
    }

    slideRight() {
        this.currentPageIndex++;
        if (this.currentPageIndex >= this.slides.length) {
            this.currentPageIndex = this.slides.length - 1;
            return;
        }//if
        this.slickModal.slickGoTo(this.currentPageIndex);
    }

    constructor(@Inject(DOCUMENT) private document: Document, private endpointService: EndpointsService, private http: HttpClient,
        private spinner: NgxSpinnerService) {

        let queryParams = new URLSearchParams(document.location.search.substring(1));
        this.currentLibraryId = queryParams.get('library_id') || '0';
        this.currentLibraryName = queryParams.get('name') || '';

        this.librarySelectorConfig = {
            shouldUpdateQueryParams: true,
            selectionChangeCallback: (library) => { this.onLibraryChange(library); },
            listenAuthChanges: true,
            selected: this.currentLibraryId
        };
    }

    private onLibraryChange(library: Library) {
        this.currentLibraryId = library._id;
        this.currentLibrary = library;
        this.currentLibraryName = library.name;
    }

    ngOnInit() {
        this.currentPageIndex = 0;
    }


    /**
     * Performs validation of basic book information.
     * @param $book_title 
     * @param $book_author_information 
     * @param $book_description 
     */
    private validateFieldValues($book_title, $book_author_information, $book_description) {
        if ($book_title == null || $book_title.trim().length < 3) {
            $('#book_title_minlength_error').css('display', 'block');
            return false;
        }

        if ($book_author_information == null || $book_author_information.trim().length < 3) {
            $('#book_author_minlength_error').css('display', 'block');
            return false;
        }

        if ($book_description == null || $book_description.trim().length < 10) {
            $('#book_description_minlength_error').css('display', 'block');
            return false;
        }
        return true;
    }

    /**
     * Shows Add Page Component
     */
    showAddPageComponents() {
        $("#add_book_thumbnail_container").css("display", "none");
        $("#add_page_container").css("display", "block");

        if (this.bookTitle != null) {
            $("#book-title-carousel").text("Books > " + this.bookTitle);
        }
    }
    /**
     * Uploads the Page of a Book.
     */
    onPageUpload() {
        $("#error-upload-page").css("display", "none");
        if (this.uploadedBookId == null) {
            $("#error-upload-page").text("Book details missing, the session may have been lost. Please login afresh and try again.");
            $("#error-upload-page").css("display", "block");
            return;
        }//if

        if (this.selectedPageFile == null) {
            $('#error-upload-page').text("Please select a PNG or JPG file to upload.");
            $("#error-upload-page").css("display", "block");
            return;
        }

        if (!this.selectedPageFile.name.endsWith('.png') && !this.selectedPageFile.name.endsWith('.jpg')
            && !this.selectedPageFile.name.endsWith('.jpeg')) {
            $('#error-upload-page').text("Only image files are allowed. Please select a PNG or JPG file to upload.");
            $("#error-upload-page").css("display", "block");
            return;
        }

        const httpUploadOptions = {
            headers: new HttpHeaders({ 'Accept': 'application/json' })
        };

        let pageGraph = {};
        let pageOOLDGraph = {};

        let pageBlankId = `_:page_for_${this.uploadedBookId}`
        let pageImageOOLDBlankId = `_:page_image_oold`;
        let pageImageOOLD = {
            _id: pageImageOOLDBlankId,
            source: `_VV:${pageBlankId}`,
            jsonClass: 'StillImage',
            namespace: '_vedavaapi',
            identifier: this.selectedPageFile.name
        };

        let pageJson = {
            _id: pageBlankId,
            jsonClass: "ScannedPage",
            purpose: "page",
            selector: {
                jsonClass: "QualitativeSelector",
            },
            source: this.uploadedBookId,
            representations: {
                jsonClass: 'DataRepresentations',
                default: 'stillImage',
                stillImage: [{
                    jsonClass: 'StillImageRepresentation',
                    data: `_OOLD:${pageImageOOLDBlankId}`
                }]
            }
        };
        pageGraph[pageBlankId] = pageJson;
        pageOOLDGraph[pageImageOOLDBlankId] = pageImageOOLD;

        const formData: any = new FormData();
        formData.append("graph", JSON.stringify(pageGraph));
        formData.append("files", this.selectedPageFile);
        formData.append("ool_data_graph", JSON.stringify(pageOOLDGraph));
        formData.append("response_projection_map", JSON.stringify({ '*': { "permissions": 0 } }));
        formData.append("should_return_resources", 'true');
        formData.append("should_return_oold_resources", 'true');



        this.spinner.show();
        return this.http.post(this.endpointService.getBaseUrl() + '/objstore/v1/graph', formData,
            httpUploadOptions).subscribe(
                response => {
                    $('#page-image-file').val('');
                    $('#info-upload-page').text("Page uploaded successfully. Continue to add more pages or click on Finish to go to the dashboard.");
                    var pageUploadResponse: any = response;
                    // get page id
                    let page = pageUploadResponse['graph'][pageBlankId];
                    let page_id = page["_id"];
                    let imageOOLDataId = page.representations.stillImage[0].data.slice(6);
                    this.resourceIds.push(page_id);
                    this.addSlide(imageOOLDataId);

                    this.spinner.hide();
                    //Uploads Page thumbnail
                    this.selectedPageFile = null;
                },
                error => {
                    this.spinner.hide();
                    $("#error-upload-page").css("display", "block");
                    let errorCode = this.getResponseErrorCode(error);
                    if (errorCode == 403) {
                        $("#error-upload-page").text("You are forbidden to perform this operation. You may not have the necessary permissionss. Please contact the  platform administrator.");
                    } else if (errorCode == 401) {
                        $("#error-upload-page").text("Unauthorized call, session may have expired. Please login afresh and retry.");
                    } else if (errorCode == 400) {
                        $("#error-upload-page").text("Bad Request. Please contact the platform administartor.");
                    } else {
                        $("#error-upload-page").text("Unable to upload the page. Please retry or contact the platform administrator.");
                    }
                }
            );
    }




    /**
     * Fetches the thumbnail from server and displays the same in the carousel.
    */
    getThumbnailId(resource_id: any) {

        if (resource_id == null) {
            return;
        }//if
        let httpThumbnailIdparams = new HttpParams()
            .append('projection', '{"_id": 1, "cover.data": 1}');
        let httpThumbnailHeaders: HttpHeaders = new HttpHeaders({
            'Content-Type': 'application/json',
        });
        this.http.get(this.endpointService.getBaseUrl() + '/objstore/v1/resources/' + resource_id, { headers: httpThumbnailHeaders, params: httpThumbnailIdparams, })
            .subscribe(
                response => {
                    this.spinner.hide();
                    try {
                        if (response != null && response['cover'] && response['cover']['data']) {
                            this.addSlide(response["cover"]["data"]);
                        }
                    } catch (error) {

                    }
                },
                error => {
                    this.spinner.hide();
                }
            );


    }
    /**
     * show modal dialog.
     * @param idx 
     */
    showModalDialog(index) {
        $("#modal_delete_button").attr('value', index);
    }
    /**
     * Delete the uploded book page from server.
     * @param index 
     */
    onDeletePage() {

        $("#modal_cancel_btn").click();
        var indexStr = $("#modal_delete_button").val();
        var index = parseInt(indexStr, 10);
        if (this.resourceIds.length <= index || index < 0) {
            return;
        }
        const deletePageFormData = new FormData();
        deletePageFormData.append('resource_ids', '["' + this.resourceIds[index] + '"]');
        const httpOptions = {
            headers: new HttpHeaders({ 'Accept': 'application/json' }), body: deletePageFormData,
        };
        this.http.delete(this.endpointService.getBaseUrl() + '/objstore/v1/resources', httpOptions)
            .subscribe(
                response => {
                    $("#delete_page_error").hide();
                    $("#" + index).remove();
                },
                error => {
                    $("#error_message_title").show();
                    let errorCode = this.getResponseErrorCode(error);
                    let error_message = "Error";
                    if (errorCode == 403) {
                        error_message = "Perhaps login was not proper. Please logout and login afresh before next attempt.";
                    } else if (errorCode == 401) {
                        error_message = "Unauthorized call, session may have expired. Please login afresh and retry.";
                    } else if (errorCode == 400) {
                        error_message = "Bad Request. Please contact the platform administartor.";
                    } else {
                        error_message = "Unable to delete the page. Please retry or contact the platform administrator.";
                    }
                    $("#delete_page_error").show();
                    $("#delete_page_error").text(error_message);

                }
            );
    }

    /**
     *  Method is called when user chooses to upload many pages of book at once.
     * 
     */
    onMultiPageUpload() {
        $("#error-upload-multi-pages").css("display", "none");
        if (this.uploadedBookId == null) {
            $("#error-upload-multi-pages").text("Book details missing, the session may have been lost. Please login afresh and try again.");
            $("#error-upload-multi-pages").css("display", "block");
            return;
        }//if

        if (this.pageFilesArraySelected == null || this.pageFilesArraySelected.length == 0) {
            $('#error-upload-multi-pages').text("Please select at least one PNG or JPG file to upload.");
            $("#error-upload-multi-pages").css("display", "block");
            return;
        }

        if (this.haveNonImageFilesBeenSelected(this.pageFilesArraySelected)) {
            $('#error-upload-multi-pages').text("It seems like you have selected a file which is not an image. Please select only PNG or JPG files to upload.");
            $("#error-upload-multi-pages").css("display", "block");
            return;
        }

        let pagesGraph = {};
        let ooldGraph = {};
        let pageNo = 1;

        for (let pageFile of this.pageFilesArraySelected) {
            let pageBlankId = `_:page_${pageNo}`;
            let pageImageOOLDBlankId = `_:page_image_oold_${pageNo}`;

            let pageImageOOLD = {
                _id: pageImageOOLDBlankId,
                source: `_VV:${pageBlankId}`,
                jsonClass: "StillImage",
                namespace: "_vedavaapi",
                identifier: pageFile.name
            };

            let pageJson = {
                _id: pageBlankId,
                jsonClass: "ScannedPage",
                purpose: "page",
                selector: {
                    jsonClass: "QualitativeSelector",
                },
                source: this.uploadedBookId,
                representations: {
                    jsonClass: 'DataRepresentations',
                    default: 'stillImage',
                    stillImage: [{
                        jsonClass: 'StillImageRepresentation',
                        data: `_OOLD:${pageImageOOLDBlankId}`
                    }]
                }
            };

            pagesGraph[pageBlankId] = pageJson;
            ooldGraph[pageImageOOLDBlankId] = pageImageOOLD;
            pageNo++;
        }

        const httpUploadOptions = {
            headers: new HttpHeaders({ 'Accept': 'application/json' })
        };


        $("#error-upload-multi-pages").css("display", "none");
        $('#multi-pages-upload-info').text("Uploading pages...");
        //show the spinner
        this.spinner.show();

        const formData: any = new FormData();
        formData.append("graph", JSON.stringify(pagesGraph));
        formData.append('ool_data_graph', JSON.stringify(ooldGraph));
        for (let file of this.pageFilesArraySelected) {
            formData.append("files", file);
        }

        formData.append("response_projection_map", JSON.stringify({ "*": { "permissions": 0 } }));
        formData.append('should_return_resources', 'true');

        this.http.post(this.endpointService.getBaseUrl() + '/objstore/v1/graph', formData,
            httpUploadOptions).subscribe(
                response => {
                    this.spinner.hide();
                    $('#multiple-pages-images-files').val('');
                    $('#multi-pages-upload-info').text("All Pages uploaded successfully. Continue to add more pages or click on Finish to go to the dashboard.");
                    this.pageFilesArraySelected = null;
                },
                error => {
                    this.spinner.hide();
                    $('#multiple-pages-images-files').val('');
                    $('#multi-pages-upload-info').css("display", "none");
                    $("#error-upload-multi-pages").css("display", "block");

                    let errorCode = this.getResponseErrorCode(error);
                    if (errorCode == 403) {
                        $("#error-upload-multi-pages").text("You are forbidden to perform this operation. You may not have the necessary permissionss. Please contact the  platform administrator.");
                    } else if (errorCode == 401) {
                        $("#error-upload-multi-pages").text("No Pages uploaded. Unauthorized call, session may have expired, please login afresh and retry.");
                    } else if (errorCode == 400) {
                        $("#error-upload-multi-pages").text("No Pages uploaded. Bad Request, please contact the platform administrator.");
                    } else {
                        $("#error-upload-multi-pages").text("Unable to upload any of the selected pages. Network may be down, please retry or contact the platform administrator if there there no issues of network availability.");
                    }

                    this.pageFilesArraySelected = null;
                } //error
            );
    }


    /**
     * Creates a Book Entry on the Vedavaapi Platform.
     * @param $book_title 
     * @param $book_author_information 
     * @param $book_type 
     * @param $book_description 
     */
    createBookEntry($book_title, $book_author_information,
        $book_type, $book_description) {

        this.hideErrorDivs();

        if (!this.validateFieldValues($book_title, $book_author_information, $book_description)) {
            return;
        }

        this.bookTitle = $book_title;

        this.spinner.show();

        let authors = $book_author_information.split(',');

        let metadataJson = [
            {
                jsonClass: "MetadataItem",
                label: "description",
                value: $book_description
            }];

        let book_type = "book";
        if ($book_type != null && $book_type.trim().length > 0) {
            book_type = $book_type;
        }

        let requestJson = {
            author: authors, jsonClassLabel: book_type,
            jsonClass: "ScannedBook", metadata: metadataJson,
            title: $book_title,
            source: this.currentLibraryId
        };

        const httpUploadOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
        };

        let request_params = "resource_jsons=" + encodeURIComponent(JSON.stringify([requestJson]));
        return this.http.post(this.endpointService.getBaseUrl() + '/objstore/v1/resources', request_params,
            httpUploadOptions).subscribe(
                response => {
                    this.spinner.hide();

                    $("#upload_book_container").css("display", "none");
                    $("#book_title").attr("readonly", "true");
                    $("#book_type").attr("readonly", "true");
                    $("#book_author").attr("readonly", "true");
                    $("#book_description").attr("readonly", "true");
                    $("#add_book_thumbnail_container").css("display", "block");

                    const responseStr = JSON.stringify(response);
                    JSON.parse(responseStr, (key, value) => {
                        if (key === '_id') {
                            this.uploadedBookId = value;
                        }
                        return value;
                    });

                    if (this.uploadedBookId == null) {
                        $("#error-add-book").text("Server did not send the book Id. Cannot continue with the upload process. Plese retry.");
                        $("#error-add-book").css("display", "block");
                        return;
                    }

                    $("#info-book-created").text("The book '" + $book_title + "' has been created. Please upload the thumbnail.");
                    this.bookTitle = $book_title;
                },
                error => {
                    this.spinner.hide();
                    let errorCode = this.getResponseErrorCode(error);
                    if (errorCode == 403) {
                        $("#error-add-book").text("You are forbidden to perform this operatiton. You may not have the necessary permissionss. Please contact the  platform administrator.");
                    } else if (errorCode == 401) {
                        $("#error-add-book").text("Unauthorized call, session may have expired. Please login afresh and retry.");
                    } else if (errorCode == 400) {
                        $("#error-add-book").text("Bad Request. Please contact the platform administartor.");
                    } else {
                        $("#error-add-book").text("Unknown error, please contact the platform administrator or retry after some time.");
                    }

                    $("#error-add-book").css("display", "block");

                }
            );
    }

    /**
     * Displays the container for uploading books in bulk.
     */
    private showBulkUploadBooks() {
        $('#add_page_container').css('display', 'none');
        $('#upload-bulk-image').css('display', 'block');
    }

    /**
     * Hides the Error Div elements
     */
    private hideErrorDivs() {
        $('#book_title_minlength_error').css('display', 'none');
        $('#book_author_minlength_error').css('display', 'none');
        $('#book_description_minlength_error').css('display', 'none');
    }

    /**
     * 
     * @param error Returns the Error Code.
     */
    private getResponseErrorCode(error) {
        const errorStr = JSON.stringify(error);
        let errorCode = -1;
        JSON.parse(errorStr, (key, value) => {
            if (key === 'code') {
                errorCode = value;
            }
            return value;
        });
        return errorCode;
    }


    /**
     * Image file for Page selected callback.
     * @param event
     */
    onPageImageSelected(event) {
        $("#error-upload-page").css("display", "none");

        let fileSelected = event.target.files[0];
        if (fileSelected == null) {
            return;
        }
        if (!fileSelected.name.endsWith('.png') && !fileSelected.name.endsWith('.jpg')
            && !fileSelected.name.endsWith('.jpeg')) {
            $('#error-upload-page').text("Only image files are allowed. Please select a PNG or JPG file to upload.");
            $("#error-upload-page").css("display", "block");
            return;
        }

        this.selectedPageFile = event.target.files[0];
    }

    /**
     * Multiple files selected callback.
     * @param event 
     */
    onMultiPageImageSelected(fileInput: any) {
        $("#error-upload-multi-pages").css("display", "none");
        let filesSelected = fileInput.target.files;
        if (this.haveNonImageFilesBeenSelected(filesSelected)) {
            $('#error-upload-multi-pages').text("It seems like you have selected a file which is not an image. Please select only PNG or JPG files to upload.");
            $("#error-upload-multi-pages").css("display", "block");
            this.pageFilesArraySelected = null;
            $('#multiple-pages-images-files').val('');
            return;
        }

        this.pageFilesArraySelected = <Array<File>>fileInput.target.files;
    }

    /**
     * Returns true if non image files have been selected.
     * @param selectedFiles 
     */
    private haveNonImageFilesBeenSelected(selectedFiles: Array<File>): boolean {
        if (selectedFiles != null && selectedFiles.length > 0) {
            for (let i = 0; i < selectedFiles.length; i++) {
                if (selectedFiles[i] == null || selectedFiles[i].name == null) {
                    continue;
                }
                if (!selectedFiles[i].name.endsWith('.png') && !selectedFiles[i].name.endsWith('.jpg')
                    && !selectedFiles[i].name.endsWith('.jpeg')) {
                    return true;
                }
            }//for
        }
        return false;
    }


    /**
     * On Thumbnail File changed callback.
     * @param event 
     */
    onThumbnailFileChanged(event) {

        let fileSelected = event.target.files[0];
        if (fileSelected == null) {
            return;
        }

        if (!fileSelected.name.endsWith('.png') && !fileSelected.name.endsWith('.jpg')
            && !fileSelected.name.endsWith('.jpeg')) {
            $('#error-upload-thumbnail').text("Please choose a proper image file. Only PNG and JPEG files are supported.");
            $('#error-upload-thumbnail').css("display", "block");
            return;
        }

        let fileSize = fileSelected.size;
        if (fileSize > (1024 * 1024 * 5)) {
            $('#error-upload-thumbnail').text("The Size of the image file is greater than 5 MB. Please retry with an image of lesser size.");
            $('#error-upload-thumbnail').css("display", "block");
            return;
        }


        this.selectedThumbnailFile = event.target.files[0];
        const reader = new FileReader();
        reader.onload = () => {
            this.imagePreview = reader.result;
            $("#img_book_thumbnail").attr('src', this.imagePreview);
            var img = new Image();
            img.onload = () => {
                if ((img.width / img.height) == (2 / 3)) {
                    this.isThumbnailAsPerAspectRatio = true;
                }
            };

            img.src = reader.result as string;
        };
        reader.readAsDataURL(event.target.files[0]);
    }


    /**
     * Method uploads Book thumbnail.
     */
    onThumbnailUpload() {
        if (this.uploadedBookId == null || this.uploadedBookId.trim().length == 0) {
            $('#error-upload-thumbnail').text("Book Entry not created. Perhaps the session expired. please retry after some time.");
            $('#error-upload-thumbnail').css("display", "block");
            return;
        }

        if (this.selectedThumbnailFile == null) {
            $('#error-upload-thumbnail').text("Please select a PNG or JPG file to upload.");
            $('#error-upload-thumbnail').css("display", "block");
            return;
        }

        if (this.selectedThumbnailFile.name == null) {
            $('#error-upload-thumbnail').text("The file seems to be corrupt. Please select a different file.");
            $('#error-upload-thumbnail').css("display", "block");
            return;
        }

        if (this.selectedThumbnailFile.size > (1024 * 1024 * 5)) {
            $('#error-upload-thumbnail').text("The Size of the image file is greater than 5 MB. Please retry with an image of lesser size.");
            $('#error-upload-thumbnail').css("display", "block");
            return;
        }

        if (!this.isThumbnailAsPerAspectRatio) {
            $('#error-upload-thumbnail').text("The thumbnail image should be in the aspect ratio 2:3(width:height). Please retry with an image that matches the aspect ratio.");
            $('#error-upload-thumbnail').css("display", "block");
            return;
        }


        //Content-Type not required;
        // https://stackoverflow.com/questions/46787612/file-upload-angular2-via-multipart-form-data-400-error

        this.spinner.show();
        const httpUploadOptions = {
            headers: new HttpHeaders(
                { 'Accept': 'application/json' })
        };

        let coverPageOOLDBlankId = `_:cover_page_oold`;
        let coverPageOOLD = {
            jsonClass: "StillImage",
            source: this.uploadedBookId,
            namespace: "_vedavaapi",
            identifier: this.selectedThumbnailFile.name
        };

        let bookJson = {
            _id: this.uploadedBookId,
            jsonClass: 'ScannedBook',
            source: this.currentLibraryId,
            cover: {
                jsonClass: 'StillImageRepresentation',
                data: `_OOLD:${coverPageOOLDBlankId}`
            }
        };

        let graph = {
            [this.uploadedBookId]: bookJson
        };
        let ooldGraph = {
            [coverPageOOLDBlankId]: coverPageOOLD
        };

        const uploadData = new FormData();
        uploadData.append('files', this.selectedThumbnailFile);
        uploadData.append("graph", JSON.stringify(graph));
        uploadData.append('ool_data_graph', JSON.stringify(ooldGraph));
        uploadData.append("projection", JSON.stringify({ "_id": 1, "cover": 1 }));
        uploadData.append("should_return_resource", 'true');

        this.http.post(this.endpointService.getBaseUrl() + "/objstore/v1/graph", uploadData, httpUploadOptions)
            .subscribe(
                response => {
                    this.spinner.hide();
                    $("#add_book_thumbnail_container").css('display', 'none');
                    $("#add_page_container").css('display', 'block');
                    if (this.bookTitle != null) {
                        $("#book-title-carousel").text("Books > " + this.bookTitle);
                    }
                },
                error => {
                    this.spinner.hide();

                    if (error != null) {
                        let errorCode = this.getResponseErrorCode(error);
                        if (errorCode == 403) {
                            $("#error-upload-thumbnail").text("You are forbidden to perform this operation. You may not have the necessary permissionss. Please contact the  platform administrator.");
                        } else if (errorCode == 401) {
                            $("#error-upload-thumbnail").text("Unauthorized call, session may have expired. Please login afresh and retry.");
                        } else if (errorCode == 400) {
                            $("#error-upload-thumbnail").text("Bad Request. Please contact the platform administartor.");
                        } else {
                            $("#error-upload-thumbnail").text("Unknown error, please contact the platform administrator or retry after some time.");
                        }
                    } else {
                        $("#error-upload-thumbnail").text("Unknown error, please contact the platform administrator or retry after some time.");
                    }
                }
            )
    }
}// end of class declaration
