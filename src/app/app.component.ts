import { Component } from '@angular/core';
import { setTheme } from 'ngx-bootstrap/utils';
import { OauthHelperService } from 'src/app/services/oauth-helper.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';

  constructor(private oauthHelperService: OauthHelperService) {
    setTheme('bs4');
  }
}
