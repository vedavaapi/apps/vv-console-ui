import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './ui/dashboard/dashboard.component';
import { HomeComponent } from './ui/home/home.component';
import { AddBookComponent } from './ui/add-book/add-book.component';
import { AddTextDocComponent } from './ui/add-textdoc/add-textdoc.component';
import { IIIFImporterComponent } from './ui/iiif-importer/iiif-importer.component';
import { ApplicationInfoPageComponent } from './ui/application-info-page/application-info-page.component';
import { ViewAllBooksComponent } from './ui/view-all-books/view-all-books.component';
import { EditBookComponent } from './ui/edit-book/edit-book.component';
import { BookInformationPageComponent } from './ui/book-information-page/book-information-page.component';
import { ObjectViewerComponent } from './ui/view-object/view-object.component';
import { OauthInitializerComponent } from './ui/oauth-initializer/oauth-initializer.component';
import { OauthCallbackComponent } from './ui/oauth-callback/oauth-callback.component';
import { ApplicationPageComponent } from 'src/app/ui/application-page/application-page.component';
import { UserListComponent } from './ui/user-list/user-list.component';
import { GroupListComponent } from './ui/group-list/group-list.component';
import { CreateObjectComponent } from './ui/create-object/create-object.component';
import { BookListComponent } from './ui/book-list/book-list.component';
import { LibraryListComponent } from './ui/library-list/library-list.component';
import { ObjectsListComponent } from './ui/objects-list/objects-list.component';
import { SiteUrlComponent } from './ui/site-url/site-url.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: "oauth_initializer",
    component: OauthInitializerComponent
  },
  {
    path: "oauth_callback",
    component: OauthCallbackComponent
  },
  {
    path: 'annotator/dashboard',
    component: DashboardComponent,
    // canActivate: [AnnotatorAuthGuard]
  },
  {
    path: 'annotator/add-book',
    component: AddBookComponent,
    // canActivate: [AnnotatorAuthGuard]
  },
{
    path: 'annotator/add-textdoc',
    component: AddTextDocComponent,
    // canActivate: [AnnotatorAuthGuard]
},
  {
    path: 'annotator/iiif-importer',
    component: IIIFImporterComponent,
    // canActivate: [AnnotatorAuthGuard]
  },
  {
      path: 'applications',
      component: ApplicationPageComponent
  },
  {
    path: 'application/description',
    component: ApplicationInfoPageComponent
  },
  {
    path: 'all-books',
    component: DashboardComponent
  },
  {
    path: 'annotator/edit-books',
    component: ViewAllBooksComponent,
    // canActivate: [AnnotatorAuthGuard]
  },
  {
    path: 'dashboard/browse-books',
    component: ViewAllBooksComponent,
    // canActivate: [AnnotatorAuthGuard]
  },
  {
    path: 'annotator/edit-book',
    component: EditBookComponent,
    // canActivate: [AnnotatorAuthGuard]
  },
  {
    path: 'book/:id',
    component: BookInformationPageComponent
  },
    {
        path: 'object/:id',
        component: ObjectViewerComponent
    },
  {
    path: 'users/:team_id',
    component: UserListComponent
  },
  {
    path: 'users/:team_id/addremove',
    component: UserListComponent
  },
  {
    path: 'teams',
    component: GroupListComponent
  },
  {
      path: 'create/:schema_type',
      component: CreateObjectComponent
  },
  {
    path: 'edit/:schema_type/:_id',
    component: CreateObjectComponent
  },
  {
    path: 'users/:team_id/permissions',
    component: UserListComponent
  },
  {
    path: 'libraries',
    component: LibraryListComponent
  },
  {
    path: 'objects',
    component: ObjectsListComponent
  },
  {
    path: 'library/:_id/books',
    component: DashboardComponent
  },
  {
    path: 'teams/:display_mode/:object_id/select',
    component: GroupListComponent
  },
  {
    path: 'users/:team_id/permissions/:team_display_mode/:object_id/select',
    component: UserListComponent
  },
  {
    path: 'site-url',
    component: SiteUrlComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
