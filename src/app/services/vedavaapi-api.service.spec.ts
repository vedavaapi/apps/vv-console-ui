import { TestBed, inject } from '@angular/core/testing';

import { VedavaapiApiService } from './vedavaapi-api.service';

describe('VedavaapiApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VedavaapiApiService]
    });
  });

  it('should be created', inject([VedavaapiApiService], (service: VedavaapiApiService) => {
    expect(service).toBeTruthy();
  }));
});
