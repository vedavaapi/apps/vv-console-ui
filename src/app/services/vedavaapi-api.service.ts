import { Injectable } from '@angular/core';
import { EndpointsService } from '../endpoints.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})

/**
 * Parent Class for all Services classes providing API calls.
 */
export class VedavaapiApiService {

    public static ERROR_MISSING_ACCESS_TOKEN: string = 'Missing credentials. Please login and retry.';

    public static ERROR_MISSING_RESOURCE_ID: string = 'Resource Identifier should not be empty or null.';

    public static ERROR_MISSING_ACTION_PARAMTER_EMPTY: string = 'Actions sent as request parameters should not be empty.';

    private static ERROR_MISSING_USER_TEAM_IDS_PARAMTER_EMPTY: string = 'Either User Ids or Team ids should be part of the request.';

    private static ERROR_MISSING_ACL_CONTROL: string = 'CONTROL is missing in the request.';

    public static ERROR_MISSING_JSON: string = 'Resources JSON is missing in the request.';


    constructor(private endpointService: EndpointsService,
        private http: HttpClient) { }

    /**
     * Extracts the Error status code from the JSON Object and 
     * returns an appropriate error message based on the action whose exection had failed.
     * @param error 
     * @param action 
     * @param defaultErrorMessage 
     */
    protected getErrorMessage(error: any, action: string, defaultErrorMessage: string) {
        var statusCode = -1;
        if (error && error.hasOwnProperty("status")) {
            statusCode = error['status'];
        }//if

        var errorMessage = defaultErrorMessage;
        switch (statusCode) {

            case 401:
                errorMessage = "Not Authorized. Perhaps you may not have signed in or your session may have expired. Please login and try.";
                break;

            case 403:
                errorMessage = "You are forbidden to perform this operation. You may not have the necessary permissions.";
                break;

            case 404:
                if (action) {
                    if (action === 'grant_revoke_permissions') {
                        errorMessage = "Unable to grant or revoke permissions. The Resource does not exist in the database.";
                    } else if (action === 'create_resource') {
                        errorMessage = "Unable to create the resource. Some required information missing in database";
                    } else {
                        errorMessage = "Unable to grant or revoke permissions. The team does not exist in the database.";
                    }
                } else {
                    errorMessage = "404. Not found error.";
                }
                break;

            default:
                break;
        }
        return errorMessage
    }


    /**
     * Makes a POST request to partUrl(/acls/v1//{resource_id}) to grant or revoke Permissions.
     * @param accessToken 
     * @param resourceId 
     * @param actions 
     * @param control 
     * @param userIds 
     * @param groupIds 
     * @param successCallback 
     * @param errorCallback 
     */
    private permissionsApiCall(
        accessToken: string, resourceId: string,
        actions: string[], control: string,
        userIds: string[], teamIds: string[],
        successCallback: (response: any) => void,
        errorCallback: (error: string) => void) {

        if (accessToken == null || accessToken.trim().length == 0) {
            if (errorCallback) {
                errorCallback(VedavaapiApiService.ERROR_MISSING_ACCESS_TOKEN);
            }
            return;
        }

        if (resourceId == null || resourceId.trim().length == 0) {
            if (errorCallback) {
                errorCallback(VedavaapiApiService.ERROR_MISSING_RESOURCE_ID);
            }
            return;
        }//if

        if (!control || control.trim().length == 0) {
            if (errorCallback) {
                errorCallback(VedavaapiApiService.ERROR_MISSING_ACL_CONTROL);
            }
            return;
        }

        if (actions == null || actions.length == 0) {
            if (errorCallback) {
                errorCallback(VedavaapiApiService.ERROR_MISSING_ACTION_PARAMTER_EMPTY);
            }
            return;
        }//if

        if ((userIds == null || userIds.length == 0) && (teamIds == null || teamIds.length == 0)) {
            if (errorCallback) {
                errorCallback(VedavaapiApiService.ERROR_MISSING_USER_TEAM_IDS_PARAMTER_EMPTY);
            }
            return;
        }//if

        let request_params = "";
        if (actions) {
            request_params += "actions=" + encodeURIComponent(JSON.stringify(actions));
        }
        if (userIds) {
            request_params += "&user_ids=" + encodeURIComponent(JSON.stringify(userIds));
        }
        if (teamIds) {
            request_params += "&team_ids=" + encodeURIComponent(JSON.stringify(teamIds));
        }

        var endpointUrl: string = this.endpointService.getBaseUrl() + '/acls/v1/' + resourceId;
        if (control === 'revoke') {

            if (control) {
                request_params += "&control=grant";
            }

            const httpUploadOptions = {
                headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded', 'Authorization': 'Bearer ' + accessToken }),
                body: request_params
            };

            this.http.delete(endpointUrl,
                httpUploadOptions).subscribe(
                    response => {
                        console.log("Permissions Call response: " + JSON.stringify(response));
                        if (successCallback) {
                            successCallback(response);
                        }
                    },
                    error => {
                        if (errorCallback) {
                            errorCallback(this.getErrorMessage(error, "grant_revoke_permissions", "The resource does exist in the database."));
                        }
                    }
                );
        } else {
            if (control) {
                request_params += "&control=" + control;
            }

            const httpUploadOptions = {
                headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded', 'Authorization': 'Bearer ' + accessToken })
            };

            this.http.post(endpointUrl, request_params,
                httpUploadOptions).subscribe(
                    response => {
                        console.log("Permissions Call response: " + JSON.stringify(response));
                        if (successCallback) {
                            successCallback(response);
                        }
                    },
                    error => {
                        if (errorCallback) {
                            errorCallback(this.getErrorMessage(error, "grant_revoke_permissions", "The resource does exist in the database."));
                        }
                    }
                );
        }

    }

    /**
     * Method grants permission over a team to a set of users or teams.
     * 
     * @param accessToken 
     * @param resourceId 
     * @param actions 
     * @param userIds 
     * @param groupIds 
     * @param successCallback 
     * @param errorCallback 
     */
    public grantPermissionsOnResource(accessToken: string, resourceId: string, actions: string[],
        userIds: string[], groupIds: string[],
        successCallback: (response: any) => void,
        errorCallback: (error: string) => void) {

        this.permissionsApiCall(
            accessToken, resourceId, actions,
            'grant', userIds, groupIds, successCallback, errorCallback);
    }

    /**
     * Method Withdaws permissions give to a set of users or teams over a single team.
     * 
     * @param accessToken 
     * @param resourceId 
     * @param actions 
     * @param userIds 
     * @param groupIds 
     * @param successCallback 
     * @param errorCallback 
     */
    public withdrawPermissionsOnResource(accessToken: string, resourceId: string, actions: string[],
        userIds: string[], groupIds: string[],
        successCallback: (response: any) => void,
        errorCallback: (error: string) => void) {

        this.permissionsApiCall(
            accessToken, resourceId, actions,
            'revoke', userIds, groupIds, successCallback, errorCallback);
    }

    /**
     * Fetches list of Libraries from the vedavaapi server.
     * @param access_token 
     * @param search_string
     * @param start 
     * @param count 
     * @param successCallback 
     * @param errorCallback 
     */
    public getLibraryList(access_token: string, search_string: string,
        start: number, count: number,
        successCallback: (response: any) => void,
        errorCallback: (error: string) => void) {

        var selector_doc = '{"jsonClass": "Library"}';
        if (search_string && search_string.trim().length > 0) {
            selector_doc = '{"jsonClass": "Library",  "name":  {"$regex": "^' + search_string + '.*$" }}';
        }

        this.getResources(access_token, selector_doc, null, '[["created", -1]]',
            start, count, successCallback, errorCallback);
    }
    /**
     * Fetches list of Objects from the vedavaapi server.
     * @param access_token 
     * @param search_string 
     * @param start 
     * @param count 
     * @param successCallback 
     * @param errorCallback 
     */
    public getObjectsList(access_token: string, search_string: string, start: number, count: number,
        successCallback: (response: any) => void,
        errorCallback: (error: string) => void) {

        var selector_doc = '{"jsonClass": {"$in": ["Library","ScannedBook","Team","User"]}}';
        if (search_string && search_string.trim().length > 0) {
            selector_doc = '{"$or": [{"jsonClass": "Library",  "name":  {"$regex": "^' + search_string + '.*$" }}, {"jsonClass":"ScannedBook",  "title":  {"$regex": "^' + search_string + '.*$" }}, {"jsonClass":"Team",  "name":  {"$regex": "^' + search_string + '.*$" }},  {"jsonClass":"User",  "name":  {"$regex": "^' + search_string + '.*$" }}]}';
        }

        this.getResources(access_token, selector_doc, null, '[["created", -1]]',
            start, count, successCallback, errorCallback);
    }
    /**
     * Fetches the List of Resources.
     * @param access_token 
     * @param selector_doc ex: {"jsonClass": "Library"}
     * @param projection ex: {"permissions": "0"}
     * @param sort_doc ex: [["created", -1]] - returns list in descending order
     * @param start The start index
     * @param count The count of resources to be returned in the response.
     * @param successCallback 
     * @param errorCallback 
     */
    public getResources(access_token: string, selector_doc: string, projection: string, sort_doc: string,
        start: number, count: number,
        successCallback: (response: any) => void,
        errorCallback: (error: string) => void) {

        let httpParams = new HttpParams();
        if (selector_doc) {
            httpParams = httpParams.append('selector_doc', selector_doc);
        }
        if (projection) {
            httpParams = httpParams.append('projection', projection);
        }
        if (sort_doc) {
            httpParams = httpParams.append('sort_doc', sort_doc);
        }

        httpParams = httpParams.append('start', start.toString())
            .append('count', count.toString());


        let httpHeaders: HttpHeaders = null;
        if (access_token) {
            httpHeaders = new HttpHeaders({
                'Content-Type': 'application/json', 'Authorization': 'Bearer ' + access_token
            });
        } else {
            httpHeaders = new HttpHeaders({
                'Content-Type': 'application/json'
            });
        }

        console.log('querying: ', this.endpointService.getBaseUrl() + '/objstore/v1/resources');

        this.http.get(
            this.endpointService.getBaseUrl() + '/objstore/v1/resources',
            { headers: httpHeaders, params: httpParams, })
            .subscribe(
                response => {
                    if (successCallback) {
                        successCallback(response);
                    }
                },
                error => {
                    if (errorCallback) {
                        errorCallback(this.getErrorMessage(error, 'get_resources', 'Unable to fetch the requested resources.'));
                    }
                }
            );
    }//end of method

    /**
     * Creates a Resource. A Resource is usually a Library or a Book or 
     * any other object deeemed as resource by the Vedavaapi platform.
     * @param accessToken 
     * @param resources_json 
     * @param successCallback 
     * @param errorCallback 
     */
    public createResource(accessToken: string, resources_json: any,
        successCallback: (response: any) => void,
        errorCallback: (error: string) => void) {

        if (accessToken == null || accessToken.trim().length == 0) {
            if (errorCallback) {
                errorCallback(VedavaapiApiService.ERROR_MISSING_ACCESS_TOKEN);
            }
            return;
        }

        if (resources_json == null || resources_json.length == 0) {
            if (errorCallback) {
                errorCallback(VedavaapiApiService.ERROR_MISSING_JSON);
            }
            return;
        }//if


        const httpUploadOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
        }

        let request_params = "resource_jsons=" + encodeURIComponent(JSON.stringify([resources_json]));
        this.http.post(this.endpointService.getBaseUrl() + '/objstore/v1/resources', request_params,
            httpUploadOptions).subscribe(
                response => {
                    if (successCallback) {
                        successCallback(response);
                    }
                },
                error => {
                    if (errorCallback) {
                        errorCallback(this.getErrorMessage(error, 'create_resource', 'Unable to create the resource.'));
                    }
                }
            );
    }//end of Method

}//end of class Declaration
