import { Injectable, OnInit } from "@angular/core";
import { LocalStorageService } from "./local-storage.service";
import { e } from "@angular/core/src/render3";
import { OauthHelperService } from './oauth-helper.service';

@Injectable({
    providedIn: "root"
})
export class VedavaapiShellHelperService {

    constructor(private oauthHelperService: OauthHelperService) {

    }

    postAuthorizationChangeMessageToIframe(vvSiteUrl: string, iframeWindowRef: Window) {
        let authorizedCreds = this.oauthHelperService.getAuthorizedCreds(vvSiteUrl);
        let message = {
            "type": "authorization_change",
            authorized: Boolean(authorizedCreds),
            "vv_site_url": vvSiteUrl
        };
        if (authorizedCreds) {
            let authorizedDetails = {
                "access_token": authorizedCreds['access_token']
            };
            message['authorization_details'] = authorizedDetails;
        }
        iframeWindowRef.postMessage(message, '*');
    }

    channelAuthorizationChangesToIframe(iframeWindowRef: Window) {
        let bc = new BroadcastChannel(this.oauthHelperService.authorizationChangeBroadcastChannelName);
        bc.onmessage = ({ data }) => {
            this.postAuthorizationChangeMessageToIframe(data['vv_site_url'], iframeWindowRef);
        };
    }

    postApplicationsChangeMessageToIframe(vvSiteUrl: string, iframeWindowRef: Window) {
        let applications = this.oauthHelperService.getApplications(vvSiteUrl);
        let message = {
            "type": "applications_change",
            "applications": applications,
            "vv_site_url": vvSiteUrl
        };
        iframeWindowRef.postMessage(message, '*');
    }

    channelApplicationsChangeMessageToIframe(iframeWindowRef: Window) {
        let bc = new BroadcastChannel(this.oauthHelperService.applicationsUpdateBroadcastChannelName);
        bc.onmessage = ({ data }) => {
            this.postAuthorizationChangeMessageToIframe(data['vv_site_url'], iframeWindowRef);
        }
    }

    listenAuthorizationRequestsFromIframe(iframeWindowRef: Window) {
        window.addEventListener('message', ({data}) => {
            if (data.type != 'authorization_request') {
                return;
            }
            if (data.status == 'syncing') {
                this.postAuthorizationChangeMessageToIframe(data['vv_site_url'], iframeWindowRef);
                return;
            }
            let authorizedCreds = this.oauthHelperService.getAuthorizedCreds(data['vv_site_url']);
            if (authorizedCreds && data.status == 'unavailable') {
                this.postAuthorizationChangeMessageToIframe(data['vv_site_url'], iframeWindowRef);
            }
            else {
                this.oauthHelperService.initializeAuthorizationFlowInNewWindow('oauth_initializer', true, null, 'iframeBroadcast');
            }
        }, false)
    }

    listenApplicationsDetailsRequestFromIframe(iframeWindowRef: Window) {
        window.addEventListener('message', ({ data }) => {
            if (data.type != 'applications_request') {
                return;
            }
            this.postApplicationsChangeMessageToIframe(data['vv_site_url'], iframeWindowRef);
        }, false)
    }

    setupCommunication(iframeWindowRef: Window) {
        this.channelAuthorizationChangesToIframe(iframeWindowRef);
        this.channelApplicationsChangeMessageToIframe(iframeWindowRef);
        this.listenAuthorizationRequestsFromIframe(iframeWindowRef);
        this.listenApplicationsDetailsRequestFromIframe(iframeWindowRef);
    }

}
