import { TestBed, inject } from '@angular/core/testing';

import { OauthHelperService } from './oauth-helper.service';

describe('OauthHelperService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [OauthHelperService]
        });
    });

    it('should be created', inject([OauthHelperService], (service: OauthHelperService) => {
        expect(service).toBeTruthy();
    }));
});
