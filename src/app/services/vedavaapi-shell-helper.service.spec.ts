import { TestBed, inject } from '@angular/core/testing';

import { VedavaapiShellHelperService } from './vedavaapi-shell-helper.service';

describe('OauthHelperService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [VedavaapiShellHelperService]
        });
    });

    it('should be created', inject([VedavaapiShellHelperService], (service: VedavaapiShellHelperService) => {
        expect(service).toBeTruthy();
    }));
});
