import { Injectable } from '@angular/core';
import { HttpParams, HttpHeaders, HttpClient } from '@angular/common/http';
import { EndpointsService } from '../endpoints.service';
import { UserApiService } from './user-api.service';

@Injectable({
    providedIn: 'root'
})

/**
 * Provides API related to Groups.
 */
export class GroupsApiService {

    constructor(private endpointService: EndpointsService, private http: HttpClient,
        private _userApiService: UserApiService) {

    }

    /**
     * Creates a Group
     * @param access_token 
     * @param team_json 
     * @param successCallback 
     * @param errorCallback 
     */
    public createGroup(access_token: string, team_json: any,
        successCallback: (response: any) => void,
        errorCallback: (error: string) => void) {

        if (access_token == null || access_token.trim().length == 0) {
            if (errorCallback) {
                errorCallback('Missing credentials. Your login session may have expired. Please login and retry.');
            }
            return;
        }


        const httpUploadOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded', 'Authorization': 'Bearer ' + access_token })
        };
        
        let request_params = "team_json=" + encodeURIComponent(JSON.stringify(team_json));
        var endpointUrl: string = this.endpointService.getBaseUrl() + '/accounts/v1/teams';

        this.http.post(endpointUrl, request_params,
            httpUploadOptions).subscribe(
                response => {
                    if (successCallback != null) {
                        successCallback(response['items']);
                    }
                },
                error => {
                    console.log("Error Creating a group: " + JSON.stringify(error));

                    if (errorCallback != null) {
                        var statusCode = -1;
                        if (error && error.hasOwnProperty("status")) {
                            statusCode = error['status'];
                        }//if

                        var errorMessage = 'Unable to create the team';
                        switch (statusCode) {

                            case 401:
                                errorMessage = "Not Authorized. Perhaps you may not have signed in or your session may have expired. Please login and try.";
                                break;

                            case 403:
                                errorMessage = "You are forbidden to perform this operation. You may not have the necessary permissions.";
                                break;

                            default:
                                break;
                        }
                        errorCallback(errorMessage);
                    }
                }
            )
    }//end of method

    /**
     * Adds Members to a Group.
     * @param access_token 
     * @param team_identifier 
     * @param memberIds 
     * @param successCallback 
     * @param errorCallback 
     */
    public addMembersToGroup(access_token: string, team_identifier: string,
        memberIds: any,
        successCallback: (response: any) => void,
        errorCallback: (error: string) => void) {

        if (access_token == null || access_token.trim().length == 0) {
            if (errorCallback) {
                errorCallback('Missing credentials. Please login and retry.');
            }
            return;
        }

        if (team_identifier == null || team_identifier.trim().length == 0) {
            if (errorCallback) {
                errorCallback('Team Identifier should not be empty or null.');
            }
            return;
        }//if

        if (memberIds == null || memberIds.length == 0) {
            if (errorCallback) {
                errorCallback('Member Id array should not be empty.');
            }
            return;
        }//if

        const httpUploadOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded', 'Authorization': 'Bearer ' + access_token })
        };

        let request_params = "member_identifiers=" + encodeURIComponent(JSON.stringify(memberIds));
        var endpointUrl: string = this.endpointService.getBaseUrl() + '/accounts/v1/teams/' + team_identifier + "/members";
        this.http.post(endpointUrl, request_params,
            httpUploadOptions).subscribe(
                response => {
                    if (successCallback != null) {
                        successCallback(response);
                    }
                },
                error => {
                    console.log("Error Adding Member to Team: " + JSON.stringify(error));

                    if (errorCallback != null) {
                        var statusCode = -1;
                        if (error && error.hasOwnProperty("status")) {
                            statusCode = error['status'];
                        }//if

                        var errorMessage = 'Unable to add member to the Team';
                        switch (statusCode) {

                            case 401:
                                errorMessage = "Not Authorized. Perhaps you may not have signed in or your session may have expired. Please login and try.";
                                break;

                            case 403:
                                errorMessage = "You are forbidden to perform this operation. You may not have the necessary permissions.";
                                break;

                            case 404:
                                errorMessage = "The Team does not exist. Please add the member to a different group.";
                            default:
                                break;
                        }
                        errorCallback(errorMessage);
                    }
                }
            )
    }//end of method

    /**
     * Extracts the Error status code from the JSON Object and 
     * returns an appropriate error message based on the action whose exection had failed.
     * @param error 
     * @param action 
     * @param defaultErrorMessage 
     */
    private getErrorMessage(error: any, action: string, defaultErrorMessage: string) {
        var statusCode = -1;
        if (error && error.hasOwnProperty("status")) {
            statusCode = error['status'];
        }//if

        var errorMessage = defaultErrorMessage;
        switch (statusCode) {

            case 401:
                errorMessage = "Not Authorized. Perhaps you may not have signed in or your session may have expired. Please login and try.";
                break;

            case 403:
                errorMessage = "You are forbidden to perform this operation. You may not have the necessary permissions.";
                break;

            case 404:
                errorMessage = "The team does not exist in the database.";
                break;

            default:
                break;
        }
        return errorMessage
    }

    /**
     * Deletes the team.
     * 
     * @param access_token 
     * @param team_identifier 
     * @param successCallback 
     * @param errorCallback 
     */
    public deleteTeam(access_token: string,
        team_identifier: string,
        successCallback: (response: any) => void,
        errorCallback: (error: string) => void) {
        if (access_token == null || access_token.trim().length == 0) {
            if (errorCallback) {
                errorCallback('Missing credentials. Please login and retry.');
            }
            return;
        }

        if (team_identifier == null || team_identifier.trim().length == 0) {
            if (errorCallback) {
                errorCallback('Team Identifier should not be empty or null.');
            }
            return;
        }//if

        let request_params = 'team_ids=["' + team_identifier + '"]';
        const httpUploadOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded', 'Authorization': 'Bearer ' + access_token }),
            body: request_params
        };

        var endpointUrl: string = this.endpointService.getBaseUrl() + '/accounts/v1/teams';
        this.http.delete(endpointUrl,
            httpUploadOptions).subscribe(
                response => {
                    if (successCallback != null) {
                        successCallback(response['items']);
                    }
                },
                error => {
                    if (errorCallback) {
                        errorCallback(this.getErrorMessage(error, 'delete_team', "Unable to delete the team"));
                    }
                }
            );
    }

    /**
     * Removes Members from the Team.
     * @param access_token 
     * @param team_identifier 
     * @param memberIds 
     * @param successCallback 
     * @param errorCallback 
     */
    public removeMembersFromGroup(access_token: string, team_identifier: string,
        memberIds: any,
        successCallback: (response: any) => void,
        errorCallback: (error: string) => void) {

        if (access_token == null || access_token.trim().length == 0) {
            if (errorCallback) {
                errorCallback('Missing credentials. Please login and retry.');
            }
            return;
        }

        if (team_identifier == null || team_identifier.trim().length == 0) {
            if (errorCallback) {
                errorCallback('Team Identifier should not be empty or null.');
            }
            return;
        }//if

        if (memberIds == null || memberIds.length == 0) {
            if (errorCallback) {
                errorCallback('Member Id array should not be empty.');
            }
            return;
        }//if

        let request_params = "member_identifiers=" + encodeURIComponent(JSON.stringify(memberIds));

        const httpUploadOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded', 'Authorization': 'Bearer ' + access_token }),
            body: request_params
        };

        var endpointUrl: string = this.endpointService.getBaseUrl() + '/accounts/v1/teams/' + team_identifier + "/members";

        this.http.delete(endpointUrl,
            httpUploadOptions).subscribe(
                response => {
                    if (successCallback != null) {
                        successCallback(response);
                    }
                },
                error => {
                    console.log("Error Adding Member to Group: " + JSON.stringify(error));

                    if (errorCallback != null) {
                        var statusCode = -1;
                        if (error && error.hasOwnProperty("status")) {
                            statusCode = error['status'];
                        }//if

                        var errorMessage = 'Unable to remove member from the group';
                        switch (statusCode) {

                            case 401:
                                errorMessage = "Not Authorized. Perhaps you may not have signed in or your session may have expired. Please login and try.";
                                break;

                            case 403:
                                errorMessage = "You are forbidden to perform this operation. You may not have the necessary permissions.";
                                break;

                            case 404:
                                errorMessage = "The team does not exist. Please add the member to a different group.";
                                break;

                            default:
                                break;
                        }
                        errorCallback(errorMessage);
                    }
                }
            )
    }




    /**
     * Calls GET /teams API and returns the list of teams returned buy the API.
     * @param access_token 
     * @param start_index 
     * @param count 
     * @param selector_doc_filter 
     * @param successCallback 
     * @param errorCallback 
     */
    private getTeams(access_token: string,
        start_index: number, count: number,
        selector_doc_filter: any,
        successCallback: (response: any) => void,
        errorCallback: (error: string) => void) {
        /*
        if (access_token == null || access_token.trim().length == 0) {
            if (errorCallback) {
                errorCallback('Missing credentials. Please login and retry.');
            }
            return;
        }
        */

        let httpImageParams = new HttpParams()
            .append('selector_doc', selector_doc_filter)
            //.append('projection', '{"permissions": 0}')
            .append('sort_doc', '[["created", -1]]')
            .append('start', start_index.toString())
            .append('count', count.toString());


        let httpHeaders: HttpHeaders = new HttpHeaders({
            'Content-Type': 'application/json',
//            'Authorization': 'Bearer ' + access_token
        });

        var apiEndpointUrl = this.endpointService.getBaseUrl() + '/accounts/v1/teams';
        this.http.get(
            apiEndpointUrl,
            { headers: httpHeaders, params: httpImageParams, })
            .subscribe(
                response => {
                    if (successCallback != null) {
                        successCallback(response);
                    }
                },
                error => {
                    console.log("Error Fetching Groups: " + JSON.stringify(error));

                    if (errorCallback != null) {
                        var statusCode = -1;
                        if (error && error.hasOwnProperty("status")) {
                            statusCode = error['status'];
                        }
                        var errorMessage = 'Unable to fetch teams. Please try again.';
                        switch (statusCode) {

                            case 401:
                                errorMessage = "Not Authorized. Perhaps you may not have signed in or your session may have expired. Please login and try.";
                                break;

                            case 403:
                                errorMessage = "You are forbidden to perform this operation. You may not have the necessary permissions.";
                                break;


                            default:
                                break;
                        }
                        errorCallback(errorMessage);
                    }
                }
            );
    }

    /**
     * Returns the Teams list of which the User is a part of.
     * 
     * @param user_id
     * @param access_token 
     * @param search_string 
     * @param start_index 
     * @param count 
     * @param successCallback 
     * @param errorCallback 
     */
    public getMyTeams(user_id: string, access_token: string, search_string: string,
        start_index: number, count: number,
        successCallback: (response: any) => void,
        errorCallback: (error: string) => void) {


        if (!user_id || user_id.trim().length == 0) {
            if (errorCallback) {
                errorCallback("User credentials are missing. Perhaps you may need to re-login.");
            }
            return;
        }
        var selector_doc = {};
        // selector_doc["members"] = user_id;
        if (search_string != null && search_string.trim().length > 0) {
            selector_doc["name"] = {"$regex": `^${search_string}.*$` };
        }
        selector_doc = JSON.stringify(selector_doc);
        this.getTeams(access_token, start_index, count,
            selector_doc, successCallback, errorCallback);
    }

    /**
     * Fetches Groups List from the Server.
     * @param access_token 
     * @param search_string 
     * @param start_index 
     * @param count 
     * @param successCallback 
     * @param errorCallback 
     */
    public getGroupsList(access_token: string, search_string: string,
        start_index: number, count: number,
        successCallback: (response: any) => void,
        errorCallback: (error: string) => void) {

        var selector_doc = '{}';
        if (search_string != null && search_string.trim().length > 0) {
            selector_doc = '{"name":  {"$regex": "^' + search_string + '.*$" }}';
        }
        this.getTeams(access_token, start_index, count,
            selector_doc, successCallback, errorCallback);
    }
    /**
     * Returns the Users of a Group.
     * @param access_token 
     * @param group_identifier 
     * @param search_string
     * @param start_index 
     * @param count 
     * @param successCallback 
     * @param errorCallback 
     */
    public getUsersOfGroup(access_token: string, group_identifier: string,
        search_string: string,
        start_index: number, count: number,
        successCallback: (response: any) => void,
        errorCallback: (error: string) => void) {
        var that = this;
        this.getGroupMembers(access_token, group_identifier,
            memberIds => {
                var filter = '"_id": {"$in": ' + JSON.stringify(memberIds) + '}';
                var selector_doc = '{' + filter + '}';
                if (search_string != null && search_string.trim().length > 0) {
                    selector_doc = '{"name":  {"$regex": "^' + search_string + '.*$" }, ' + filter + '}';
                }

                that._userApiService.getUsers(access_token, start_index, count,
                    selector_doc, '{"permissions": 0}', '[["created", -1]]',
                    successCallback, errorCallback);

            },
            error => {
                if (errorCallback) {
                    errorCallback(error);
                }
            });
    }



    /**
     * Returns the Group Member ids.
     * @param access_token 
     * @param group_identifier 
     * @param successCallback 
     * @param errorCallback 
     */
    public getGroupMembers(access_token: string,
        group_identifier: string,
        successCallback: (response: any) => void,
        errorCallback: (error: string) => void) {
        if (access_token == null || access_token.trim().length == 0) {
            if (errorCallback) {
                errorCallback('Missing credentials. Please login and retry.');
            }
            return;
        }

        if (group_identifier == null || group_identifier.trim().length == 0) {
            if (errorCallback) {
                errorCallback('Group Identifier should not be empty.');
            }
            return;
        }


        let httpImageParams = new HttpParams().append('only_explicit_members', 'true');
        let httpHeaders: HttpHeaders = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + access_token
        });

        var apiEndpointUrl = this.endpointService.getBaseUrl() + '/accounts/v1/teams/' + group_identifier + "/members";

        console.log('querying: ', apiEndpointUrl);
        this.http.get(
            apiEndpointUrl,
            { headers: httpHeaders, params: httpImageParams, })
            .subscribe(
                response => {
                    if (successCallback != null) {
                        successCallback(response);
                    }
                },
                error => {
                    if (errorCallback != null) {
                        var statusCode = -1;
                        if (error && error.hasOwnProperty("status")) {
                            statusCode = error['status'];
                        }
                        var errorMessage = 'Unable to fetch Team members.';
                        switch (statusCode) {

                            case 401:
                                errorMessage = "Not Authorized. Perhaps you may not have signed in or your session may have expired. Please login and try.";
                                break;

                            case 403:
                                errorMessage = "You are forbidden to perform this operation. You may not have the necessary permissions.";
                                break;


                            case 404:
                                errorMessage = "Team not found. Please verify the URL or contact the administrator.";
                                break;

                            default:
                                break;
                        }
                        errorCallback(errorMessage);
                    }
                }
            );
    }//end of Method
}//end of class declaration
