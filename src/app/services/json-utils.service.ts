import { Injectable } from '@angular/core';
import { HttpParams, HttpHeaders, HttpClient } from '@angular/common/http';
import { EndpointsService } from '../endpoints.service';

@Injectable({
    providedIn: 'root'
})

/**
 * JsonUtilsService class provides utility methods corresponding to JSON Schema related operations.
 */
export class JsonUtilsService {

    constructor(private endpointService: EndpointsService,
        private http: HttpClient) { }
    
    /**
     * This Methods transforms JSON Schema into a Schema Format that makes Rendering of Dynamic forms simpler and as required by the 
     * web application. 
     * @param jsonSchema The JSON Schema to be transformed.
     * @param includeRequiredFields If true then the fields specified in required array are included in the transformed json
     * @param skipMetaFields Skips Meta fields like agentClass, jsonClass and type.
     */
    public tranformJsonSchema(jsonSchema: any, includeRequiredFields: boolean, skipMetaFields: boolean) {

        if (jsonSchema == null) {
            return null;
        }

        var displayFields = [];
        if (jsonSchema.hasOwnProperty('_display')) {
            var _display = jsonSchema['_display'];
            if (_display && _display.length > 0) {
                _display.forEach(element => {
                    var isAdd = true;
                    if(skipMetaFields){
                        isAdd = !(element === 'agentClass' || element === 'jsonClass' || element === 'type'
                        || element == 'creator' || element === 'logo');
                    }


                    if(isAdd){
                        displayFields.push(element);
                    }
                });

            }
        }

        var requiredFields = []
        if (includeRequiredFields && jsonSchema.hasOwnProperty('required')) {
            var requiredArray = jsonSchema['required'];
            if (requiredArray && requiredArray.length > 0) {
                requiredArray.forEach(element => {
                    if (displayFields.indexOf(element) >= 0) {
                        requiredFields.push(element);
                    }
                });
            }
        }

        var transformedJsonSchema = {};
        var selectedProperties = {}
        if (jsonSchema.hasOwnProperty('properties')) {
            var propertiesArray = jsonSchema['properties'];
            if (propertiesArray) {
                displayFields.forEach(element => {
                    if (propertiesArray.hasOwnProperty(element)) {
                        selectedProperties[element] = propertiesArray[element];
                    }
                });
            }
        }

        transformedJsonSchema['properties'] = selectedProperties;
        transformedJsonSchema['required'] = requiredFields;
        transformedJsonSchema['type'] = 'object';

        return transformedJsonSchema;
    }

    
    /**
     * Fetches the Schema JSON from the server
     * @param schemaType 
     * @param successCallback 
     * @param errorCallback 
     */
    public getSchemaJSON(schemaType: string, successCallback: (response: any) => void,
        errorCallback: (error: string) => void) {

        if (schemaType == null || schemaType.trim().length == 0) {
            if (errorCallback) {
                errorCallback('Schema Type is missing in the request parameters');
            }
            return;
        }
        let httpHeaders: HttpHeaders = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        var apiEndpointUrl = this.endpointService.getBaseUrl() + '/objstore/v1/schemas/' + schemaType;
        this.http.get(
            apiEndpointUrl,
            { headers: httpHeaders })
            .subscribe(
                successResponse => {
                    if (successCallback) {
                        successCallback(successResponse);
                    }
                },
                error => {
                    if (errorCallback) {
                        errorCallback("Unable to fetch the Schema.");
                    }
                }
            );
    }

    public getErrorMessage(error: any, action: string, defaultErrorMessage: string) {
        var statusCode = -1;
        if (error && error.hasOwnProperty("status")) {
            statusCode = error['status'];
        }//if

        var errorMessage = defaultErrorMessage;
        switch (statusCode) {

            case 401:
                errorMessage = "Not Authorized. Perhaps you may not have signed in or your session may have expired. Please login and try.";
                break;

            case 403:
                errorMessage = "You are forbidden to perform this operation. You may not have the necessary permissions.";
                break;

            case 404:
                if (action) {
                    if (action === 'grant_revoke_permissions') {
                        errorMessage = "Unable to grant or revoke permissions. The Resource does not exist in the database.";
                    } else if (action === 'create_resource') {
                        errorMessage = "Unable to create the resource. Some required information missing in database";
                    }
                } else {
                    errorMessage = "404. Not found error.";
                }
                break;

            default:
                break;
        }
        return errorMessage
    }

}
