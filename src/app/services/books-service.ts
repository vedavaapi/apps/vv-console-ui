import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import {OauthHelperService} from 'src/app/services/oauth-helper.service';
import { joFromJson, ScannedBook, StillImage, ScannedPage } from 'src/app/services/models'
import { ObjstoreApiService } from 'src/app/services/objstore-api.service'


@Injectable({
    providedIn: 'root'
})
export class BooksService {
    constructor(private http: HttpClient, private oauthHelperService: OauthHelperService, private objstore: ObjstoreApiService) {

    }

    private wrapperScb(successCallback: (response: any) => void) {
        return (result: object) => {
            result = joFromJson(result);
            successCallback ? successCallback(result) : null;
        };
    }

    private wrapperEcb(errorCallback: (response: any) => void) {
        return (error: object) => {
            errorCallback ? errorCallback(error) : null;
        };
    }

    public getBooks(vvSiteUrl: string, selectorDoc?: object, projection?: object, sortDoc?: object, start?: number, count?: number, countOnly = false, successCallback?: (response: any) => void, errorCallback?: (error: any) => void) {
        if(selectorDoc) {
            selectorDoc['jsonClass'] = {"$in": ['ScannedBook', 'TextDocument']};
        }
        this.objstore.getResources(
            vvSiteUrl, selectorDoc, projection, sortDoc,
            start, count, countOnly, this.wrapperScb(successCallback),
            this.wrapperEcb(errorCallback)
        );
    }

    public getBook(vvSiteUrl: string, bookId: string, projection?: object, successCallback?: (response: any) => void, errorCallback?: (error: any) => void) {

        this.objstore.getResource(vvSiteUrl, bookId, projection, this.wrapperScb(successCallback), this.wrapperEcb(errorCallback)); 
    }

    public postBookGraph(
        vvSiteUrl: string, source: string, title: string, metadataMap: any, authorNames: Array<string>,
        coverImageFile?: File, jsonClassLabel?: string, pageFiles?: Array<File>,
        successCallback?: (response: any) => void, errorCallback?: (error: any) => void,
        shouldReturnResources = false, shoulReturnOOLDResources = false,
        returnProjectionMap?: object, upsert = false) {

        let graph = {};
        let oold_graph = {};

        let bookBlankId = '_:book';
        let coverOOLDBlankId = '_:book_cover';

        let book = new ScannedBook(source, metadataMap, title, authorNames, coverOOLDBlankId, jsonClassLabel);
        book['_id'] = bookBlankId;
        graph[bookBlankId] = book;

        let coverOOLD = new StillImage(bookBlankId, '_vedavaapi', coverImageFile.name);
        coverOOLD['_id'] = coverOOLDBlankId;
        oold_graph[coverOOLDBlankId] = coverOOLD;

        pageFiles.forEach((pageFile, i) => {
            let pageBlankId = `_:page-${i}`;
            let pageImageOOLDBlankId = `_:page-${i}-image`;

            let page = new ScannedPage(bookBlankId, [pageImageOOLDBlankId], {jsonClass: 'QualitativeSelector'});
            page['_id'] = pageBlankId;
            graph[pageBlankId] = page;

            let pageImageOOLD = new StillImage(pageBlankId, '_vedavaapi', pageFile.name);
            pageImageOOLD['_id'] = pageImageOOLDBlankId;
            oold_graph[pageImageOOLDBlankId] = pageImageOOLD;
        });

        let allFiles = (coverImageFile ? [coverImageFile] : []).concat(pageFiles || []);
        this.objstore.postGraph(
            vvSiteUrl, graph, oold_graph, allFiles,
            shouldReturnResources, shoulReturnOOLDResources,
            returnProjectionMap, upsert,
            this.wrapperScb(successCallback), this.wrapperEcb(errorCallback)
        );
    }
}
