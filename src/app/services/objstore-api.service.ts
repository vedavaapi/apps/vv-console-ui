import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { from, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class ObjstoreApiService {

    private objstoreApiPrefix = 'objstore/v1';

    private resourcesUrlPart = 'resources';
    private graphUrlPart = 'graph';
    private filesUrlPart = 'files';

    constructor(private http: HttpClient) {

    }

    private absUrl(vvSiteUrl, urlPart) {
        return `${vvSiteUrl}/${this.objstoreApiPrefix}/${urlPart}`;
    }

    private purgeUndefinedParams(doc: object) {
        Object.keys(doc).forEach((k) => {
            if(doc[k] == undefined) {
                delete doc[k];
            }
        });
    }

    private defaultSuccessCallback(response: any) {
        return;
    }

    private defaultErrorCallback(error: any) {
        return;
    }

    private getHttpParams(params: object) {
        let httpParams = new HttpParams();
        Object.keys(params).forEach(k => {
            if (params[k] != undefined) {
                httpParams = httpParams.append(k, params[k]);
            }
        });
        return httpParams;
    }

    private getFormData(data: object) {
        let formData = new FormData();
        Object.keys(data).forEach(k => {
            if(data[k] !== undefined) {
                formData.append(k, data[k]);
            }
        });
        return formData;
    }

    private stringifyIfDefined(doc) {
        return (doc == undefined) ? doc : JSON.stringify(doc);
    }

    private getResourcesApiParams(selectorDoc: object, projection?: object, sortDoc?: object, start?: number, count?: number, countOnly = false) {
        let params = {
            selector_doc: JSON.stringify(selectorDoc),
            projection: this.stringifyIfDefined(projection),
            sort_doc: this.stringifyIfDefined(sortDoc),
            start: this.stringifyIfDefined(start),
            count: this.stringifyIfDefined(count),
            return_count: JSON.stringify(countOnly)
        };
        let httpParams = this.getHttpParams(params);
        return httpParams;
    }

    private subscribeHttpObservable(observable: Observable<any>, successCallback?: (response: any) => void, errorCallback?: (error: any) => void) {
        observable.subscribe(
            response => (successCallback || this.defaultSuccessCallback)(response),
            error => (errorCallback || this.defaultErrorCallback)(error)
        );
    }

    public getResources(vvSiteUrl: string, selectorDoc: object, projection?: object, sortDoc?: object, start?: number, count?: number, countOnly=false, successCallback?: (response: any) => void, errorCallback?: (error: any) => void) {

        let resourcesUrl = this.absUrl(vvSiteUrl, this.resourcesUrlPart);
        let httpParams = this.getResourcesApiParams(selectorDoc, projection, sortDoc, start, count, countOnly);

        let responseObservable = this.http.get(resourcesUrl, {
            headers: new HttpHeaders({'Content-Type': 'application/json'}),
            params: httpParams
        });
        this.subscribeHttpObservable(responseObservable, successCallback, errorCallback);
    }

    public getResource(vvSiteUrl: string, resourceId: string, projection?: any, successCallback?: (response: any) => void, errorCallback?: (error: any) => void, inLinks=false) {
        let resourceUrl =this.absUrl(vvSiteUrl, `${this.resourcesUrlPart}/${resourceId}`);
        let params = {projection: this.stringifyIfDefined(projection), 'attach_in_links': JSON.stringify(inLinks)};
        let httpParams = this.getHttpParams(params);
        
        let responseObservable = this.http.get(resourceUrl, {
            headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
            params: httpParams
        });
        this.subscribeHttpObservable(responseObservable, successCallback, errorCallback);
    }

    public getGraph(vvSiteUrl: string, startNodesSelector: object, direction: string, startNodesSortDoc?: object, startNodesOffset?: number, startNodesCount?: number, traverseKeyFilterMapList?: Array<object>, hopInclusionsConfig?: Array<boolean>, includeIncompletePaths = false, maxHops = 0, includeOOLDGraph = false, jsonClassProjectionMap?: object, successCallback?: (response: any) => void, errorCallback?: (error: any) => void) {

        let params = {
            start_nodes_selector: JSON.stringify(startNodesSelector), direction: direction,
            start_nodes_sort_doc: this.stringifyIfDefined(startNodesSortDoc), start_nodes_offset: startNodesOffset, start_nodes_count: startNodesCount,
            traverse_key_filter_maps_list: this.stringifyIfDefined(traverseKeyFilterMapList),
            hop_inclusions_config: this.stringifyIfDefined(hopInclusionsConfig),
            include_incomplete_paths: this.stringifyIfDefined(includeIncompletePaths),
            max_hops: maxHops,
            include_ool_data_graph: this.stringifyIfDefined(includeOOLDGraph),
            json_class_projection_map: this.stringifyIfDefined(jsonClassProjectionMap)
        };
        let httpParams = this.getHttpParams(params);

        let graphUrl = this.absUrl(vvSiteUrl, this.graphUrlPart);
        let responseObservable = this.http.get(graphUrl, {
            headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
            params: httpParams
        });
        this.subscribeHttpObservable(responseObservable, successCallback, errorCallback);
    }

    public postResources(vvSiteUrl: string, resources: Array<object>, returnProjection?: object, upsert = false, successCallback?: (response: any) => void, errorCallback?: (error: any) => void) {
        let data = {
            resource_jsons: JSON.stringify(resources),
            return_projection: this.stringifyIfDefined(returnProjection),
            upsert: JSON.stringify(upsert)
        };
        let formData = this.getFormData(data);
        let resourcesUrl = this.absUrl(vvSiteUrl, this.resourcesUrlPart);

        let responseObservable = this.http.post(resourcesUrl, formData);
        this.subscribeHttpObservable(responseObservable, successCallback, errorCallback);
    }

    public postGraph(vvSiteUrl: string, graph: object, ooldGraph?: object, files?: Array<any>, shouldReturnResources = false, shoulReturnOOLDResources = false, returnProjectionMap?: object, upsert = false, successCallback?: (response: any) => void, errorCallback?: (error: any) => void) {

        let data = {
            graph: JSON.stringify(graph),
            ool_data_graph: this.stringifyIfDefined(ooldGraph),
            should_return_resources: JSON.stringify(shouldReturnResources),
            should_return_oold_resources: JSON.stringify(shoulReturnOOLDResources),
            upsert: JSON.stringify(upsert),
            return_projection_map: this.stringifyIfDefined(returnProjectionMap)
        };
        let formData = this.getFormData(data);
        if(files) {
            files.forEach(f => {
                formData.append('files', f);
            });
        }

        let graphPostUrl = this.absUrl(vvSiteUrl, this.graphUrlPart);
        let responseObservable = this.http.post(graphPostUrl, formData);
        this.subscribeHttpObservable(responseObservable);
    }

    public deleteResources(vvSiteUrl: string, resourceIds: Array<string>, successCallback?: (response: any) => void, errorCallback?: (error: any) => void) {
        let data = {
            resource_ids: JSON.stringify(resourceIds)
        };
        let formData = this.getFormData(data);

        let resourcesUrl = this.absUrl(vvSiteUrl, this.resourcesUrlPart);
        let httpOptions = {
            headers: new HttpHeaders({ 'Accept': 'application/json' }),
            body: formData
        }
        let responseObservable = this.http.delete(resourcesUrl, httpOptions);
        this.subscribeHttpObservable(responseObservable, successCallback, errorCallback);
    }
}
