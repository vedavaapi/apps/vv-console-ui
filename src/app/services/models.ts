import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';


function purgeUndefined(doc: object) {
    Object.keys(doc).forEach(k => {
        if(doc[k] == undefined) {
            delete doc[k];
        }
    });
}


export class TextObj {
    public jsonClass: string;
    public chars?: string;
    public script?: string;

    constructor(chars?: string, script?: string) {
        let jo = {
            jsonClass: "Text",
            chars: chars,
            script: script
        };
        purgeUndefined(jo);
        Object.assign(this, jo);
    }
}


export class MetadataItem {
    public jsonClass: string;
    public label?: string;
    public value?: string;

    constructor(label?: string, value?: string) {
        let jo = { jsonClass: "MetadataItem", label: label, value: value };
        purgeUndefined(jo);
        Object.assign(this, jo);
    }
}


export class Person {
    public jsonClass: string;
    public name?: Array<TextObj>;
    public agentClass?: string;

    constructor(name?: string) {
        let jo = {
            jsonClass: "Person",
            name: name ? [new TextObj(name)] : undefined,
            agentClass: "Person"
        };
        purgeUndefined(jo);
        Object.assign(this, jo);
    }

    getName() {
        if (!this.name || !this.name.length || !this.name[0].chars) {
            return null;
        }
        return this.name[0].chars;
    }
}


export class StillImage {
    jsonClass: string;
    source?: string;
    namespace?: string;
    identifier?: string;
    url?: string

    constructor(source?: string, namespace = '_vedavaapi', identifier?: string, url?: string) {
        let jo = {
            jsonClass: "StillImage",
            source: source,
            namespace: namespace,
            identifier: identifier,
            url: url
        };
        purgeUndefined(jo);
        Object.assign(this, jo);
    }
}


export class StillImageRepresentation {
    jsonClass: string;
    data?: string;
    implements?: Array<string>;

    constructor(ooldId?: string) {
        let jo = {
            jsonClass: "StillImageRepresentation",
            data: `_OOLD:${ooldId}`
        };
        purgeUndefined(jo);
        Object.assign(this, jo);
    }

    getOOLDId() {
        if(!this.data) {
            return null;
        }
        return this.data.slice(6);
    }

    supportsIIIF() {
        return this.implements && Array.isArray(this.implements) && this.implements.indexOf('iiif_image') > 0;
    }

    getIIIFOrDefaultUrl(vvSiteUrl?: string, xywh?: string, resolution?: string, rotation='0', quality='default', format='png') {
        if(!this.supportsIIIF()) {
            return `${vvSiteUrl}/objstore/v1/files/${this.getOOLDId()}`;
        }
        else {
            return `${vvSiteUrl}/iiif_image/v1/objstore/${this.getOOLDId()}/${xywh}/${resolution}/${rotation}/${quality}.${format}`;
        }
    }
}


export class DataRepresentations {
    jsonClass = 'DataRepresentations';
    default?: string;
    stillImage?: Array<StillImageRepresentation>

    constructor(defaultRepr?: string, stillImageOOLDIds?: Array<string>) {
        let jo = {
            jsonClass: "DataRepresentations",
            default: defaultRepr,
            stillImage: stillImageOOLDIds ? stillImageOOLDIds.map(ooldId => new StillImageRepresentation(ooldId)) : undefined
        };
        purgeUndefined(jo);
        Object.assign(this, jo);
    }

    getStillImage() {
        if(!this.stillImage || !this.stillImage.length || !this.stillImage[0]) {
            return null;
        }
        return this.stillImage[0];
    }
}


export class ScannedBook {
    jsonClass = 'ScannedBook';
    jsonClassLabel?: string;
    _id?: string;
    source?: string;
    metadata?: Array<MetadataItem>;
    title: string;
    author?: Array<string>;
    cover?: StillImageRepresentation;

    getJc() {
        return 'ScannedBook';
    }

    constructor(source?: string, metadataMap?: object, title?: string, authors?: Array<string>, coverOOLDId?: string, jsonClassLabel?: string) {

        let metadata = [];
        if(metadataMap) {
            Object.keys(metadataMap).forEach(l => {
                let metadataItem = new MetadataItem(l, metadataMap[l]);
                metadata.push(metadataItem);
            });
        }
        
        let jo = {
            jsonClass: this.getJc(),
            jsonClassLabel: jsonClassLabel,
            source: source,
            metadata: metadata.length ? metadata : undefined,
            title: title || undefined,
            author: authors || undefined,
            cover: coverOOLDId ? new StillImageRepresentation(coverOOLDId) : undefined
        };
        purgeUndefined(jo);
        Object.assign(this, jo);
    }

    getTitle() {
        return this.title;
    }

    getAuthors() {
        return this.author;
    }

    getMetadataMap() {
        if(!this.metadata || !Array.isArray(this.metadata) || !this.metadata.length) {
            return null;
        }
        let metadataMap = {};
        for(let mi of this.metadata) {
            metadataMap[mi.label] = mi.value;
        } 
        return metadataMap;
    }
}


export class TextDocument extends ScannedBook {
    jsonClass = 'TextDocument';
    
    getJc() {
        return 'TextDocument';
    }
}


export class ScannedPage {
    jsonClass = 'ScannedPage';
    _id?: string;
    purpose = 'page';
    selector?: object;
    source?: string;
    representations?: DataRepresentations;

    constructor(source?: string, stillImageOOLDIds?: Array<string>, selector?: object) {
        let jo = {
            jsonClass: "ScannedPage",
            purpose: "page",
            selector: selector,
            source: source,
            representations: stillImageOOLDIds ? new DataRepresentations('stillImage', stillImageOOLDIds) : undefined
        };
        purgeUndefined(jo);
        Object.assign(this, jo);
    }
}

export class Library {
    jsonClass = "Library";
    _id?: string;
    source?: string;
    name?: string;
    description?: string;
    logo?: StillImageRepresentation
}


let jsonClassRegistry = {
    Text: TextObj, StillImage, StillImageRepresentation,
    MetadataItem, Person, ScannedBook, Library, TextDocument
}

export function joFromJson(json: any) {

    if(Object.prototype.isPrototypeOf(json)) {
        let jsonClass = json.jsonClass;
        if (!jsonClassRegistry[jsonClass]) {
            var jo = json;
        }
        else {
            var jo = new jsonClassRegistry[jsonClass]();
            Object.assign(jo, json);
        }
        Object.keys(jo).forEach(k => {
            jo[k] = joFromJson(jo[k]);
        });
        return jo;
    }
    else if(Array.isArray(json)) {
        return json.map(item => joFromJson(item));
    }
    else {
        return json;
    }
}
