import { Injectable } from '@angular/core';
import { HttpParams, HttpHeaders, HttpClient } from '@angular/common/http';
import { EndpointsService } from '../endpoints.service';

@Injectable({
    providedIn: 'root'
})
export class UserApiService {

    constructor(private endpointService: EndpointsService, private http: HttpClient) {

    }


    /**
     * Creates a User and adds him to the team if the team id is passed to the method.
     * @param access_token 
     * @param user_json 
     * @param teamId 
     * @param successCallback 
     * @param errorCallback 
     */
    public createUser(access_token: string, user_json: any,
        teamId: string,
        successCallback: (response: any) => void,
        errorCallback: (error: string) => void) {

        if (access_token == null || access_token.trim().length == 0) {
            if (errorCallback) {
                errorCallback('Missing credentials. Your login session may have expired. Please login and retry.');
            }
            return;
        }

        const httpUploadOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded', 'Authorization': 'Bearer ' + access_token })
        };

        let request_params = "user_json=" + encodeURIComponent(JSON.stringify(user_json));
        if (teamId) {
            request_params += "&initial_team_id=" + encodeURIComponent(teamId);
        }

        var endpointUrl: string = this.endpointService.getBaseUrl() + '/accounts/v1/users';
        this.http.post(endpointUrl, request_params,
            httpUploadOptions).subscribe(
                response => {
                    if (successCallback != null) {
                        successCallback(response['items']);
                    }
                },
                error => {
                    console.log("Error Creating a User: " + JSON.stringify(error));

                    if (errorCallback != null) {
                        var statusCode = -1;
                        if (error && error.hasOwnProperty("status")) {
                            statusCode = error['status'];
                        }//if

                        var errorMessage = 'Unable to create the User';
                        switch (statusCode) {

                            case 401:
                                errorMessage = "Not Authorized. Perhaps you may not have signed in or your session may have expired. Please login and try.";
                                break;

                            case 403:
                                errorMessage = "You are forbidden to perform this operation. This may be a temporary condition. Please check your input data.";
                                break;

                            default:
                                break;
                        }
                        errorCallback(errorMessage);
                    }
                }
            )
    }//end of method


    /**
   * Extracts the Error status code from the JSON Object and 
   * returns an appropriate error message based on the action whose exection had failed.
   * @param error 
   * @param action 
   * @param defaultErrorMessage 
   */
    private getErrorMessage(error: any, action: string, defaultErrorMessage: string) {
        var statusCode = -1;
        if (error && error.hasOwnProperty("status")) {
            statusCode = error['status'];
        }//if

        var errorMessage = defaultErrorMessage;
        switch (statusCode) {

            case 401:
                errorMessage = "Not Authorized. Perhaps you may not have signed in or your session may have expired. Please login and try.";
                break;

            case 403:
                errorMessage = "You are forbidden to perform this operation. You may not have the necessary permissions.";
                break;

            case 404:
                errorMessage = "The User account may not exist in the database.";
                break;

            default:
                break;
        }
        return errorMessage
    }


    /**
     * Deletes the User Account.
     * @param access_token 
     * @param user_identifier 
     * @param successCallback 
     * @param errorCallback 
     */
    public deleteUser(access_token: string,
        user_identifier: string,
        successCallback: (response: any) => void,
        errorCallback: (error: string) => void) {
        if (access_token == null || access_token.trim().length == 0) {
            if (errorCallback) {
                errorCallback('Missing credentials. Please login and retry.');
            }
            return;
        }

        if (user_identifier == null || user_identifier.trim().length == 0) {
            if (errorCallback) {
                errorCallback('User Identifier should not be empty or null.');
            }
            return;
        }//if

        let request_params = 'user_ids=["' + user_identifier + '"]';
        const httpUploadOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded', 'Authorization': 'Bearer ' + access_token }),
            body: request_params
        };

        var endpointUrl: string = this.endpointService.getBaseUrl() + '/accounts/v1/users';
        this.http.delete(endpointUrl,
            httpUploadOptions).subscribe(
                response => {
                    if (successCallback != null) {
                        successCallback(response['items']);
                    }
                },
                error => {
                    if (errorCallback) {
                        errorCallback(this.getErrorMessage(error, 'delete_user', "Unable to delete the User."));
                    }
                }
            );
    }



    /**
     * Fetches the User list from the server.
     * @param access_token 
     * @param startIndex 
     * @param count 
     * @param selector_doc Important for applying various filters.
     * @param projection Controls the response attributes to be fetched or not fetched.
     * @param sort_doc To specify the attribute to be sorted.
     * @param successCallback 
     * @param errorCallback 
     */
    public getUsers(access_token: string, startIndex: number, count: number,
        selector_doc: string,
        projection: string,
        sort_doc: string,
        successCallback: (response: any) => void,
        errorCallback: (error: string) => void) {

        if (access_token == null || access_token.trim().length == 0) {
            if (errorCallback) {
                errorCallback('Missing credentials. Please login and retry.');
            }
            return;
        }

        //selectorDoc['jsonClass'] = 'User';
        //selectorDoc['_id'] = '{"$in": ["5c7a6de9a9e2cb000889ec36", "5c7a8c81a9e2cb0008ba6bfd"]}';


        let httpImageParams = new HttpParams()
            .append('selector_doc', selector_doc)
            //.append('projection', projection)
            .append('sort_doc', sort_doc)
            .append('start', startIndex.toString())
            .append('count', count.toString());


        let httpHeaders: HttpHeaders = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + access_token
        });

        var apiEndpointUrl = this.endpointService.getBaseUrl() + '/accounts/v1/users';
        console.log('querying: ', apiEndpointUrl);
        this.http.get(
            apiEndpointUrl,
            { headers: httpHeaders, params: httpImageParams, })
            .subscribe(
                response => {
                    if (successCallback != null) {
                        successCallback(response);

                    }
                },
                error => {
                    console.log("Error Fetching users: " + JSON.stringify(error));

                    if (errorCallback != null) {
                        var statusCode = -1;
                        if (error && error.hasOwnProperty("status")) {
                            statusCode = error['status'];
                        }
                        var errorMessage = 'Unable to fetch users. Please try again.';
                        switch (statusCode) {

                            case 401:
                                errorMessage = "Not Authorized. Perhaps you may not have signed in or your session may have expired. Please login and try.";
                                break;

                            default:
                                break;
                        }
                        errorCallback(errorMessage);
                    }
                }
            );
    }

    /**
     * Returns The User List
     * @param access_token The Access Token should be sent as part of Authorization header.
     * @param searchString Search user by name.
     * @param startIndex Start Index.
     * @param count Number of results to be fetched.
     * @param successCallback Callback method for successful response.
     * @param errorCallback Callback method to handle error response.
     */
    public getUserList(access_token: string, searchString: string, startIndex: number, count: number, successCallback: (response: any) => void,
        errorCallback: (error: string) => void) {


        let selectorDoc = '{}';
        if (searchString != null && searchString.trim().length > 0) {
            selectorDoc = '{"name":  {"$regex": "^' + searchString + '.*$" }}';
        }

        this.getUsers(access_token, startIndex, count,
            selectorDoc, '{"permissions": 0}', '[["created", -1]]', successCallback, errorCallback);

    }//end of method.
}
