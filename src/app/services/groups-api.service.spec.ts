import { TestBed, inject } from '@angular/core/testing';

import { GroupsApiService } from './groups-api.service';

describe('GroupsApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GroupsApiService]
    });
  });

  it('should be created', inject([GroupsApiService], (service: GroupsApiService) => {
    expect(service).toBeTruthy();
  }));
});
