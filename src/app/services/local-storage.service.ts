import { Injectable } from '@angular/core';
import { Router } from '@angular/router'

@Injectable(/*{
  providedIn: 'root'
}*/)
export class LocalStorageService {


    constructor(private Router: Router) {}

    public getAppName(appUrl?:string) {
        if(!appUrl) {
            appUrl = window.location.href;
        }
        // console.log({urlTree: this.Router.parseUrl(window.location.href), root: this.Router.url, href: window.location.href});
        let href = window.location.href.split('?')[0];
        let rout = this.Router.url.split('?')[0];

        let matchRe = new RegExp(`^([^:]*)://([^/]*)(/.*)?${rout}.*$`);
        let urlParts = href.match(matchRe);
        // console.log({ matchRe, urlParts, href, name: urlParts[3] });
        let appName = urlParts[3] || '/';
        for(let k of Object.keys(localStorage)) {
            if (appName.startsWith(k)) {
                return k;
            }
        }
        return appName;
    }

  /**
   * Stores Key Value pair in Local Storage.
   * @param key 
   * @param value 
   * @param appUrl
   */
  public storeValue(key: string, value: string, appUrl?: string) {
    if(key == null || value == null){
      return;
    }//if
    let appName = this.getAppName();
    let appData = JSON.parse(localStorage.getItem(appName) || '{}');
    appData[key] = value;
    localStorage.setItem(appName, JSON.stringify(appData));
  }
  
  /**
   * Returns Locally stored value associated with the Key.
   * @param key 
   */
  public getValue(key: string) {
    if(key == null){
      return null;
    }
    let appName = this.getAppName();
    let appData = JSON.parse(localStorage.getItem(appName) || '{}');
    return appData[key];
  }
  

}
