import { Injectable, OnInit } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpResponse,
    HttpErrorResponse
} from "@angular/common/http";
import { LocalStorageService } from "./local-storage.service";
import { e } from "@angular/core/src/render3";
import { Observable } from "rxjs";
import { HttpXhrBackend } from "@angular/common/http";

interface AuthorizedCredsForVvSite {
    access_token: string;
}

/*
 * app_mntpath : { auth_creds : 
 *                      { 
 *                         site1_url : 
 *                          { 
 *                             user_email1 : { access_token : "...",
 *                                             expiry_date : "....",
 *                                             profile_fields } 
 *                             user_email2 : { profile } 
 *                             user_email3 : { profile } 
 *                          }
 *                         site2_url : { user_email2 : { profile } }
 *                         site3_url : { user_email2 : { profile } }
                        }
 *                 client_creds : { site1 : oauth_client_details }
 *                 registered_apps : 
 *
 */

@Injectable(/*{
    providedIn: "root"
}*/)
export class OauthHelperService {

    public clientCredsKey = "client_creds";
    public authorizedCredsKey = "authorized_creds";
    public userDetailsKey = "user_details";
    public applicationsDetailsKey = "applications";

    public authorizationChangeBroadcastChannelName =
        "authorization_change_channel";
    public applicationsUpdateBroadcastChannelName = "applications_update_channel";

    public clientCredsUrl = "assets/config/client_creds.json";
    // public companionUrl = "http://localhost:5000";
    public companionUrl = '../api';
    

    private popUpWindow: Window;

    constructor(
        private http: HttpClient,
        private localstorageService: LocalStorageService
    ) {
        this.subscribeOauthCallbackBroadcast();
        for(let k of [this.authorizedCredsKey, this.userDetailsKey, this.applicationsDetailsKey, this.clientCredsKey]) {
            let persistedObj = this.getStorageView(k);
            if(!persistedObj) {
                this.localstorageService.storeValue(k, '{}');
            }
        }
        let a = document.createElement('a');
        a.href = this.companionUrl;
        this.companionUrl = a.href;
        a.remove();
        if (this.companionUrl.endsWith('/')) {
            this.companionUrl = this.companionUrl.substr(0, this.companionUrl.length - 1);
        }
        /* TODO
        for(let k in this.getStorageView(this.authorizedCredsKey)) {
            console.log({k});
            this.initializeAuthorizationFlowIfToBeExpired(5*60*1000, 'oauth_initializer', true, k, '');
            setTimeout(() => {
                console.log('in set timeout fn')
                this.initializeAuthorizationFlowIfToBeExpired(5*60*1000, "oauth_initializer", true, k, "" );
            }, 4*60*1000);
            
        }*/
        
    }

    /**
     * when user authorized/ unauthorized, a broadcast will be broadcasted.
     * this service subscribes to those broadcasts, and inturn it brodcasts a centralized authorizationChange nofication broadcast.
     * any actions that should be performed on authorization change should subscribe to the seconde centralized broadcast.
     */
    public subscribeOauthCallbackBroadcast() {
        let bc = new BroadcastChannel("oauth_callback_channel");
        bc.onmessage = ({ data }) => {
            this.popUpWindow.close();
            this.popUpWindow = null;
            console.log({ data });
            if (data && data.success && data.creds) {
                this.persistAuthorizedCreds(data["creds"], data["vv_site_url"]);
                this.persistUserDetails(data["user_details"], data["vv_site_url"]);
                this.publishAuthorizationChangeBroadcast(
                    data["vv_site_url"],
                    true,
                    data["additional_state"]
                );
            }
        };
    }

    /**
     * centralized authorization change brodcast. any component, that have to perfoorm operations at time of auth change should subscribe to this broadcast. see unified-header component, for implementation.
     * @param vvSiteUrl 
     * @param authorizationStatus 
     * @param additionalState 
     */
    public publishAuthorizationChangeBroadcast(
        vvSiteUrl: string,
        authorizationStatus: boolean,
        additionalState: object
    ) {
        this.getRegisteredApplications(vvSiteUrl);
        console.log("publishing to authorization change broadcast");
        let bc = new BroadcastChannel(this.authorizationChangeBroadcastChannelName);
        let message = {
            authorized: authorizationStatus,
            vv_site_url: vvSiteUrl,
            additional_state: additionalState
        };
        bc.postMessage(message);
    }

    /**
     * when registered applications are retrieved, this will get fired.
     */
    public publishApllicationsUpdateBroadcast() {
        console.log("publishing applications update broadcast");
        let bc = new BroadcastChannel(this.applicationsUpdateBroadcastChannelName);
        bc.postMessage({ updated: true });
    }

    /**
      * gets oauth client_creds from assets, and saves them into LocalStorage
      * @param successCallback
      * @param errorCallback
      */
    public getOauthClientCreds(
        successCallback: () => void,
        errorCallback: () => void
    ) {
        this.http.get(this.clientCredsUrl).subscribe(
            response => {
                console.log({ message: "success in getting client_creds", response });
                this.localstorageService.storeValue(
                    this.clientCredsKey,
                    JSON.stringify(response)
                );
                successCallback();
            },
            error => {
                console.log({ message: "error in getting client_creds", error });
                errorCallback();
            }
        );
    }

    public getCompanionUrl() {
        return this.companionUrl;
    }

    public getVVSiteDetails(
        successCallback: () => void,
        errorCallback: () => void
    ) {
        this.http.get(`${this.getCompanionUrl()}/oauth/site`).subscribe(
            response => {
                console.log({ message: "success in getting vvsite details", response });
                this.localstorageService.storeValue(
                    'server_url',
                    response['url']
                );
                successCallback();
            },
            error => {
                console.log({ message: "error in getting vvsite details", error });
                errorCallback();
            }
        );
    }

    /**
     * 
     * @param vvSiteUrl gets persisted oauth client creds, like client_id, etc.
     */
    public getPersistedOauthClientCreds(vvSiteUrl?: string) {
        if (!vvSiteUrl) {
            vvSiteUrl = this.localstorageService.getValue("server_url");
        }
        let clientCreds = this.getStorageView(this.clientCredsKey);
        if (!vvSiteUrl || !((vvSiteUrl in clientCreds) || ('*' in clientCreds))) {
            return null;
        }
        return (clientCreds[vvSiteUrl] || clientCreds['*']);
    }

    /**
     * anything in localstorage, it's live-view will be returned, after jsonifying if needed.
     * @param key 
     * @param isJson 
     */
    public getStorageView(key: string, isJson=true) {
        let persistedString = this.localstorageService.getValue(key);
        if(!persistedString) {
            return persistedString;
        }
        if(isJson) {
            return JSON.parse(persistedString);
        }
        return persistedString;
    }

    /**
     * retrieves registered application from registry api.
     * @param vvSiteUrl 
     * @param successCallback 
     * @param errorCallback 
     */
    public getRegisteredApplications(
        vvSiteUrl?: string,
        successCallback?: () => void,
        errorCallback?: () => void
    ) {
        if (!vvSiteUrl) {
            vvSiteUrl = this.localstorageService.getValue("server_url");
        }

        if (!vvSiteUrl) {
            if (errorCallback) {
                errorCallback();
            }
        }

        let resourcesUrl = `${vvSiteUrl}/objstore/v1/resources`;
        let params = new HttpParams().append('projection', '{"permissions": 0}').append("selector_doc", JSON.stringify({ "jsonClass": "Service" }) );
        this.http.get(resourcesUrl, {params: params}).subscribe(
            response => {
                /*let mResponse = {};
                for(let service of response['items']) {
                    mResponse[service['name']] = mResponse[service['name']] || [];
                    mResponse[service['name']].push(service);
                }*/
                let applicationsDetails = {
                    [vvSiteUrl]: response['items']
                };
                this.localstorageService.storeValue(
                    this.applicationsDetailsKey,
                    JSON.stringify(applicationsDetails)
                );
                this.publishApllicationsUpdateBroadcast();
                if (successCallback) {
                    successCallback();
                }
            },
            error => {
                console.log("error in getting applications", error);
                if (errorCallback) {
                    errorCallback();
                }
            }
        );
    }

    /**
     * gets localstorage persisted applications.
     * @param vvSiteUrl 
     */
    public getApplications(vvSiteUrl?: string) {
        if (!vvSiteUrl) {
            vvSiteUrl = this.localstorageService.getValue("server_url");
        }
        if (!vvSiteUrl) {
            return null;
        }
        let allSitesApplicationsDetails = this.getStorageView(this.applicationsDetailsKey);
        if(!allSitesApplicationsDetails) {
            return {};
        }
        return allSitesApplicationsDetails[vvSiteUrl];
    }

    /**
     * user authorized creds persisted in localstorage.
     * @param vvSiteUrl 
     */
    public getAuthorizedCreds(vvSiteUrl?: string) {
        if (!vvSiteUrl) {
            vvSiteUrl = this.localstorageService.getValue("server_url");
        }
        let authorizedCreds = this.getStorageView(this.authorizedCredsKey);
        console.log({vvSiteUrl, authorizedCreds});
        if (
            !vvSiteUrl ||
            !(vvSiteUrl in authorizedCreds)
        ) {
            return null;
        }
        let authorizedCredsForCurrentVvSite = authorizedCreds[vvSiteUrl];
        console.log({"vvSiteCreds": authorizedCredsForCurrentVvSite});
        return authorizedCredsForCurrentVvSite;
    }

    public getAuthorizationHeaders(vvSiteUrl?: string) {
        let authorizedCredsForVvSite = this.getAuthorizedCreds(vvSiteUrl);
        if (!authorizedCredsForVvSite) {
            return null;
        }
        let access_token = authorizedCredsForVvSite["access_token"];
        return {
            Authorization: `Bearer ${access_token}`
        };
    }

    public getAuthorizedUserDetails(vvSiteUrl?: string) {
        if (!vvSiteUrl) {
            vvSiteUrl = this.localstorageService.getValue("server_url");
        }
        let userDetails = this.getStorageView(this.userDetailsKey);
        // console.log({vvSiteUrl, 'authorizedCreds': this.authorizedCreds});
        if (
            !vvSiteUrl ||
            !(vvSiteUrl in userDetails)
        ) {
            return null;
        }
        let userDetailsForCurrentVvSite = userDetails[vvSiteUrl];
        return userDetailsForCurrentVvSite;
    }

    /**
     * persists authorized credentials of user for a vvSite.
     * @param autthorizedCredsForVvSite 
     * @param vvSiteUrl 
     */
    private persistAuthorizedCreds(
        autthorizedCredsForVvSite: AuthorizedCredsForVvSite,
        vvSiteUrl?: string
    ) {
        if (!vvSiteUrl) {
            vvSiteUrl = this.localstorageService.getValue("server_url");
        }
        let authorizedCreds = this.getStorageView(this.authorizedCredsKey);

        if (!vvSiteUrl) {
            return null;
        }
        authorizedCreds[vvSiteUrl] = autthorizedCredsForVvSite;
        let authorizedCredsStr = JSON.stringify(authorizedCreds);
        this.localstorageService.storeValue(
            this.authorizedCredsKey,
            authorizedCredsStr
        );
    }

    /**
     * persists signed-in user details.
     * @param userDetailsForVvSite 
     * @param vvSiteUrl 
     */
    private persistUserDetails(userDetailsForVvSite: object, vvSiteUrl?: string) {
        if (!vvSiteUrl) {
            vvSiteUrl = this.localstorageService.getValue("server_url");
        }
        let userDetails = this.getStorageView(this.userDetailsKey);

        if (!vvSiteUrl) {
            return null;
        }
        userDetails[vvSiteUrl] = userDetailsForVvSite;
        let userDetailsStr = JSON.stringify(userDetails);
        this.localstorageService.storeValue(this.userDetailsKey, userDetailsStr);
    }

    /**
     * purges authorized creds of a user for a vvSite.
     * shoud be done at the time of unauthorizing
     * @param vvSiteUrl 
     */
    private purgeAuthorizedCreds(vvSiteUrl?: string): boolean {
        if (!vvSiteUrl) {
            vvSiteUrl = this.localstorageService.getValue("server_url");
        }

        let authorizedCreds = this.getStorageView(this.authorizedCredsKey);

        if (
            !vvSiteUrl ||
            !(vvSiteUrl in authorizedCreds)
        ) {
            return false;
        }
        delete authorizedCreds[vvSiteUrl];
        let authorizedCredsStr = JSON.stringify(authorizedCreds);
        this.localstorageService.storeValue(
            this.authorizedCredsKey,
            authorizedCredsStr
        );
        return true;
    }

    private purgeUserDetails(vvSiteUrl?: string): boolean {
        if (!vvSiteUrl) {
            vvSiteUrl = this.localstorageService.getValue("server_url");
        }
        let userDetails = this.getStorageView(this.userDetailsKey);

        if (
            !vvSiteUrl ||
            !(vvSiteUrl in userDetails)
        ) {
            return false;
        }
        delete userDetails[vvSiteUrl];
        let userDetailsStr = JSON.stringify(userDetails);
        this.localstorageService.storeValue(this.userDetailsKey, userDetailsStr);
        return true;
    }

    public unAuthorizeUser(vvSiteUrl?: string) {
        if (!vvSiteUrl) {
            vvSiteUrl = this.localstorageService.getValue("server_url");
        }
        let authorizedBefore = this.purgeAuthorizedCreds(vvSiteUrl);
        this.purgeUserDetails(vvSiteUrl);
        
        let signOut = () => {
            let signoutUrl = `${vvSiteUrl}/accounts/v1/oauth/signout`;
            this.http.get(signoutUrl, { withCredentials: true }).subscribe(
                response => {
                    if (authorizedBefore) {
                        this.publishAuthorizationChangeBroadcast(vvSiteUrl, false, null);
                    }
                },
                error => {
                    if (authorizedBefore) {
                        this.publishAuthorizationChangeBroadcast(vvSiteUrl, false, null);
                    }
                }
            );
        }

        let companionLogoutUrl = `${this.companionUrl}/oauth/logout`;
        this.http.get(companionLogoutUrl, { withCredentials: true }).subscribe(
            resp => signOut(),
            error => alert('error in logging out')
        );
        return authorizedBefore;
    }

    public initializeAuthorizationFlowInNewWindow(
        oauthInitializerUrl: string,
        explicitlyConversate = true,
        vvSiteUrl?: string,
        additionalState = ""
    ) {
        console.log("initializing auth flow");
        if (this.popUpWindow) {
            if (this.popUpWindow.closed) {
                this.popUpWindow = null;
            } else {
                return;
            }
        }
        let state = JSON.stringify({
            "opened_in_new_window": true,
            "additional_state": additionalState
        });
        let a = document.createElement("a");
        a.href = `oauth_callback`;
        let clientCallbackUrl = a.href;

        a.href = `${this.companionUrl}/oauth/initialize`;
        let oauthInitializerFullUrl = `${
            a.href
        }?client_callback_uri=${clientCallbackUrl}&scope=vedavaapi.root&state=${encodeURIComponent(state)}`;
        a.remove();

        let x = screen.width / 2 - 400 / 2;
        let y = screen.height / 2 - 500 / 2;
        this.popUpWindow = window.open(
            oauthInitializerFullUrl,
            "_blank",
            `height=500,width=400,toolbar=no,menubar=no, location=no,status=no, left=${x}, top=${y},,modal=yes,alwaysRaised=yes'`
        );
    }

    public initializeAuthorizationFlowIfNotAuthorized(
        oauthInitializerUrl: string,
        explicitlyConversate = true,
        vvSiteUrl?: string,
        additionalState = ""
    ) {
        if (!vvSiteUrl) {
            vvSiteUrl = this.localstorageService.getValue("server_url");
        }
        let authorizedCreds = this.getStorageView(this.authorizedCredsKey);

        if (!vvSiteUrl) {
            return null;
        }
        if (vvSiteUrl in authorizedCreds) {
            return true;
        }
        this.initializeAuthorizationFlowInNewWindow(
            oauthInitializerUrl,
            explicitlyConversate,
            vvSiteUrl,
            additionalState
        );
        return false;
    }

    public isTokenExpired(creds: AuthorizedCredsForVvSite, marginTime: number) {
        let expiresIn = Number(creds['expires_in']);
        let grantedTime = Number(creds['granted_time']);
        let currentTime = Date.now();
        if ((grantedTime + expiresIn * 1000) - currentTime < marginTime) {
            return true;
        }
        return false;
    }

    // TODO.....
    public initializeAuthorizationFlowIfToBeExpired(
        marginTime: number,
        oauthInitializerUrl: string,
        explicitlyConversate = true,
        vvSiteUrl?: string,
        additionalState = "") {
        console.log({marginTime, vvSiteUrl});
        let creds = this.getAuthorizedCreds(vvSiteUrl);
        if(!creds) {
            return;
        }
        let isExpired = this.isTokenExpired(creds, marginTime);
        if(isExpired) {
            console.log("isExpired");
            this.initializeAuthorizationFlowInNewWindow(oauthInitializerUrl, explicitlyConversate, vvSiteUrl, additionalState);
            return true;
        }
        return false;
    }

    public interceptClickForAuthorization(
        href?: string,
        oauthInitializerUrl = "oauth_initializer",
        explicitlyConversate = true,
        vvSiteUrl?: string
    ) {
        console.log(href);
        let additionalState = `${Date.now()}:intercept-click:${href}`;
        let alreadyAuthorized = this.initializeAuthorizationFlowIfNotAuthorized(
            oauthInitializerUrl,
            explicitlyConversate,
            vvSiteUrl,
            additionalState
        );
        if (!alreadyAuthorized && href) {
            let bc = new BroadcastChannel("authorization_change_channel");
            bc.onmessage = ({ data }) => {
                bc.close();
                if (href && data["additional_state"] == additionalState) {
                    window.location.href = href;
                }
            };
        }
        return alreadyAuthorized;
    }

    displayOnlyIfAuthorized(displayStyle='block', vvSiteUrl?: string) {
        let authorizedCreds = this.getAuthorizedCreds(vvSiteUrl);
        return authorizedCreds ? displayStyle : 'none';
    }
}


@Injectable({
    providedIn: "root"
})
export class AuthorizationInterceptor implements HttpInterceptor {
    constructor(
        private oauthHelperService: OauthHelperService,
        private localStorageService: LocalStorageService,
        // private failedRequestsHandlerService: FailedRequestsHandlerService
    ) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let vvSiteUrl = this.localStorageService.getValue("server_url");
        if (!request.url.startsWith(vvSiteUrl)) {
            return next.handle(request);
        }
        let authorizationHeaders = this.oauthHelperService.getAuthorizationHeaders(
            vvSiteUrl
        );

        if (authorizationHeaders) {
           let authorizedCreds = this.oauthHelperService.getAuthorizedCreds(
                vvSiteUrl
            );

            let isExpired = this.oauthHelperService.isTokenExpired(
              authorizedCreds,
              0
            );

            if(isExpired) {
                this.oauthHelperService.initializeAuthorizationFlowInNewWindow('oauth_initializer', true, vvSiteUrl);
            }

            if(!isExpired) {
                request = request.clone({
                    setHeaders: authorizationHeaders
                });
            }
            return next.handle(request);
        }

        else {
            return next.handle(request);
        }
    }
}
