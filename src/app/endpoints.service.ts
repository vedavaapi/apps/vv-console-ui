import { Injectable } from '@angular/core';
import { LocalStorageService } from 'src/app/services/local-storage.service'

@Injectable({
  providedIn: 'root'
})
export class EndpointsService {

  /**
   * Backend Endpoint URL
   */
  private server_endpoint_url: string;

  /**
   * Server API Version
   */
  private server_api_version: number = 0;

  /**
   * Mirador endpoint url.
   */
  private mirador_endpoint_url = 'https://apps.vedavaapi.org/stage/apps/mirador/mirador-view.html'; //apps.vedavaapi.org

   // private mirador_endpoint_url = 'http://localhost:8000/mirador-view.html'; //apps.vedavaapi.org

  /**
   * This property is applicable only for version 0 API version.
   */
  private org_name: string = '';
  
  /**
   * Default Constructor.
   */
  constructor(private localStorage: LocalStorageService) { }

  /**
   *
   * Returns the Base Server URL.
   * @returns String
   * @memberof EndpointsService
   */
  public getBaseUrl(): string {
      console.log('getBaseUrl', localStorage.getItem('server_url'));
    return this.localStorage.getValue('server_url');
  }


  /**
   * Sets the Server Endpoint URL.
   * @param serverUrl string
   */
  public setBaseServerUrl(serverUrl: string) {
    this.server_endpoint_url = serverUrl;
    console.log('server api url is setted ' + this.server_endpoint_url)
  }

  /**
   * Sets the Mirador instance URL.
   * @param miradorUrl string
   */
  public setMiradorInstanceUrl(miradorUrl: string) {
    this.mirador_endpoint_url = miradorUrl;
  }

  
  /**
   * Returns Instance of Mirador Host.
   * @returns String
   * @memberof EndpointsService
   */
  getMiradorInstanceUrl(): string {
    return this.mirador_endpoint_url;
  }


  /**
   * Returns the Instance of Mirador Viewer URL.
   * @param bookId string
   */
  getMiradorViewerUrl(bookId: string): string {
    if(bookId == null){
      return this.mirador_endpoint_url;
    }
    return this.mirador_endpoint_url + "?book_id=" + bookId;
  }

  /**
   * Returns the Server API Version.
   */
  getServerAPIVersion(): number {
    return this.server_api_version;
  }

  /**
   * Returns the organization name.
   */
  public getOrgName(): string {
    return this.org_name;
  }
}
