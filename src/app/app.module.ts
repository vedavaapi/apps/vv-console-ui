import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UiModule } from './ui/ui.module';
import { EndpointsService } from './endpoints.service';
import { OauthHelperService, AuthorizationInterceptor } from './services/oauth-helper.service';
import { LocalStorageService } from './services/local-storage.service';
import { VedavaapiShellHelperService } from './services/vedavaapi-shell-helper.service';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BsDropdownModule.forRoot(),
    UiModule,
    HttpClientModule
  ],
  providers: [
    EndpointsService,
    LocalStorageService,
    OauthHelperService,
    VedavaapiShellHelperService,
    //FailedRequestsHandlerService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthorizationInterceptor,
      multi: true
    },
    /*{
      provide: HTTP_INTERCEPTORS,
      useClass: AuthorizationRefreshInterceptor,
      multi: true
    }*/
],
    bootstrap: [AppComponent]
})
export class AppModule { }
